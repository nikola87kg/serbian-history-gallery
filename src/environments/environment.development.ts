const firebaseConfig = {
  apiKey: 'AIzaSyCFdeAptcwoDcYrQESIWlJUvkQ-Y_O_8Y4',
  authDomain: 'galerijasrbije.firebaseapp.com',
  projectId: 'galerijasrbije',
  storageBucket: 'galerijasrbije.appspot.com',
  messagingSenderId: '8371106390',
  appId: '1:8371106390:web:64ed8f36aec45df6f701f7',
  measurementId: 'G-324PVJ07P1',
};

export const environment = {
  production: false,
  firebaseConfig,
  storage: 'galerijasrbije.appspot.com/',
  adminId: 'oUaqerXvRYNFTH4xohWCyHocCbR2',
  gtag: 'G-324PVJ07P1',
};
