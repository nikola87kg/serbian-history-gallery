import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: 'input[numbersOnly]',
  standalone: true,
})
export class NumbersOnlyDirective {
  constructor(private host: ElementRef) { }

  ngOnInit() {
    this.host.nativeElement.onpaste = this.pasteHandler.bind(this);
  }

  pasteHandler(event: ClipboardEvent) {
    const pastedData = event.clipboardData?.getData('Text');
    if (pastedData) {
      this.delayRegexChecker(pastedData);
    }
  }

  removeInvalidChars(regex: RegExp) {
    const word = this.value;
    const newValue = word?.replace(regex, '');
    this.value = newValue;
  }

  forceTriggerInput(element?: any) {
    element?.dispatchEvent(new Event('input', { bubbles: true }));
  }

  delayRegexChecker(event: any) {
    setTimeout(() => {
      this.removeInvalidChars(/[^0-9]/gi);
      this.forceTriggerInput(event.target);
    }, 0);
  }

  allowedKeys = {
    Delete: 46,
    Backspace: 8,
    Tab: 9,
    Escape: 27,
    Enter: 13,
    End: 35,
    Home: 36,
    ArrowLeft: 37,
    ArrowRight: 39,
    ArrowUp: 38,
  };

  allowedCtrlKeys = {
    KeyA: 65,
    KeyC: 67,
    KeyV: 86,
    KeyX: 88,
  };

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    if (this.keyExists(event, this.allowedKeys) || this.ctrlKeyExists(event)) {
      return;
    }
    if (!this.isNumber(event) || this.isSpace(event)) {
      event.preventDefault();
    }
  }

  isSpace(event: KeyboardEvent) {
    return event.keyCode === 32;
  }

  ctrlKeyExists(event: KeyboardEvent) {
    if (event.ctrlKey !== true && event.metaKey !== true) {
      return false;
    }
    return this.keyExists(event, this.allowedCtrlKeys);
  }

  keyExists(event: KeyboardEvent, allowedKeys: { [name: string]: number; }) {
    return Object.values(allowedKeys).indexOf(event.keyCode) !== -1;
  }

  isNumber(event: KeyboardEvent) {
    if (event.key !== undefined) {
      return !(event.key === null || isNaN(Number(event.key)));
    }
    if (event.keyCode !== undefined) {
      return !(
        (event.shiftKey || event.keyCode < 48 || event.keyCode > 57) &&
        (event.keyCode < 96 || event.keyCode > 105)
      );
    }
    return false;
  }

  set value(value) {
    this.host.nativeElement.value = value;
  }

  get value() {
    return this.host?.nativeElement?.value;
  }
}
