import { Directive, Input } from '@angular/core';

@Directive({
  standalone: true,
  selector: '[appDefaultLogo]',
  host: {
    '[src]': 'setPath(src)',
    '(error)': 'onError()',
  },
})
export class DefaultLogoDirective {
  @Input() src!: string | null | undefined;
  public defaultImg = 'assets/images/default.png';
  public onError() {
    this.src = this.defaultImg;
  }
  public setPath(src: string | null | undefined) {
    return src || this.defaultImg;
  }
}
