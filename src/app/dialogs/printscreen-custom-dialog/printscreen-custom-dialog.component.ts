
import { ChangeDetectionStrategy, Component, inject, Inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { SerbianMapComponent } from '@app/components/serbian-map/serbian-map.component';
import { CalendarDatePipe } from '@app/pipes/calendar-date.pipe';
import { Tag } from '@app/services/tags.service';
import { ThemeService } from '@app/services/theme.service';

@Component({
  templateUrl: './printscreen-custom-dialog.component.html',
  styleUrl: './printscreen-custom-dialog.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [MatButtonModule, MatIconModule, MatInputModule, FormsModule,
    CalendarDatePipe, SerbianMapComponent]
})
export class PrintscreenCustomDialogComponent {
  public themeService = inject(ThemeService);
  public showButtons = false;
  public bold = true;
  public dayTitle = 'Дан града';
  public fontSizePx = 32;
  public tag!: Tag;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { tag: Tag; },
    public dialogRef: MatDialogRef<PrintscreenCustomDialogComponent>
  ) {
    this.tag = data.tag;
  }

  switchDayTitle() {
    if (this.dayTitle === 'Дан града') {
      this.dayTitle = 'Дан општине';
    } else {
      this.dayTitle = 'Дан града';
    }
  }

  decreaseFont() {
    this.fontSizePx--;
  }
}
