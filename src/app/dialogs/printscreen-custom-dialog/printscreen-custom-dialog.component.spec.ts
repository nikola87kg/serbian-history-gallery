import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintscreenCustomDialogComponent } from './printscreen-custom-dialog.component';

describe('BirthdayDialogComponent', () => {
  let component: PrintscreenCustomDialogComponent;
  let fixture: ComponentFixture<PrintscreenCustomDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrintscreenCustomDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PrintscreenCustomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
