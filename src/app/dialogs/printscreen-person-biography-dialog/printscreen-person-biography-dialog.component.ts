import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, inject
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CategoriesService, Category } from '@app/services/categories.service';
import { Epoch, EpochsService } from '@app/services/epochs.service';
import { GlossaryService } from '@app/services/glossary.service';
import { Person } from '@app/services/persons.service';
import { ThemeService } from '@app/services/theme.service';
import { UtilsService } from '@app/services/utils.service';

@Component({
  imports: [MatButtonModule],
  templateUrl: './printscreen-person-biography-dialog.component.html',
  styleUrl: './printscreen-person-biography-dialog.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintscreenPersonBiographyDialogComponent {
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private cdr = inject(ChangeDetectorRef);
  public descFontSize = 16;
  public showImage = true;
  public fragments: string[] = [];

  public person!: Person;
  public Glossary = GlossaryService.Glossary;
  public horoscopeSign!: string;
  public categories: Category[] = [];
  public epochs: Epoch[] = [];
  public themeService = inject(ThemeService);
  public fragmentCount = 2;

  get birthday() {
    const day = this.person.birthDay;
    const month = UtilsService.getMonthTitleFromOrder('' + this.person.birthMonth);
    if (!day || !month) {
      return '';
    }
    return `${day}. ${month}`;
  }

  get years() {
    const born = this.person.birthYear;

    const fullYears = this.person.birthDeath;
    let die = '';
    if (fullYears?.includes(' - ') || fullYears?.includes(' — ')) {
      const segments = fullYears.split(' - ');
      const deathDate = segments[segments.length - 1];
      const deathSegments = deathDate.split('.');
      const deathYear = deathSegments[deathSegments.length - 1];
      die = ` - ${deathYear}`;
    }
    return `${born}${die}`;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { item: Person; },
    public dialogRef: MatDialogRef<PrintscreenPersonBiographyDialogComponent>
  ) {
    this.person = data.item;
    let fragments = this.person.keyNotes?.split('<p>') || [];
    fragments.shift();
    fragments = fragments.map(f => {
      f = f.split('</strong>- ').join('</strong>');
      f = f.split('</strong> - ').join('</strong>');
      f = f.split('> - ').join('>');
      f = f.split('- Са').join('Са');
      return '<p>' + f;
    });
    this.fragments = [...fragments];
  }

  ngOnInit() {
    this.handleCategories();
  }

  increaseFragments() {
    this.fragmentCount++;

    if (this.fragmentCount > 5) {
      this.fragmentCount = 2;
    }

  }

  decreaseFont() {
    this.descFontSize--;
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.categories = [...response];
        this.cdr.detectChanges();
      },
    });
    this.epochsService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.epochs = [...response];
        this.cdr.detectChanges();
      },
    });
  }

  onClose() {
    this.dialogRef.close();
  }

  onRemoveFragment() {
    this.fragments.shift();
  }
}

