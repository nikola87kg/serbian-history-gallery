import { ComponentFixture, TestBed } from '@angular/core/testing';

import {
  PrintscreenPersonBiographyDialogComponent
} from './printscreen-person-biography-dialog.component';

describe('PrintscreenPersonBiographyDialogComponent', () => {
  let component: PrintscreenPersonBiographyDialogComponent;
  let fixture: ComponentFixture<PrintscreenPersonBiographyDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrintscreenPersonBiographyDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PrintscreenPersonBiographyDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
