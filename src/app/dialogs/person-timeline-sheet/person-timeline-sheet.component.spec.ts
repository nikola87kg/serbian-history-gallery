import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';

import { PersonTimelineSheetComponent } from './person-timeline-sheet.component';

describe('PersonTimelineSheetComponent', () => {
  let component: PersonTimelineSheetComponent;
  let fixture: ComponentFixture<PersonTimelineSheetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PersonTimelineSheetComponent],
      providers: [
        { provide: MAT_BOTTOM_SHEET_DATA, useValue: {} },
        { provide: MatBottomSheetRef, useValue: {} },
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    fixture = TestBed.createComponent(PersonTimelineSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
