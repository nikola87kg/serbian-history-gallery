import { ChangeDetectionStrategy, Component, DestroyRef, Inject, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { GlossaryService } from '@services/glossary.service';
import { Person, PersonsService } from '@services/persons.service';

@Component({
    imports: [MatButtonModule, MatIconModule, TranslitPipe],
    templateUrl: './person-timeline-sheet.component.html',
    styleUrls: ['./person-timeline-sheet.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonTimelineSheetComponent {
  private service = inject(PersonsService);
  private destroyRef = inject(DestroyRef);
  private router = inject(Router);

  public person!: Person;
  public Glossary = GlossaryService.Glossary;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: { slug: string; },
    private bottomSheetRef: MatBottomSheetRef<PersonTimelineSheetComponent>,
  ) { }

  ngOnInit() {
    this.handlePerson();
  }

  handlePerson() {
    this.service.getItemBySlug(this.data.slug)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Person) => {
          this.person = { ...response };
        },
      });
  }

  goToBio(slug: string | null | undefined) {
    this.bottomSheetRef.dismiss();
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onClose(event: any) {
    event.stopPropagation();
    this.bottomSheetRef.dismiss(true);
  }
}
