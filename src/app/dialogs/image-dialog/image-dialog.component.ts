import { UploadResult } from 'firebase/storage';
import { ImageCroppedEvent, ImageCropperModule } from 'ngx-image-cropper';

import { ChangeDetectionStrategy, Component, Inject, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { DefaultLogoDirective } from '@directives/default-logo.directive';
import { CrudService } from '@services/firestore-crud.service';
import { Person } from '@services/persons.service';

export interface FileInfo {
  filename: string;
  originalname: string;
}

interface Data {
  item: Person;
  service: CrudService<any>,
  id: string;
  defaultFormat: 'portrait' | 'landscape' | 'box';
}


@Component({
  imports: [
    MatDialogModule,
    MatButtonModule,
    DefaultLogoDirective,
    ImageCropperModule
  ],
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageDialogComponent {
  public imageSrc!: string;
  public imageChangedEvent: any = '';
  public imagePreview!: any;
  public croppedImage: any = '';
  public resizeToWidth = 500;
  public ratio = 500 / 315;
  public extension: any = 'jpeg';
  public lastBlob: Blob | null | undefined;
  public maintainAspectRatio = true;

  private lastUploadPath!: string;
  private sanitizer = inject(DomSanitizer);

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Data,
    public dialogRef: MatDialogRef<ImageDialogComponent>
  ) {
    this.imageSrc = data?.item?.imagePath!;

    if (data.defaultFormat === 'portrait') {
      this.switchRatio();
      this.switchRatio();
    }

    if (data.defaultFormat === 'box') {
      this.switchRatio();
    }
  }

  switchRatio() {
    if (this.resizeToWidth === 500) {
      this.ratio = 400 / 400;
      this.resizeToWidth = 400;
      this.extension = 'jpeg';
    } else if (this.resizeToWidth === 400) {
      this.ratio = 150 / 210;
      this.resizeToWidth = 150;
      this.extension = 'png';
    } else if (this.resizeToWidth === 150) {
      this.ratio = 500 / 315;
      this.resizeToWidth = 500;
      this.extension = 'jpeg';
    }
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = this.sanitizer.bypassSecurityTrustUrl((event as any).objectUrl);
    this.lastBlob = event.blob;
  }

  doImageUpload() {
    if (this.lastBlob) {
      const reader: FileReader = new FileReader();
      reader.onload = () => {
        this.imagePreview = reader.result;
      };
      reader.readAsDataURL(this.lastBlob);
      const slug = (this.data?.item?.slug || 'image') + '-' + Math.floor(Math.random() * 1000);
      const filename = `${slug}.${this.extension}`;

      this.data.service.uploadFile(
        this.lastBlob,
        filename
      ).then((snapshot: UploadResult) => {
        const path = snapshot.metadata.fullPath;
        this.data.service.getDownloadURL(path).then((url: string) => {
          this.lastUploadPath = url;
          this.onSave();
        });
      });
    }
  }

  onSave() {
    if (!this.lastUploadPath) {
      return;
    }

    this.data.service.updateItem(
      this.data.id,
      { imagePath: this.lastUploadPath }
    ).then(() => {
      this.dialogRef.close(true);
    });
  }
}
