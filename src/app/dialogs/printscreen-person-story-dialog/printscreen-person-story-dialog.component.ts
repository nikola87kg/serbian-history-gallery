import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, inject
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IdToNamePipe } from '@app/pipes/id-to-name.pipe';
import { CategoriesService, Category } from '@app/services/categories.service';
import { Epoch, EpochsService } from '@app/services/epochs.service';
import { ThemeService } from '@app/services/theme.service';
import { UtilsService } from '@app/services/utils.service';
import { Person } from '@services/persons.service';

@Component({
  imports: [CommonModule, MatButtonModule,
    IdToNamePipe],
  templateUrl: './printscreen-person-story-dialog.component.html',
  styleUrls: ['./printscreen-person-story-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintscreenPersonStoryDialogComponent {
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private cdr = inject(ChangeDetectorRef);

  public person!: Person;
  public categories: Category[] = [];
  public epochs: Epoch[] = [];
  public themeService = inject(ThemeService);
  public transparentText = false;
  public isGrayscale = false;

  get birthday() {
    const day = this.person.birthDay;

    const month = UtilsService.getMonthTitleFromOrder('' + this.person.birthMonth);
    if (!day || !month) {
      return '';
    }
    return `${day}. ${month}`;
  }

  get years() {
    const born = this.person.birthYear;

    const fullYears = this.person.birthDeath;
    let die = '';
    if (fullYears?.includes(' - ') || fullYears?.includes(' — ')) {
      const segments = fullYears.split(' - ');
      const deathDate = segments[segments.length - 1];
      const deathSegments = deathDate.split('.');
      const deathYear = deathSegments[deathSegments.length - 1];
      die = ` - ${deathYear}`;
    }
    return `${born}${die}`;
  }

  get dateBirth() {
    const fullYears = this.person.birthDeath;
    if (fullYears?.includes(' - ') || fullYears?.includes(' — ')) {
      const segments = fullYears.split(' - ');
      return segments[0];
    }
    if (fullYears?.includes(' — ')) {
      const segments = fullYears.split(' — ');
      return segments[0];
    }
    return fullYears;
  }

  get dateDeath() {
    const fullYears = this.person.birthDeath;
    if (fullYears?.includes(' - ') || fullYears?.includes(' — ')) {
      const segments = fullYears.split(' - ');
      return segments[1];
    }
    if (fullYears?.includes(' — ')) {
      const segments = fullYears.split(' — ');
      return segments[1];
    }
    return null;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { item: Person; },
    public dialogRef: MatDialogRef<PrintscreenPersonStoryDialogComponent>
  ) {
    this.person = data.item;
  }

  ngOnInit() {
    this.handleCategories();
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.categories = [...response];
        this.cdr.detectChanges();
      },
    });
    this.epochsService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.epochs = [...response];
        this.cdr.detectChanges();
      },
    });
  }
  onClose() {
    this.dialogRef.close();
  }

  onBirthday() {
    this.transparentText = true;
  }
}
