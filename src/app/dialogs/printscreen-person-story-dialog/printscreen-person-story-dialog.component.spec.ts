import { Firestore } from 'firebase/firestore';

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { PrintscreenPersonStoryDialogComponent } from './printscreen-person-story-dialog.component';

describe('ViewImageDialogComponent', () => {
  let component: PrintscreenPersonStoryDialogComponent;
  let fixture: ComponentFixture<PrintscreenPersonStoryDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PrintscreenPersonStoryDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    fixture = TestBed.createComponent(PrintscreenPersonStoryDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
