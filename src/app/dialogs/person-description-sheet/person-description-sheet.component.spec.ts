import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';

import { PersonDescriptionSheetComponent } from './person-description-sheet.component';

describe('PersonDescriptionSheetComponent', () => {
  let component: PersonDescriptionSheetComponent;
  let fixture: ComponentFixture<PersonDescriptionSheetComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PersonDescriptionSheetComponent],
      providers: [
        { provide: MAT_BOTTOM_SHEET_DATA, useValue: {} },
        { provide: MatBottomSheetRef, useValue: {} },
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    fixture = TestBed.createComponent(PersonDescriptionSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
