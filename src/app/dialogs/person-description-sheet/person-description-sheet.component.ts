import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, Inject, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TagItemComponent } from '@app/components/tag-item/tag-item.component';
import { BirthMonthDatePipe } from '@app/pipes/birth-month-date.pipe';
import { IdToNamePipe } from '@app/pipes/id-to-name.pipe';
import { IdToSlugPipe } from '@app/pipes/id-to-slug.pipe';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@app/services/auth.service';
import { CategoriesService, Category } from '@app/services/categories.service';
import { Epoch, EpochsService } from '@app/services/epochs.service';
import { Tag, TagsService } from '@app/services/tags.service';
import { UtilsService } from '@app/services/utils.service';
import { GlossaryService } from '@services/glossary.service';
import { Person, PersonsService } from '@services/persons.service';
import { UserProfile, UserProfileService } from '@services/user-profile.service';

@Component({
  imports: [CommonModule, MatButtonModule, MatIconModule,
    TranslitPipe,
    TagItemComponent, BirthMonthDatePipe, IdToNamePipe],
  templateUrl: './person-description-sheet.component.html',
  styleUrls: ['./person-description-sheet.component.scss'],
  providers: [IdToSlugPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonDescriptionSheetComponent {
  private service = inject(PersonsService);
  private userProfile!: UserProfile | null;
  private userProfileService = inject(UserProfileService);
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private destroyRef = inject(DestroyRef);
  private tagsService = inject(TagsService);
  private cdr = inject(ChangeDetectorRef);
  private idToSlugPipe = inject(IdToSlugPipe);

  public authService = inject(AuthService);
  public person!: Person;
  public categories: Category[] = [];
  public epochs: Epoch[] = [];
  public Glossary = GlossaryService.Glossary;
  public personTags: Tag[] = [];
  public favouritedByUser: boolean | null = null;

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: { slug: string; },
    private bottomSheetRef: MatBottomSheetRef<PersonDescriptionSheetComponent>,
  ) { }

  ngOnInit() {
    this.handleCategories();
    this.handlePerson();
    this.handleFavourites();
  }

  handleFavourites(): void {
    this.authService.userProfile$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe(user => {
        if (user) {
          this.userProfile = user!;
          const slug = this.person.slug!;
          this.favouritedByUser = !!user?.favourites?.includes(slug);
        } else {
          this.userProfile = null;
          this.favouritedByUser = null;
        }
      });
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.categories = [...response];
        this.cdr.detectChanges();
      },
    });
    this.epochsService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.epochs = [...response];
        this.cdr.detectChanges();
      },
    });
  }

  handlePerson() {
    this.service.getItemBySlug(this.data.slug)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Person) => {
          this.person = { ...response };
          this.handleTags();
        },
      });
  }

  handleTags(): void {
    this.tagsService.getItemsSorted('slug')
      .subscribe({
        next: (response: any[]) => {
          const tags = [...response];
          this.personTags = this.person?.tags?.map((t: string) => {
            const tag = tags.find(tag => tag.id === t);
            return tag!;
          }) || [];

          this.cdr.detectChanges();
        },
      });
  }

  goToCategoryPage() {
    const categories = this.categories;
    const category = this.person.category!;
    const slug = this.idToSlugPipe.transform(category, categories);
    const url = `kategorija/${slug}`;
    this.bottomSheetRef.dismiss({ onX: false, redirection: true, url });
  }

  goToBio(slug: string | null | undefined) {
    const url = `biografija/${slug}`;
    this.bottomSheetRef.dismiss({ onX: false, redirection: true, url });
  }

  onTagUrl(url: string) {
    this.bottomSheetRef.dismiss({ onX: false, redirection: true, url });
  }

  onClose(event: any, onX = false, redirection = false) {
    event?.stopPropagation();
    this.bottomSheetRef.dismiss({ onX, redirection });
  }

  onFullStar() {
    if (this.userProfile) {
      const slug = `${this.person.slug}`;
      this.userProfileService.removeFromFavourites(this.userProfile, slug);
      setTimeout(() => {
        UtilsService.openSnackbar(`Личност успешно уклоњена из фаворита.`, false, true);
      }, 120);
    }
  }

  onEmptyStar() {
    if (this.userProfile) {
      const slug = `${this.person.slug}`;
      const [isError, message] = this.userProfileService
        .addToFavourites(this.userProfile, slug);
      UtilsService.openSnackbar(message, isError, true);
    }
  }

  onNullStar() {
    const url = `nalog}`;
    this.bottomSheetRef.dismiss({ onX: false, redirection: true, url });
  }

  onEdit(): void {
    const url = `admin/panel/person-form/${this.person.id}`;
    this.bottomSheetRef.dismiss({ onX: false, redirection: true, url });
  }

  onBirth() {
    const { birthDay, birthMonth } = this.person;
    const path = UtilsService.getDaySlug(birthDay!, birthMonth!);
    const url = `kalendar/${path}/rodjeni`;
    this.bottomSheetRef.dismiss({ onX: false, redirection: true, url });
  }

  onCategory() {
    const pipe = new IdToSlugPipe();
    const category = pipe.transform(this.person.category!, this.categories);
    const url = `kategorija/${category}`;
    this.bottomSheetRef.dismiss({ onX: false, redirection: true, url });
  }
}
