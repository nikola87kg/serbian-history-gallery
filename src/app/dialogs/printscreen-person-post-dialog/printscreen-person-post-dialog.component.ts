import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, inject
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IdToNamePipe } from '@app/pipes/id-to-name.pipe';
import { YearsOldPipe } from '@app/pipes/years-old.pipe';
import { CategoriesService, Category } from '@app/services/categories.service';
import { Epoch, EpochsService } from '@app/services/epochs.service';
import { GlossaryService } from '@app/services/glossary.service';
import { Person } from '@app/services/persons.service';
import { ThemeService } from '@app/services/theme.service';
import { UtilsService } from '@app/services/utils.service';

@Component({
    imports: [MatButtonModule, IdToNamePipe, YearsOldPipe],
    templateUrl: './printscreen-person-post-dialog.component.html',
    styleUrl: './printscreen-person-post-dialog.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintscreenPersonPostDialogComponent {
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private cdr = inject(ChangeDetectorRef);

  public person!: Person;
  public showLogo = true;
  public Glossary = GlossaryService.Glossary;
  public horoscopeSign!: string;
  public categories: Category[] = [];
  public epochs: Epoch[] = [];
  public descFontSize = 16;
  public descHeight = 14;
  public themeService = inject(ThemeService);
  public showInput = false;

  get birthday() {
    const day = this.person.birthDay;
    const month = UtilsService.getMonthTitleFromOrder('' + this.person.birthMonth);
    if (!day || !month) {
      return '';
    }
    return `${day}. ${month}`;
  }

  getRemSizeTitle() {
    return this.person?.commonName?.length
      && this.person.commonName?.length > 20 ? '2' : '2.5';
  }

  get years() {
    const born = this.person.birthYear;

    const fullYears = this.person.birthDeath;
    let die = '';
    if (fullYears?.includes(' - ') || fullYears?.includes(' — ')) {
      const segments = fullYears.split(' - ');
      const deathDate = segments[segments.length - 1];
      const deathSegments = deathDate.split('.');
      const deathYear = deathSegments[deathSegments.length - 1];
      die = ` - ${deathYear}`;
    }
    return `${born}${die}`;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { item: Person; },
    public dialogRef: MatDialogRef<PrintscreenPersonPostDialogComponent>
  ) {
    this.person = data.item;
  }

  ngOnInit() {
    this.handleCategories();
  }

  onInputChange($event: any) {
    const stringValue: string = $event.target.value;
    this.themeService.adminHeadline.set(stringValue);
  }

  decreaseFont() {
    this.descFontSize--;
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.categories = [...response];
        this.cdr.detectChanges();
      },
    });
    this.epochsService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.epochs = [...response];
        this.cdr.detectChanges();
      },
    });
  }

  onImage() {
    this.descHeight--;
  }

  onClose() {
    this.dialogRef.close();
  }
}

