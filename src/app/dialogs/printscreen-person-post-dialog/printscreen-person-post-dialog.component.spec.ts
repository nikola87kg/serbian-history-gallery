import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintscreenPersonPostDialogComponent } from './printscreen-person-post-dialog.component';

describe('PrintscreenPostDialogComponent', () => {
  let component: PrintscreenPersonPostDialogComponent;
  let fixture: ComponentFixture<PrintscreenPersonPostDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrintscreenPersonPostDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PrintscreenPersonPostDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
