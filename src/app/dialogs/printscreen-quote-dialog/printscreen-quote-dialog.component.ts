import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Inject, inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { TrimTextPipe } from '@app/pipes/trim-text.pipe';
import { Person } from '@app/services/persons.service';
import { Quote } from '@app/services/quotes.service';
import { ThemeService } from '@app/services/theme.service';

@Component({
    imports: [CommonModule, TrimTextPipe, MatIconModule],
    templateUrl: './printscreen-quote-dialog.component.html',
    styleUrl: './printscreen-quote-dialog.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintscreenQuoteDialogComponent {
  public person!: Person;
  public quote!: Quote;
  public themeService = inject(ThemeService);
  public fontPixel = 26;
  public invertImage = false;
  public pxSize = 1;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { item: Person; quote: Quote; },
    public dialogRef: MatDialogRef<PrintscreenQuoteDialogComponent>
  ) {
    this.person = data.item;
    this.quote = data.quote;

  }

  decreaseFont() {
    this.fontPixel--;
  }

  onClose() {
    this.dialogRef.close();
  }

  onImageClick() {
    this.pxSize = this.pxSize + 10;
  }

  onLogoClick() {
    this.invertImage = !this.invertImage;
  }

}

