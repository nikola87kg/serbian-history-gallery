import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintscreenQuoteDialogComponent } from './printscreen-quote-dialog.component';

describe('PrintscreenQuoteDialogComponent', () => {
  let component: PrintscreenQuoteDialogComponent;
  let fixture: ComponentFixture<PrintscreenQuoteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrintscreenQuoteDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PrintscreenQuoteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
