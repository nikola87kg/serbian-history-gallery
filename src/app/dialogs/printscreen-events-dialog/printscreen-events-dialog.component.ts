import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Inject, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ThemeService } from '@app/services/theme.service';
import { UtilsService } from '@app/services/utils.service';

@Component({
  imports: [CommonModule, MatButtonModule],
  templateUrl: './printscreen-events-dialog.component.html',
  styleUrls: ['./printscreen-events-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintscreenEventsDialogComponent {

  public themeService = inject(ThemeService);
  public item!: any;
  public storyTitle!: string;
  public storyDate!: string;
  public index = 0;
  public descFontSize = 22;
  public descTitleSize = 34;
  public showLogo = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { items: any, saint: boolean; },
    public dialogRef: MatDialogRef<PrintscreenEventsDialogComponent>
  ) {
    this.storyTitle = data.saint ? 'Празник - ' : 'Догодило се на ';

    this.item = data.items[this.index];
    this.setItem();
  }

  onStoryTitle() {
    if (this.storyTitle === 'Празник - ') {
      this.storyTitle = 'Верски празник - ';
    } else if (this.storyTitle === 'Верски празник - ') {
      this.storyTitle = 'Обележавање - ';
    } else if (this.storyTitle === 'Обележавање - ') {
      this.storyTitle = 'Догађај - ';
    }
  }

  setItem() {
    this.item = this.data.items[this.index];
    if (!this.item) {
      return;
    }

    this.storyDate = this.data.saint
      ? `${this.item.saintDay}. ${UtilsService.getMonthTitleFromOrder(String(this.item.saintMonth))}`
      : `${this.item.day}. ${UtilsService.getMonthTitleFromOrder(String(this.item.month))}`;
  }

  changeIndex() {
    this.index++;
    if (this.index >= this.data.items.length) {
      this.index = 0;
    }
    this.setItem();
  }

  onClose() {
    this.dialogRef.close();
  }

  decreaseTitleFont() {
    this.descTitleSize--;
  }

  decreaseFont() {
    this.descFontSize--;
  }

  styleFirstWord(text: string) {
    if (this.item.name) {
      return text;
    }

    const firstWordEnd = text.indexOf(' ');
    if (firstWordEnd !== -1) {
      return `<span class="first-word red">${text.substring(0, firstWordEnd)}</span>${text.substring(firstWordEnd)}`;
    }

    return text;
  }

}
