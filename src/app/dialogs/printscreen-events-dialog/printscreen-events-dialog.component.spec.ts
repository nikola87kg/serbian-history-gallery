import { Firestore } from 'firebase/firestore';

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { PrintscreenEventsDialogComponent } from './printscreen-events-dialog.component';

describe('PrintscreenEventsDialogComponent', () => {
  let component: PrintscreenEventsDialogComponent;
  let fixture: ComponentFixture<PrintscreenEventsDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PrintscreenEventsDialogComponent],
      providers: [
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: MatDialogRef, useValue: {} },
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    fixture = TestBed.createComponent(PrintscreenEventsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
