import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';

import { BasicSheetComponent } from './basic-sheet.component';

describe('BasicSheetComponent', () => {
  let component: BasicSheetComponent<any>;
  let fixture: ComponentFixture<BasicSheetComponent<any>>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BasicSheetComponent],
      providers: [
        { provide: MAT_BOTTOM_SHEET_DATA, useValue: {} },
        { provide: MatBottomSheetRef, useValue: {} },
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    fixture = TestBed.createComponent(BasicSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
