

import { AsyncPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_BOTTOM_SHEET_DATA, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { TranslitPipe } from '@app/pipes/translit.pipe';

@Component({
    selector: 'app-multiselect-sheet',
    imports: [AsyncPipe,
        MatFormFieldModule,
        ReactiveFormsModule,
        MatButtonModule,
        TranslitPipe,
        MatCheckboxModule],
    templateUrl: './multiselect-sheet.component.html',
    styleUrl: './multiselect-sheet.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiselectSheetComponent {
  public selectedItems: string[] = [];

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any,
    private bottomSheetRef: MatBottomSheetRef<MultiselectSheetComponent>,
  ) {
    this.selectedItems = data.selectedItems || [];
  }

  onCheckbox(item: any) {
    const id = item.id;
    const alreadySelected = this.selectedItems.includes(id);

    if (alreadySelected) {
      this.selectedItems = this.selectedItems.filter(e => e !== id);
      return;
    }

    this.selectedItems.push(id);
  }

  onConfirmation() {
    this.bottomSheetRef.dismiss(this.selectedItems);
  }
}