import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MultiselectSheetComponent } from './multiselect-sheet.component';

describe('MultiselectSheetComponent', () => {
  let component: MultiselectSheetComponent;
  let fixture: ComponentFixture<MultiselectSheetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MultiselectSheetComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MultiselectSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
