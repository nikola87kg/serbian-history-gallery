import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintscreenCitySightsDialogComponent } from './printscreen-city-sights-dialog.component';

describe('PrintscreenCitySightsDialogComponent', () => {
  let component: PrintscreenCitySightsDialogComponent;
  let fixture: ComponentFixture<PrintscreenCitySightsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrintscreenCitySightsDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PrintscreenCitySightsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
