import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Inject, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ThemeService } from '@app/services/theme.service';

@Component({
  selector: 'app-printscreen-city-sights-dialog',
  imports: [CommonModule, MatButtonModule],
  templateUrl: './printscreen-city-sights-dialog.component.html',
  styleUrl: './printscreen-city-sights-dialog.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PrintscreenCitySightsDialogComponent {
  public themeService = inject(ThemeService);
  public items: any[] = [];
  public itemsDisplayed = 6;
  public showDesc = true;
  public showInput = false;
  public descSizePx = 15;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PrintscreenCitySightsDialogComponent>
  ) {
    this.items = data.items;
  }

  onRemove(item: any) {
    if (this.items.length === 1) {
      return;
    }
    const index = this.items.findIndex(i => i.id === item.id);
    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

  onInputChange($event: any) {
    const stringValue: string = $event.target.value;
    this.themeService.adminHeadline.set(stringValue);
  }

  onDescription() {
    this.descSizePx--;
    if (this.descSizePx < 14) {
      this.showDesc = false;
    }
  }

  switchView() {
    this.itemsDisplayed = this.itemsDisplayed === 3
      ? 6
      : this.itemsDisplayed === 6
        ? 1
        : 3;
  }

  onClose() {
    this.dialogRef.close();
  }
}

