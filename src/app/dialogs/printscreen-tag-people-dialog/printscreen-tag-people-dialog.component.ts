import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Inject
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { CategoriesService, Category } from '@app/services/categories.service';
import { Epoch, EpochsService } from '@app/services/epochs.service';
import { Person } from '@app/services/persons.service';
import { Tag } from '@app/services/tags.service';
import { ThemeService } from '@app/services/theme.service';

interface InitData {
  items: Person[];
  tag: Tag;
  grid?: boolean;
}

@Component({
  templateUrl: './printscreen-tag-people-dialog.component.html',
  styleUrl: './printscreen-tag-people-dialog.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, MatButtonModule, MatIconModule, FormsModule]
})
export class PrintscreenTagPeopleDialogComponent {
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private cdr = inject(ChangeDetectorRef);

  public showLogo = true;

  public items!: Person[];
  public categories: Category[] = [];
  public tagName!: string;
  public themeService = inject(ThemeService);
  public epochs: Epoch[] = [];
  public sixGrid = false;
  public itemDisplayed = 4;
  public clamp = 4;
  public gridTitle = 'ГАЛЕРИЈА СРБИЈЕ';
  public showGridInput = false;
  public fontPx = 16;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InitData,
    public dialogRef: MatDialogRef<PrintscreenTagPeopleDialogComponent>
  ) {
    this.initData(data);
  }

  ngOnInit() {
    this.handleCategories();
  }

  increaseClamp() {
    this.clamp++;
  }

  initData(data: InitData) {
    this.items = [...data.items];
    this.tagName = data.tag?.name || '';
    this.sixGrid = !!data?.grid;

    this.itemDisplayed = this.sixGrid ? 6 : 4;
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.categories = [...response];
        this.cdr.detectChanges();
      },
    });
    this.epochsService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.epochs = [...response];
        this.cdr.detectChanges();
      },
    });
  }

  onRemove(event: any, item: Person) {
    event?.stopPropagation();

    if (this.items.length === 1) {
      return;
    }
    const index = this.items.findIndex(i => i.id === item.id);
    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

  onGridTitle() {
    this.showGridInput = !this.showGridInput;
  }

  onItemData() {
    this.fontPx--;
  }

  reduceFontSize(event: Event) {
    const target = event.target as HTMLElement;
    const currentFontSize = parseFloat(window.getComputedStyle(target).fontSize) / 10;
    let size = Math.round(currentFontSize * 10 - 1) / 10;
    if (size < 2) {
      size = 2.5;
    }
    target.style.fontSize = `${size.toFixed(1)}rem`;
  }
}