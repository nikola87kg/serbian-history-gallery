import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintscreenTagPeopleDialogComponent } from './printscreen-tag-people-dialog.component';

describe('BirthdayDialogComponent', () => {
  let component: PrintscreenTagPeopleDialogComponent;
  let fixture: ComponentFixture<PrintscreenTagPeopleDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrintscreenTagPeopleDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PrintscreenTagPeopleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
