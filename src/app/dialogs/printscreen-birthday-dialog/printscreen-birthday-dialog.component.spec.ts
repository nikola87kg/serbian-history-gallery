import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintscreenBirthdayDialogComponent } from './printscreen-birthday-dialog.component';

describe('BirthdayDialogComponent', () => {
  let component: PrintscreenBirthdayDialogComponent;
  let fixture: ComponentFixture<PrintscreenBirthdayDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PrintscreenBirthdayDialogComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PrintscreenBirthdayDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
