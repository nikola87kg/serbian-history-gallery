import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Inject
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { DefaultLogoDirective } from '@app/directives/default-logo.directive';
import { CategoriesService, Category } from '@app/services/categories.service';
import { Epoch, EpochsService } from '@app/services/epochs.service';
import { Person } from '@app/services/persons.service';
import { ThemeService } from '@app/services/theme.service';
import { UtilsService } from '@app/services/utils.service';

@Component({
  templateUrl: './printscreen-birthday-dialog.component.html',
  styleUrl: './printscreen-birthday-dialog.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule, MatButtonModule, MatIconModule,
    DefaultLogoDirective]
})
export class PrintscreenBirthdayDialogComponent {
  private cdr = inject(ChangeDetectorRef);
  private epochsService = inject(EpochsService);
  private categoriesService = inject(CategoriesService);

  public items!: Person[];
  public categories: Category[] = [];
  public epochs: Epoch[] = [];
  public themeService = inject(ThemeService);
  public showLogo = true;

  getYearColor(item: Person) {
    return item.age ? 'red' : 'green'
  }

  getDeathYear(item: Person) {
    if (!item.age) {
      return;
    }

    const death = item.birthDeath?.split(' - ')[1];

    if (!death) {
      return;
    }
    if (death?.length === 4) {
      return `-${death}`;
    }

    const deathYear = death.split('.')[2];
    return `-${deathYear}`;
  }

  get birthday(): string {
    const person = this.items[0];
    if (!person) {
      return '';
    }
    const day = person.birthDay;

    const month = UtilsService.getMonthTitleFromOrder('' + person.birthMonth);
    if (!day || !month) {
      return '';
    }
    return `${day}. ${month}`;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { items: Person[]; },
    public dialogRef: MatDialogRef<PrintscreenBirthdayDialogComponent>
  ) {
    this.items = [...data.items];
  }

  ngOnInit() {
    this.handleCategories();
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.categories = [...response];
        this.cdr.detectChanges();
      },
    });
    this.epochsService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.epochs = [...response];
        this.cdr.detectChanges();
      },
    });
  }

  onRemove(item: Person) {
    if (this.items.length === 1) {
      return;
    }
    const index = this.items.findIndex(i => i.id === item.id);
    if (index > -1) {
      this.items.splice(index, 1);
    }
  }

  getColorByIndex(i: number): string {
    const colorMap: any = {
      0: '#faa',
      1: '#c0d5ff',
      2: '#f2ffc7',
      3: '#b2fdd8',
    };

    return colorMap[i];
  }

  extractDate(birthDeath: string): string {
    return birthDeath.split('.')[0] + '.';
  }

  extractMonth(birthDeath: string): string {
    return birthDeath.split('.')[1] + '.';
  }

  extractRest(birthDeath: string): string {
    const parts = birthDeath.split('.');
    parts.shift();
    parts.shift();
    return parts.join('.');
  }
}
