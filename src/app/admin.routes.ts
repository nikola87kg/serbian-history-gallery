

import { AdminPanelComponent } from './pages/admin-panel/admin-panel.component';
import { ArtFormComponent } from './pages/admin-panel/art-form/art-form.component';
import { ArtListComponent } from './pages/admin-panel/art-list/art-list.component';
import { BattleFormComponent } from './pages/admin-panel/battle-form/battle-form.component';
import { BattleListComponent } from './pages/admin-panel/battle-list/battle-list.component';
import { CategoryFormComponent } from './pages/admin-panel/category-form/category-form.component';
import { CategoryListComponent } from './pages/admin-panel/category-list/category-list.component';
import {
  ChronologyFormComponent
} from './pages/admin-panel/chronology-form/chronology-form.component';
import {
  ChronologyListComponent
} from './pages/admin-panel/chronology-list/chronology-list.component';
import { ChurchFormComponent } from './pages/admin-panel/church-form/church-form.component';
import { ChurchListComponent } from './pages/admin-panel/church-list/church-list.component';
import { EpochFormComponent } from './pages/admin-panel/epoch-form/epoch-form.component';
import { EpochListComponent } from './pages/admin-panel/epoch-list/epoch-list.component';
import {
  HistoricalEventFormComponent
} from './pages/admin-panel/historical-event-form/historical-event-form.component';
import {
  HistoricalEventListComponent
} from './pages/admin-panel/historical-event-list/historical-event-list.component';
import {
  InstagramFormComponent
} from './pages/admin-panel/instagram-form/instagram-form.component';
import {
  InstagramListComponent
} from './pages/admin-panel/instagram-list/instagram-list.component';
import {
  ManifestationFormComponent
} from './pages/admin-panel/manifestation-form/manifestation-form.component';
import {
  ManifestationListComponent
} from './pages/admin-panel/manifestation-list/manifestation-list.component';
import { MemorialFormComponent } from './pages/admin-panel/memorial-form/memorial-form.component';
import { MemorialListComponent } from './pages/admin-panel/memorial-list/memorial-list.component';
import { MessageListComponent } from './pages/admin-panel/message-list/message-list.component';
import { NatureFormComponent } from './pages/admin-panel/nature-form/nature-form.component';
import { NatureListComponent } from './pages/admin-panel/nature-list/nature-list.component';
import { NoteFormComponent } from './pages/admin-panel/note-form/note-form.component';
import { NoteListComponent } from './pages/admin-panel/note-list/note-list.component';
import { PersonFormComponent } from './pages/admin-panel/person-form/person-form.component';
import { PersonListComponent } from './pages/admin-panel/person-list/person-list.component';
import { QuoteFormComponent } from './pages/admin-panel/quote-form/quote-form.component';
import { QuoteListComponent } from './pages/admin-panel/quote-list/quote-list.component';
import { SaintFormComponent } from './pages/admin-panel/saint-form/saint-form.component';
import { SaintListComponent } from './pages/admin-panel/saint-list/saint-list.component';
import { SongFormComponent } from './pages/admin-panel/song-form/song-form.component';
import { SongListComponent } from './pages/admin-panel/song-list/song-list.component';
import { SymbolFormComponent } from './pages/admin-panel/symbol-form/symbol-form.component';
import { SymbolListComponent } from './pages/admin-panel/symbol-list/symbol-list.component';
import { TagFormComponent } from './pages/admin-panel/tag-form/tag-form.component';
import { TagListComponent } from './pages/admin-panel/tag-list/tag-list.component';
import { UserListComponent } from './pages/admin-panel/user-list/user-list.component';

export const ADMIN_ROUTES = [
  {
    path: 'panel',
    component: AdminPanelComponent,
    children: [
      { path: 'category-list', component: CategoryListComponent },
      { path: 'category-form', component: CategoryFormComponent },
      { path: 'category-form/:id', component: CategoryFormComponent },

      { path: 'epoch-list', component: EpochListComponent },
      { path: 'epoch-form', component: EpochFormComponent },
      { path: 'epoch-form/:id', component: EpochFormComponent },

      { path: 'person-list', component: PersonListComponent },
      { path: 'person-form', component: PersonFormComponent },
      { path: 'person-form/:id', component: PersonFormComponent },

      { path: 'tag-list', component: TagListComponent },
      { path: 'tag-form', component: TagFormComponent },
      { path: 'tag-form/:id', component: TagFormComponent },

      { path: 'quote-list', component: QuoteListComponent },
      { path: 'quote-form', component: QuoteFormComponent },
      { path: 'quote-form/:id', component: QuoteFormComponent },

      { path: 'song-list', component: SongListComponent },
      { path: 'song-form', component: SongFormComponent },
      { path: 'song-form/:id', component: SongFormComponent },

      { path: 'battle-list', component: BattleListComponent },
      { path: 'battle-form', component: BattleFormComponent },
      { path: 'battle-form/:id', component: BattleFormComponent },

      { path: 'church-list', component: ChurchListComponent },
      { path: 'church-form', component: ChurchFormComponent },
      { path: 'church-form/:id', component: ChurchFormComponent },

      { path: 'saint-list', component: SaintListComponent },
      { path: 'saint-form', component: SaintFormComponent },
      { path: 'saint-form/:id', component: SaintFormComponent },

      { path: 'symbol-list', component: SymbolListComponent },
      { path: 'symbol-form', component: SymbolFormComponent },
      { path: 'symbol-form/:id', component: SymbolFormComponent },

      { path: 'art-list', component: ArtListComponent },
      { path: 'art-form', component: ArtFormComponent },
      { path: 'art-form/:id', component: ArtFormComponent },

      { path: 'manifestation-list', component: ManifestationListComponent },
      { path: 'manifestation-form', component: ManifestationFormComponent },
      { path: 'manifestation-form/:id', component: ManifestationFormComponent },

      { path: 'memorial-list', component: MemorialListComponent },
      { path: 'memorial-form', component: MemorialFormComponent },
      { path: 'memorial-form/:id', component: MemorialFormComponent },

      { path: 'nature-list', component: NatureListComponent },
      { path: 'nature-form', component: NatureFormComponent },
      { path: 'nature-form/:id', component: NatureFormComponent },

      { path: 'chronology-list', component: ChronologyListComponent },
      { path: 'chronology-form', component: ChronologyFormComponent },
      { path: 'chronology-form/:id', component: ChronologyFormComponent },

      { path: 'event-list', component: HistoricalEventListComponent },
      { path: 'event-form', component: HistoricalEventFormComponent },
      { path: 'event-form/:id', component: HistoricalEventFormComponent },

      { path: 'instagram-list', component: InstagramListComponent },
      { path: 'instagram-form', component: InstagramFormComponent },
      { path: 'instagram-form/:id', component: InstagramFormComponent },

      { path: 'note-list', component: NoteListComponent },
      { path: 'note-form', component: NoteFormComponent },
      { path: 'note-form/:id', component: NoteFormComponent },

      { path: 'message-list', component: MessageListComponent },
      { path: 'user-list', component: UserListComponent },

      { path: '**', redirectTo: '/admin/panel/note-list' },
    ],
  },

  { path: '**', redirectTo: '/panel' },
];