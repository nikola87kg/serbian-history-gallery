import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'slugToId', standalone: true })
@Injectable({ providedIn: 'root' })
export class SlugToIdPipe implements PipeTransform {
  transform(slug: string, collection: any[]): string {
    const element = collection.find((c) => c.slug === slug);
    return element?.id;
  }
}
