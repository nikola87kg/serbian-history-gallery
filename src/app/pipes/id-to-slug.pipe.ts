import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'idToSlug', standalone: true })
@Injectable({ providedIn: 'root' })
export class IdToSlugPipe implements PipeTransform {
  transform(id: string, collection: any[]): string {
    const element = collection.find((c) => c.id === id);
    return element?.slug;
  }
}
