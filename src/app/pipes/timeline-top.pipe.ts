import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timelineTop',
  standalone: true
})
export class TimelineTopPipe implements PipeTransform {

  transform(items: any, index: any): string {
    const item = items[index];
    const startYear = items[0].top;
    const last = items.length - 1;
    const endYear = items[last].top;
    let value: number;

    value = (item.top - startYear) / (item.minDistance + endYear - startYear);


    const round = Math.round(value * 10000) / 100;
    return round + '%';
  }

}
