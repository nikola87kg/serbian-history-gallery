import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'idToColor', standalone: true })
@Injectable({ providedIn: 'root' })
export class IdToColorPipe implements PipeTransform {
  transform(id: string | null, collection: any[]): string {
    const element = collection.find((c) => c.id === id);
    return element?.color;
  }
}
