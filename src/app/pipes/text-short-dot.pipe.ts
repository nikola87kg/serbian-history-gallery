import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'textShortDot',
  standalone: true
})
export class TextShortDotPipe implements PipeTransform {

  transform(value: any, charCount = 200): string {
    value = String(value);

    let dotIndex = -1;
    for (let i = 0; i < charCount && i < value.length; i++) {
      const char = value[i];
      if (char === '.') {
        dotIndex = i;
      }
    }

    if (dotIndex !== -1) {
      const subValue = value.substring(0, dotIndex + 1);
      return subValue;
    }

    return value;
  }

}
