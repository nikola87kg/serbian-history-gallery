import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gradientByName',
  standalone: true
})
export class GradientByNamePipe implements PipeTransform {
  maxNameCount = 50;
  maxPercentage = 85;

  transform(name: string,
    left = 'var(--canva-red)',
    right = 'var(--canva-blue)', hover = false): string {
    name = name.trim();

    const nameCount = Math.min(name.length, this.maxNameCount);
    const spaceCount = countSpaces(name);

    const percentageCandidate = Math.ceil(nameCount * 3) - (spaceCount / 2);
    const percentage = Math.min(percentageCandidate, this.maxPercentage);

    if (hover) {
      return left;
    }

    return `linear-gradient(125deg, 
      ${left} 0%,
      ${left} ${percentage}%,
      ${right} ${percentage + 20}%,
      ${right} 100%)`;
  }
}

function countSpaces(str: string): number {
  let count = 0;
  for (let i = 0; i < str.length; i++) {
    if (str[i] === ' ') {
      count++;
    }
  }
  return count;
}