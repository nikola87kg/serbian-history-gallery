import { Injectable, Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'idToName', standalone: true })
@Injectable({ providedIn: 'root' })
export class IdToNamePipe implements PipeTransform {
  transform(id: string | null, collection: any[]): string {
    const element = collection.find((c) => c.id === id);
    return element?.name;
  }
}
