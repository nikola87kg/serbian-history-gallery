import { Pipe, PipeTransform } from '@angular/core';
import { MiniBox } from '@app/components/mini-box/mini-box.component';

@Pipe({
  name: 'miniBoxParser',
  standalone: true
})
export class MiniBoxParserPipe implements PipeTransform {

  transform(input: any, index: number = 0, imagePrefix = ''): MiniBox {

    const miniBox: MiniBox = {
      style: input.style || 'box-' + ((index % 5) + 1),
      background: input.background || (imagePrefix + input.image),
      link: input.link || input.path || input.slug,
      title: input.title || input.name || input.commonName,
      icon: input.icon,
      imagePath: input.imagePath,
      elements: input.elements,
    };
    return miniBox;
  }

}
