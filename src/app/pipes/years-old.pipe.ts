import { Pipe, PipeTransform } from '@angular/core';
import { Person } from '@app/services/persons.service';

@Pipe({
  name: 'yearsOld',
  standalone: true,
})
export class YearsOldPipe implements PipeTransform {
  transform(
    age: any = 0,
    person: Person,
    hideText = false): any {
    const year = person.birthYear;
    const month = person.birthMonth;
    const day = person.birthDay;
    if (!age && year) {
      const currentYear = new Date().getFullYear();
      const currenMonth = new Date().getMonth() + 1;
      const currentDay = new Date().getDate();

      age = currentYear - year;

      const fullBirthday = month && day;
      const monthInFuture = month && month > currenMonth;
      const sameMonth = month === currenMonth;
      const dayInFuture = sameMonth && day && day > currentDay;

      if (fullBirthday && monthInFuture || dayInFuture) {
        age -= 1;
      }
    }

    let ageString = '';
    const lastDigit = age % 10;


    if (lastDigit > 1 && lastDigit < 5) {
      ageString = `${age} године`;
    } else {
      ageString = `${age} година`;
    }


    if (hideText) {
      ageString = `${age}`;
    }

    return ageString;
  }
}
