import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderToName',
  standalone: true
})
export class OrderToNamePipe implements PipeTransform {
  transform(order: string | null, collection: any[]): string {
    const element = collection.find((c) => c.order === order);
    return element?.name;
  }
}
