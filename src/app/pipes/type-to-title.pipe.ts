import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { churchTypes } from '@app/services/church.service';
import { manifestationTypes } from '@app/services/manifestations.service';
import { memorialTypes } from '@app/services/memorial.service';
import { natureTypes } from '@app/services/nature.service';

@Pipe({ name: 'typeToTitle', standalone: true })
@Injectable({ providedIn: 'root' })
export class TypeToTitlePipe implements PipeTransform {
  types = [
    ...memorialTypes,
    ...manifestationTypes,
    ...natureTypes,
    ...churchTypes,
  ];

  transform(typeId: string | null): string {
    const type = this.types.find(t => t.id === typeId);
    return type?.name || typeId || '';
  }

}
