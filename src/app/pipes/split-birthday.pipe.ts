import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitBirthday',
  standalone: true
})
export class SplitBirthdayPipe implements PipeTransform {

  transform(birthDeath: string | null): string[] {
    if (!birthDeath) {
      return ['', ''];
    }

    const dashSplitted = birthDeath.split(' - ');
    const born = dashSplitted[0];
    const dead = dashSplitted[1];
    const hasFullBornDate = born.includes('.');

    if (!hasFullBornDate) {
      return ['', birthDeath];
    }

    const dotSplitted = born.split('.');

    if (dotSplitted.length === 3) {
      const day = dotSplitted[0];
      const month = dotSplitted[1];
      const year = dotSplitted[2];
      const restOfDate = dead
        ? `${year} - ${dead}`
        : year;
      return [`${day}.${month}.`, restOfDate];
    }

    return ['', birthDeath];

  }

}
