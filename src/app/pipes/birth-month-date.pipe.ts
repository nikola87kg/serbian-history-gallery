import { Pipe, PipeTransform } from '@angular/core';
import { UtilsService } from '@app/services/utils.service';

@Pipe({
  name: 'birthMonthDate',
  standalone: true
})
export class BirthMonthDatePipe implements PipeTransform {

  transform(input: string | null | undefined): string {
    if (!input) {
      return '';
    }

    const splitDates = input.split(' - ');
    const born = splitDates[0];
    const death = splitDates[1];

    if (!death) {
      return this.convertMonth(born);
    }

    const bornConverted = this.convertMonth(born);
    const deathConverted = this.convertMonth(death);

    return `${bornConverted} - ${deathConverted}`;
  }

  convertMonth(date: string) {
    if (date.includes('око')) {
      return date;

    } else if (!date.includes('.')) {
      return date;
    }

    const splittedBord = date.split('.');
    const day = +splittedBord[0];
    const monthOrder = +splittedBord[1];
    const month = UtilsService.getMonthTitleFromOrder(String(monthOrder));
    const year = splittedBord[2] ? +splittedBord[2] : '';
    return `${day}. ${month} ${year}`;
  }
}


