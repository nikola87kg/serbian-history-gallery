import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'removeHtmlTags',
  standalone: true
})
export class RemoveHtmlTagsPipe implements PipeTransform {

  transform(myHTML: string): string {
    var strippedHtml = myHTML.replace(/<[^>]+>/g, '');
    return strippedHtml;
  }

}
