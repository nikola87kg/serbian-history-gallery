import { Pipe, PipeTransform } from '@angular/core';
import { Person } from '@services/persons.service';

@Pipe({
  name: 'counter',
  standalone: true
})
export class CounterPipe implements PipeTransform {

  transform(item: any, personList: Person[]): number {

    if (item.source) {
      const source: string = item.source;
      const sourcePersons = personList.filter((person: any) => !!person[source]);
      return sourcePersons.length;
    }

    const tagPersons = personList.filter(person => person.tags?.includes(item.id!));
    return tagPersons.length;
  }
}
