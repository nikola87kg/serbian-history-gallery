import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'thumbnail',
  standalone: true,
})
export class ThumbnailPipe implements PipeTransform {
  transform(imagePath: any): string {
    if (!imagePath) {
      return '';
    }

    const splitter1 = '%2F';
    const splitter2 = '.';
    const pieces = imagePath.split(splitter1);
    const url = pieces[0];
    const newPieces = pieces[1].split(splitter2);
    const name = newPieces[0];
    const extension = newPieces[1];
    const thumbnailPath = '%2Fthumbnails';
    const thumbnailSuffix = '_200x200';
    const path = `${url}${thumbnailPath}${splitter1}${name}${thumbnailSuffix}${splitter2}${extension}`;
    return path;
  }
}
