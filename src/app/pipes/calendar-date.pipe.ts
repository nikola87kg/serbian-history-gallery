import { Pipe, PipeTransform } from '@angular/core';
import { UtilsService } from '@app/services/utils.service';

@Pipe({
  name: 'calendarDate',
  standalone: true
})
export class CalendarDatePipe implements PipeTransform {

  transform(input: string | null | undefined): string {
    if (!input) {
      return '';
    }

    const splitted = input?.split('.');
    const day = +splitted[0];

    const monthOrder = +splitted[1];
    const month = UtilsService.getMonthTitleFromOrder(String(monthOrder));
    const output = `${day}. ${month.toLowerCase()}`;
    return output;
  }

}
