import { CommonModule, ViewportScroller } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatIconModule } from '@angular/material/icon';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { CollapserComponent } from '@app/components/collapser/collapser.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@app/services/auth.service';
import { Instagram, InstagramService, instragramTypes } from '@app/services/instagram.service';
import { UtilsService } from '@app/services/utils.service';

@Component({
  selector: 'app-instagram-page',
  imports: [TranslitPipe, MatIconModule, CommonModule, CollapserComponent],
  templateUrl: './instagram-page.component.html',
  styleUrl: './instagram-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstagramPageComponent {
  private instagramService = inject(InstagramService);
  private destroyRef = inject(DestroyRef);
  private sanitizer = inject(DomSanitizer);
  private cdr = inject(ChangeDetectorRef);
  private authService = inject(AuthService);
  private router = inject(Router);
  private isAdmin = false;
  private route = inject(ActivatedRoute);
  private routeSection: string | null = null;
  private scroller = inject(ViewportScroller);

  public maxIndexInstagram = 3;
  public instragramTypes = instragramTypes;
  public sanitizedPosts: SafeHtml[] = [];
  public postGroups: any[] = [];

  ngOnInit(): void {
    this.scroller.setOffset([0, 100]);

    this.route.fragment.subscribe(fragment => {
      const type = instragramTypes.find(t => t.link === fragment);
      this.routeSection = type?.order || null;
    });

    this.checkAdmin();
    this.getInstagramPosts();
  }

  checkAdmin(): void {
    this.authService.userProfile$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe(user => this.isAdmin = !!user?.isAdmin);
  }

  onPost(post: Instagram) {
    const dropPrefix = post.embeddedCode!.split('https')[1];
    const final = 'https' + dropPrefix!.split('?utm_source')[0];
    window.open(final!, '_blank');
  }

  onContextMenu($event: MouseEvent, postId: string) {
    $event.stopPropagation();
    $event.preventDefault();
    if (this.isAdmin) {
      this.router.navigateByUrl(`admin/panel/instagram-form/${postId}`);
    }
  }

  getInstagramPosts() {
    /* const link = this.router.url.split('/instagram#')[1]; */

    this.instagramService.getItemsSorted('created', 'desc')
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Instagram[]) => {
          const sanitizedPosts = response.map((instagram) => {
            const type = instragramTypes.find(t => t.id === instagram.type);
            return {
              ...instagram,
              typeName: type.name,
              minimized: false, //link && link !== type.link,
              order: type.order,
              safeUrl: this.getSafeInstagramEmbedCode(instagram.embeddedCode || '')
            };
          });

          sanitizedPosts.sort((a, b) => {
            if (a.order !== b.order) {
              return a.order - b.order;
            }

            return a.title!.localeCompare(b.title!);
          });
          this.postGroups = UtilsService.groupBy(sanitizedPosts, 'type');
          this.cdr.detectChanges();
          setTimeout(() => {
            this.scrollToFragment();
          }, 100);
        }
      });
  }

  scrollToFragment(): void {
    if (this.routeSection) {
      const fragmentId = 'section' + this.routeSection;
      const element = document.getElementById(fragmentId);
      if (element) {
        this.scroller.scrollToAnchor(fragmentId);
        this.routeSection = null;  // Prevent re-scrolling
      }
    }
  }


  getSafeInstagramEmbedCode(post: string): SafeHtml {
    return this.sanitizer.bypassSecurityTrustHtml(post);
  }

  onLoadMore() {
    this.maxIndexInstagram = this.maxIndexInstagram + 3;
  }


}
