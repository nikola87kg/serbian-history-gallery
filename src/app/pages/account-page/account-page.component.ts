
import { AsyncPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { ThemeService } from '@app/services/theme.service';
import { DefaultLogoDirective } from '@directives/default-logo.directive';
import { AuthService } from '@services/auth.service';
import { UtilsService } from '@services/utils.service';

@Component({
    selector: 'app-account-page',
    imports: [AsyncPipe, MatButtonModule, DefaultLogoDirective, TranslitPipe, MatIconModule],
    templateUrl: './account-page.component.html',
    styleUrls: ['./account-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountPageComponent {
  private router = inject(Router);

  public authService = inject(AuthService);
  public themeService = inject(ThemeService);

  public innerWidth!: number;
  public innerHeight!: number;
  public navigator: any = navigator;
  public browser = window;

  ngOnInit() {
    UtilsService.scrollTop();
  }

  onFavourites() {
    this.router.navigateByUrl('favoriti');
  }
}
