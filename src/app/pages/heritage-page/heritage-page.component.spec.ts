import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { ActivatedRoute } from '@angular/router';

import { HeritagePageComponent } from './heritage-page.component';

describe('HeritagePageComponent', () => {
  let component: HeritagePageComponent;
  let fixture: ComponentFixture<HeritagePageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HeritagePageComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
        { provide: ActivatedRoute, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(HeritagePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
