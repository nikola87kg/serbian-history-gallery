

import { map, Observable } from 'rxjs';

import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { CommonModule, Location, ViewportScroller } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject, ViewChild } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { MatBottomSheet, MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatTabGroup, MatTabsModule } from '@angular/material/tabs';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ChurchCardComponent } from '@app/components/church-card/church-card.component';
import { MemorialCardComponent } from '@app/components/memorial-card/memorial-card.component';
import { SaintCardComponent } from '@app/components/saint-card/saint-card.component';
import { SymbolCardComponent } from '@app/components/symbol-card/symbol-card.component';
import { BasicSheetComponent } from '@app/dialogs/basic-sheet/basic-sheet.component';
import {
  PrintscreenCitySightsDialogComponent
} from '@app/dialogs/printscreen-city-sights-dialog/printscreen-city-sights-dialog.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import {
  ManifestationNames, ManifestationPaths, ManifestationService, ManifestationType
} from '@app/services/manifestations.service';
import { NatureNames, NaturePaths, NatureService, NatureType } from '@app/services/nature.service';
import { ThemeService } from '@app/services/theme.service';
import { AuthService } from '@services/auth.service';
import {
  ChurchNames, ChurchPaths, ChurchService, ChurchType, heritageBoxes, HeritageUrls
} from '@services/church.service';
import {
  MemorialNames, MemorialPaths, MemorialService, MemorialType, memorialTypes
} from '@services/memorial.service';
import { HolidayType, holidayTypes, SaintsService } from '@services/saints.service';
import { SymbolNames, SymbolsService, SymbolType } from '@services/symbols.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-heritage-page',
  imports: [CommonModule, MatTabsModule, MatBottomSheetModule,
    MatButtonModule, MatIconModule, TranslitPipe,
    ChurchCardComponent, MemorialCardComponent, SaintCardComponent,
    SymbolCardComponent, MatButtonToggleModule, FormsModule],
  templateUrl: './heritage-page.component.html',
  styleUrls: ['./heritage-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeritagePageComponent {
  @ViewChild(MatTabGroup) tabGroup!: MatTabGroup;
  private router = inject(Router);
  private activatedRoute = inject(ActivatedRoute);
  private destroyRef = inject(DestroyRef);
  private matBottomSheet = inject(MatBottomSheet);

  private churchService = inject(ChurchService);
  private memorialService = inject(MemorialService);
  private themeService = inject(ThemeService);
  private manifestationService = inject(ManifestationService);
  private natureService = inject(NatureService);
  private saintsService = inject(SaintsService);
  private symbolsService = inject(SymbolsService);
  private scroller = inject(ViewportScroller);
  private location = inject(Location);
  private matDialog = inject(MatDialog);
  private previousUrl = '';

  public optionPath = '';
  public currentOption: any;
  public boxPath = '';
  public currentBox: any;
  public currentStep = 0;
  public HeritageUrls = HeritageUrls;
  public authService = inject(AuthService);

  public items$!: Observable<any[]>;
  public symbolGroups: any[] = [];
  public holidayTypes: any[] = holidayTypes;
  public memorialTypes = memorialTypes;

  public get imagePrefix(): string {
    return this.themeService.isDarkMode()
      ? `radial-gradient(circle, rgb(0 0 0 / 85%) 0%, black 95%), linear-gradient(#353535, #222222), `
      : `radial-gradient(circle, rgb(255 255 255 / 80%) 0%, white 95%), linear-gradient(#c8c8c8, #c8c8c8), `;
  };

  public heritageBoxes = heritageBoxes;

  public pathOptions = [
    { box: HeritageUrls.nature, path: NaturePaths.park, title: NatureNames.park },
    { box: HeritageUrls.nature, path: NaturePaths.mountain, title: NatureNames.mountain },
    { box: HeritageUrls.nature, path: NaturePaths.river, title: NatureNames.river },
    { box: HeritageUrls.nature, path: NaturePaths.water, title: NatureNames.water },
    { box: HeritageUrls.nature, path: NaturePaths.cave, title: NatureNames.cave },
    { box: HeritageUrls.nature, path: NaturePaths.spa, title: NatureNames.spa },
    { box: HeritageUrls.nature, path: NaturePaths.zoo, title: NatureNames.zoo },
    { box: HeritageUrls.nature, path: NaturePaths.etno, title: NatureNames.etno },

    { box: HeritageUrls.churches, path: ChurchPaths.monastery, title: ChurchNames.monastery, },
    { box: HeritageUrls.churches, path: ChurchPaths.church, title: ChurchNames.church, },
    { box: HeritageUrls.churches, path: ChurchPaths.other, title: ChurchNames.other, },

    { box: HeritageUrls.symbols, path: 'drzavne-zastave', title: SymbolNames.flag, },
    { box: HeritageUrls.symbols, path: 'drzavni-grbovi', title: SymbolNames.coat, },
    { box: HeritageUrls.symbols, path: 'drzavne-mape', title: SymbolNames.map, },
    { box: HeritageUrls.symbols, path: 'drzavne-himne', title: SymbolNames.anthem, },
    { box: HeritageUrls.symbols, path: 'biljke-i-zivotinje', title: SymbolNames.nature, },
    { box: HeritageUrls.symbols, path: 'automobili', title: SymbolNames.cars, },
    { box: HeritageUrls.symbols, path: 'narodne-igre', title: SymbolNames.dances, },
    { box: HeritageUrls.symbols, path: 'drzavni-novac', title: SymbolNames.valute, },
    { box: HeritageUrls.symbols, path: 'nacionalna-kuhinja', title: SymbolNames.food, },
    { box: HeritageUrls.symbols, path: 'stvari-i-predmeti', title: SymbolNames.stuff, },

    { box: HeritageUrls.saints, path: 'slave', title: 'Славе', },
    { box: HeritageUrls.saints, path: 'verski-praznici', title: 'Верски празници', },
    { box: HeritageUrls.saints, path: 'drzavni-praznici', title: 'Државни празници', },
    { box: HeritageUrls.saints, path: 'medjunarodni-praznici', title: 'Међународни празници', },

    { box: HeritageUrls.memorials, path: MemorialPaths.building, title: MemorialNames.building, },
    { box: HeritageUrls.memorials, path: MemorialPaths.museum, title: MemorialNames.museum, },
    { box: HeritageUrls.memorials, path: MemorialPaths.palace, title: MemorialNames.palace, },
    { box: HeritageUrls.memorials, path: MemorialPaths.location, title: MemorialNames.location, },
    { box: HeritageUrls.memorials, path: MemorialPaths.monument, title: MemorialNames.monument, },
    { box: HeritageUrls.memorials, path: MemorialPaths.sports, title: MemorialNames.sports, },
    { box: HeritageUrls.memorials, path: MemorialPaths.streets, title: MemorialNames.streets, },

    { box: HeritageUrls.manifestations, path: ManifestationPaths.culture, title: ManifestationNames.culture, },
    { box: HeritageUrls.manifestations, path: ManifestationPaths.sport, title: ManifestationNames.sport, },
    { box: HeritageUrls.manifestations, path: ManifestationPaths.tavern, title: ManifestationNames.tavern, },
    { box: HeritageUrls.manifestations, path: ManifestationPaths.fair, title: ManifestationNames.fair, },
  ];

  ngOnInit() {
    UtilsService.scrollTop();
    this.scroller.setOffset([0, 100]);
    this.handleSlug();
    this.listenRouteChanges();
  }

  listenRouteChanges() {
    this.router.events
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          if (!this.router.url.includes('#') && !this.previousUrl.includes('#')) {
            UtilsService.scrollTop();
          }

          this.previousUrl = this.router.url;
          this.handleSlug();
        }
      });
  }

  handleSlug() {
    const slug = this.activatedRoute.snapshot?.paramMap?.get('slug');
    const option = this.activatedRoute.snapshot?.paramMap?.get('option');
    this.boxPath = slug || '';
    if (this.boxPath) {
      this.currentBox = this.heritageBoxes.find(b => b.path === this.boxPath);
    }
    this.optionPath = option || '';
    if (this.optionPath) {
      this.currentOption = this.pathOptions.find(b => b.path === this.optionPath);
    }
    this.currentStep = option ? 3 : slug ? 2 : 1;

    if (this.currentStep === 3) {
      this.getData();
    }

  }

  setStep2(boxPath: string) {
    this.router.navigateByUrl(`srpsko-nasledje/${boxPath}`);
  }

  setStep3(optionPath: string) {
    this.router.navigateByUrl(`srpsko-nasledje/${this.boxPath}/${optionPath}`);
  }

  scrollTo(elementId: string) {
    this.scroller.scrollToAnchor(elementId);
  }

  getData() {
    switch (this.currentOption?.box) {
      case HeritageUrls.churches: this.getChurches(); break;
      case HeritageUrls.memorials: this.getMemorials(); break;
      case HeritageUrls.manifestations: this.getManifestations(); break;
      case HeritageUrls.nature: this.getNature(); break;
      case HeritageUrls.saints: this.getSaints(); break;
      case HeritageUrls.symbols: this.getSymbols(); break;
    }
  }

  getChurches() {
    let type = '';
    switch (this.currentOption?.path) {
      case ChurchPaths.church: type = ChurchType.church; break;
      case ChurchPaths.monastery: type = ChurchType.monastery; break;
      case ChurchPaths.other: type = ChurchType.other; break;
    }

    this.items$ = this.churchService.getItemsSorted('yearMonth')
      .pipe(
        map((response) => {
          const filteredItems = response.filter(e => e.type === type);
          return filteredItems;
        }
        )
      );;
  }

  getSaints() {
    let type = '';
    switch (this.currentOption?.path) {
      case 'drzavni-praznici': type = HolidayType.national; break;
      case 'medjunarodni-praznici': type = HolidayType.world; break;
      case 'verski-praznici': type = HolidayType.religious; break;
      case 'slave': type = HolidayType.saint; break;
    }

    this.items$ = this.saintsService.getItemsSorted('saintDate')
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        map((holidays) => {
          const fixHolidays = UtilsService.setMovableHolidays(holidays);
          const filteredItems = fixHolidays.filter(e => e.type === type);
          return filteredItems;
        }
        )
      );
  }

  getMemorials() {
    let type = '';
    switch (this.currentOption?.path) {
      case MemorialPaths.location: type = MemorialType.location; break;
      case MemorialPaths.streets: type = MemorialType.streets; break;
      case MemorialPaths.sports: type = MemorialType.sports; break;
      case MemorialPaths.monument: type = MemorialType.monument; break;
      case MemorialPaths.building: type = MemorialType.building; break;
      case MemorialPaths.museum: type = MemorialType.museum; break;
      case MemorialPaths.palace: type = MemorialType.palace; break;
    }

    this.items$ = this.memorialService.getItemsSorted('name')
      .pipe(
        map(response => {
          response.sort((a, b) => a.name!.localeCompare(b.name!));
          const filteredItems = response.filter(e => e.type === type);
          return filteredItems;
        }));
  }

  getManifestations() {
    let type = '';
    switch (this.currentOption?.path) {
      case ManifestationPaths.fair: type = ManifestationType.fair; break;
      case ManifestationPaths.culture: type = ManifestationType.culture; break;
      case ManifestationPaths.sport: type = ManifestationType.sport; break;
      case ManifestationPaths.tavern: type = ManifestationType.tavern; break;
    }

    this.items$ = this.manifestationService.getItemsSorted('name')
      .pipe(
        map(response => {
          response.sort((a, b) => a.name!.localeCompare(b.name!));
          const filteredItems = response.filter(e => e.type === type);
          return filteredItems;
        }));
  }

  getNature() {
    let type = '';
    switch (this.currentOption?.path) {
      case NaturePaths.mountain: type = NatureType.mountain; break;
      case NaturePaths.spa: type = NatureType.spa; break;
      case NaturePaths.cave: type = NatureType.cave; break;
      case NaturePaths.park: type = NatureType.park; break;
      case NaturePaths.water: type = NatureType.water; break;
      case NaturePaths.river: type = NatureType.river; break;
      case NaturePaths.etno: type = NatureType.etno; break;
      case NaturePaths.zoo: type = NatureType.zoo; break;
    }

    this.items$ = this.natureService.getItemsSorted('name')
      .pipe(
        map(response => {
          response.sort((a, b) => a.name!.localeCompare(b.name!));
          const filteredItems = response.filter(e => e.type === type);
          return filteredItems;
        }));
  }

  getSymbols() {
    let type = '';
    switch (this.currentOption?.path) {
      case 'drzavne-zastave': type = SymbolType.flag; break;
      case 'drzavni-grbovi': type = SymbolType.coat; break;
      case 'drzavne-himne': type = SymbolType.anthem; break;
      case 'drzavne-mape': type = SymbolType.map; break;
      case 'biljke-i-zivotinje': type = SymbolType.nature; break;
      case 'automobili': type = SymbolType.cars; break;
      case 'narodne-igre': type = SymbolType.dances; break;
      case 'drzavni-novac': type = SymbolType.valute; break;
      case 'nacionalna-kuhinja': type = SymbolType.food; break;
      case 'stvari-i-predmeti': type = SymbolType.stuff; break;
    }

    this.items$ = this.symbolsService.getItemsSorted('dateOrder')
      .pipe(
        map(response => {
          const filteredSymbols = response.filter(e => e.type === type);
          return filteredSymbols;
        }));
  }

  onLeader(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onAddButton() {
    const pageUrl = this.findAdminPage();
    this.router.navigateByUrl(`admin/panel/${pageUrl}`);
  }

  findAdminPage(): string {
    const key = this.boxPath;

    const keyMap: any = {
      [HeritageUrls.churches]: 'church-form',
      [HeritageUrls.symbols]: 'symbol-form',
      [HeritageUrls.saints]: 'saint-form',
      [HeritageUrls.nature]: 'nature-form',
      [HeritageUrls.memorials]: 'memorial-form',
      [HeritageUrls.manifestations]: 'manifestation-form',
    };

    return keyMap[key];
  }

  onItem(item: any, items: any[]): void {
    const data = { item, items };
    const sheet = BasicSheetComponent<any>;
    const panelClass = 'basic-sheet-overlay';
    const scrollStrategy = new NoopScrollStrategy();
    const disableClose = false;

    const href = this.router.url;
    this.location.go(href + '#');

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, panelClass, scrollStrategy, disableClose
    });

    sheetRef.afterDismissed().subscribe((onX: boolean) => {
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  }

  goToStep1() {
    this.router.navigateByUrl('srpsko-nasledje');
  }

  goToStep2() {
    this.router.navigateByUrl(`srpsko-nasledje/${this.boxPath}`);
  }

  onPrintscreenSights() {

    this.items$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response) => {
          const items = response.flat();

          this.matDialog.open(PrintscreenCitySightsDialogComponent, {
            data: { items },
            panelClass: 'social-dialog-container',
            scrollStrategy: new NoopScrollStrategy(),
          });

        }
      });
  }

}
