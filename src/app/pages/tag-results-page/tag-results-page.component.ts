import { GoogleAnalyticsService } from 'ngx-google-analytics';
import { combineLatest, first, map, Observable, take } from 'rxjs';

import { NoopScrollStrategy, ScrollStrategyOptions } from '@angular/cdk/overlay';
import { CommonModule, Location, ViewportScroller } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, HostListener, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import {
  FormControl, FormGroup, FormGroupDirective, FormsModule, ReactiveFormsModule
} from '@angular/forms';
import { MatBottomSheet, MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { ChurchCardComponent } from '@app/components/church-card/church-card.component';
import { CollapserComponent } from '@app/components/collapser/collapser.component';
import { MemorialCardComponent } from '@app/components/memorial-card/memorial-card.component';
import { PersonCardComponent } from '@app/components/person-card/person-card.component';
import { SerbianMapComponent } from '@app/components/serbian-map/serbian-map.component';
import { BasicSheetComponent } from '@app/dialogs/basic-sheet/basic-sheet.component';
import {
  MultiselectSheetComponent
} from '@app/dialogs/multiselect-sheet/multiselect-sheet.component';
import {
  PrintscreenCitySightsDialogComponent
} from '@app/dialogs/printscreen-city-sights-dialog/printscreen-city-sights-dialog.component';
import {
  PrintscreenCustomDialogComponent
} from '@app/dialogs/printscreen-custom-dialog/printscreen-custom-dialog.component';
import {
  PrintscreenTagPeopleDialogComponent
} from '@app/dialogs/printscreen-tag-people-dialog/printscreen-tag-people-dialog.component';
import { CalendarDatePipe } from '@app/pipes/calendar-date.pipe';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { CategoriesService, Category } from '@app/services/categories.service';
import { Instagram, InstagramService } from '@app/services/instagram.service';
import {
  ManifestationNames, ManifestationService, ManifestationType
} from '@app/services/manifestations.service';
import { NatureService, NatureType } from '@app/services/nature.service';
import { UtilsService } from '@app/services/utils.service';
import { SlugToIdPipe } from '@pipes/slug-to-id.pipe';
import { AuthService } from '@services/auth.service';
import { Church, ChurchService, ChurchType, HeritageUrls } from '@services/church.service';
import { Memorial, MemorialNames, MemorialService, MemorialType } from '@services/memorial.service';
import { Person, PersonsService } from '@services/persons.service';
import { Tag, TagGroup, TagsService } from '@services/tags.service';

enum View { cards = 'cards', list = 'list' }

@Component({
  selector: 'app-tag-results-page',
  imports: [CommonModule, FormsModule,
    MatBottomSheetModule, MatTabsModule, MatProgressSpinnerModule,
    MatIconModule, MatButtonModule, PersonCardComponent,
    ReactiveFormsModule, TranslitPipe, CollapserComponent,
    ChurchCardComponent, SerbianMapComponent, MemorialCardComponent,
  ],
  templateUrl: './tag-results-page.component.html',
  styleUrls: ['./tag-results-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [FormGroupDirective, CalendarDatePipe]
})
export class TagResultsPageComponent {
  private personService = inject(PersonsService);
  private tagsService = inject(TagsService);
  private churchService = inject(ChurchService);
  private manifestationService = inject(ManifestationService);
  private natureService = inject(NatureService);
  private memorialService = inject(MemorialService);
  private googleAnalyticsService = inject(GoogleAnalyticsService);
  private categoriesService = inject(CategoriesService);
  private instagramService = inject(InstagramService);

  private activatedRoute = inject(ActivatedRoute);
  private calendarDatePipe = inject(CalendarDatePipe);
  private slugtoIdPipe = inject(SlugToIdPipe);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private scroll = inject(ScrollStrategyOptions);
  private matBottomSheet = inject(MatBottomSheet);
  private location = inject(Location);
  private previousUrl = '';
  private matDialog = inject(MatDialog);

  public categories$!: Observable<Category[]>;
  public authService = inject(AuthService);
  public localHeroActive = false;

  public sameRegionCities: Tag[] = [];
  public View = View;
  public tag!: Tag;
  public allTags: Tag[] = [];
  public TagGroup = TagGroup;
  public totalList: Person[] = [];
  public cityPosts: Instagram[] = [];
  public generals: any[] = [];
  public filteredList: Person[] = [];
  public localHeroList: Person[] = [];
  public churches$!: Observable<Church[]>;
  public nature$!: Observable<any[]>;
  public memorials$!: Observable<Memorial[]>;
  public manifestations$!: Observable<any[]>;
  public persons$!: Observable<Person[]>;
  public HeritageUrls = HeritageUrls;
  public showSpinner = false;
  public selectedIndex!: number;
  public flattenSights: any[] = [];
  public selectedCategories: string[] = [];
  private scroller = inject(ViewportScroller);

  public MemorialType = MemorialType;
  public NatureType = NatureType;
  public ChurchType = ChurchType;
  public ManifestationType = ManifestationType;
  public MemorialNames = MemorialNames;
  public ManifestationNames = ManifestationNames;
  public sights: any;

  public form = new FormGroup({
    categoryIds: new FormControl<string[]>([]),
  });

  public get categoryControl() {
    return this.form.controls.categoryIds;
  }

  ngOnInit() {
    UtilsService.scrollTop();
    this.scroller.setOffset([0, 100]);
    this.handleCategoriesAndTags();
    this.listenRouteChanges();
  }

  addGoogleAnalytics(tag: string) {
    this.googleAnalyticsService.event(
      `Tag Page Action - ${tag}`,
      'Tag Page Init Category',
      `Tag Page - ${tag}`,
      Date.now(),
    );
  }

  tabToIndex(): number {
    const route = this.router.url;
    const splitRoute = route.split('/');
    const tab = splitRoute[3];
    let index = 0;
    switch (tab) {
      case 'podaci': index = 0; break;
      case 'licnosti': index = 1; break;
      case 'znamenitosti': index = 2; break;
    }

    return index;
  }

  @HostListener('document:keydown.arrowleft', ['$event'])
  onTabPrevious() {
    if (this.selectedIndex === 0) {
      return;
    }

    this.selectedIndex--;
  }

  @HostListener('document:keydown.arrowright', ['$event'])
  onTabNext() {
    if (this.selectedIndex === 2) {
      return;
    }

    this.selectedIndex++;
  }

  listenRouteChanges() {
    const tabIndex = this.tabToIndex();
    if (tabIndex !== this.selectedIndex) {
      this.onTabChange(tabIndex);
    }

    this.router.events
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((val) => {
        if (val instanceof NavigationEnd) {

          const tabIndex = this.tabToIndex();

          if (tabIndex !== this.selectedIndex && this.previousUrl === this.router.url) {
            this.onTabChange(tabIndex);
            return;
          }

          this.handleCategoriesAndTags();
          this.getInstagramPosts();

          const currentUrl = this.router.url;

          if (tabIndex !== this.selectedIndex) {
            this.onTabChange(tabIndex);
            return;
          }

          if (!currentUrl.includes('#')
            && !this.previousUrl.includes('#')) {
            UtilsService.scrollTop();
          }

          this.previousUrl = currentUrl;
        }
      });
  }

  getInstagramPosts() {
    this.instagramService.getItemsSorted('created', 'desc')
      .pipe(first(), takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Instagram[]) => {
          this.cityPosts = response.filter(p => p.slug!.includes(this.tag.slug!));
        }
      });
  }

  onPost(post: Instagram) {
    const dropPrefix = post.embeddedCode!.split('https')[1];
    const final = 'https' + dropPrefix!.split('?utm_source')[0];
    window.open(final!, '_blank');
  }

  goToPerson(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  goToTag(slug: string | null) {
    this.router.navigateByUrl(`tag/${slug}/podaci`);
  }

  onEdit(id: string | null | undefined, event: any) {
    event.stopPropagation();
    if (!id) {
      return;
    }

    this.router.navigateByUrl(`admin/panel/person-form/${id}`);
  }

  handleCategoriesAndTags(): void {
    this.categories$ = this.categoriesService.getItemsSorted('order');

    this.tagsService.getItemsSorted('name')
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (tagsResponse: any) => {
          const slug = this.activatedRoute.snapshot?.paramMap?.get('slug');
          this.addGoogleAnalytics(slug || 'no-slug');
          this.allTags = [...tagsResponse];
          const tag = this.allTags.find((c) => c.slug === slug);

          if (this.tag && this.tag.id === tag?.id) {
            return;
          }

          if (tag) {
            this.tag = tag;
            this.getInstagramPosts();
            this.setGenerals(tag);
            const id = this.slugtoIdPipe.transform(tag.slug!, this.allTags);

            this.getPersons(id);
            this.getHeritage(tag.name!);
            this.getSameRegionCities(tag);
          }
          this.cdr.detectChanges();
        },
      });
  }

  setGenerals(tag: Tag) {
    const generals: any[] = [];

    generals.push({ icon: 'extension', title: 'Управни округ', text: tag.region });
    generals.push({ icon: 'event', title: 'Дан града', text: this.calendarDatePipe.transform(tag.cityDate) });
    generals.push({ icon: 'groups', title: 'Популација', text: tag.population });
    generals.push({ icon: '360', title: 'Површина', text: tag.area + ' км2' });
    generals.push({ icon: 'landscape', title: 'Надморска висина', text: tag.height + ' метара' });
    generals.push({ icon: 'local_post_office', title: 'Поштански број', text: tag.postNumber });
    generals.push({ icon: 'call', title: 'Позивни број', text: tag.callPrefix });
    generals.push({ icon: 'directions_car', title: 'Регистарске таблице', text: tag.licencePlate });

    this.generals = generals;
  }

  getPersons(id: string) {
    this.persons$ = this.personService.getItemsQuery('tags', 'array-contains', id);

    this.showSpinner = true;
    this.persons$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (personResponse: any) => {
          this.showSpinner = false;
          personResponse.sort((a: Person, b: Person) => {
            if (a.birthYear === b.birthYear && a.birthDay && b.birthDay
              && a.birthMonth === b.birthMonth) {
              return (a.birthDay as number) - (b.birthDay as number);
            }
            if (a.birthYear === b.birthYear && a.birthMonth && b.birthMonth) {
              return (a.birthMonth as number) - (b.birthMonth as number);
            }

            return (a.birthYear as number) - (b.birthYear as number);
          });
          this.totalList = personResponse;
          this.filteredList = personResponse;
          this.localHeroList = this.filteredList.filter(p => p.localHero || p.topTen);
          this.cdr.detectChanges();
        },
      });
  }

  specialFiltersApproved(place: string, tag: string) {
    if (tag === 'Бач' && place?.includes('Бачка')) {
      return false;
    }
    if (tag === 'Бач' && place?.includes('Бачки')) {
      return false;
    }
    if (tag === 'Бач' && place?.includes('Бачко')) {
      return false;
    }
    if (tag === 'Бач' && place?.includes('Бачевци')) {
      return false;
    }
    if (tag === 'Ада' && place?.includes('Адашевци')) {
      return false;
    }
    if (tag === 'Пећ' && place?.includes('Пећинци')) {
      return false;
    }
    if (tag === 'Брус' && place?.includes('Брусник')) {
      return false;
    }
    if (tag === 'Брус' && place?.includes('Брусница')) {
      return false;
    }
    if (tag === 'Бор' && place?.includes('Борач')) {
      return false;
    }
    if (tag === 'Рача' && place?.includes('Сремска Рача')) {
      return false;
    }
    if (tag === 'Гора' && place?.includes('Фрушка Гора')) {
      return false;
    }
    if (tag === 'Бечеј' && place?.includes('Нови Бечеј')) {
      return false;
    }
    if (tag === 'Топола' && place?.includes('Бачка')) {
      return false;
    }

    return true;
  }

  getHeritage(tagName: string) {
    this.nature$ = this.natureService.getItemsSorted('type').pipe(
      map(items => {
        return items.filter(i => i.place?.includes(tagName) && this.specialFiltersApproved(i.place, tagName));
      })
    );
    this.churches$ = this.churchService.getItemsSorted('type').pipe(
      map(items => {
        return items.filter(i => i.place?.includes(tagName) && this.specialFiltersApproved(i.place, tagName));
      })
    );

    this.memorials$ = this.memorialService.getItemsSorted('type').pipe(
      map(items => {
        return items.filter(i => i.place?.includes(tagName) && this.specialFiltersApproved(i.place, tagName));
      })
    );

    this.manifestations$ = this.manifestationService.getItemsSorted('type').pipe(
      map(items => {
        return items.filter(i => i.place?.includes(tagName) && this.specialFiltersApproved(i.place, tagName));
      })
    );

    this.showSpinner = true;
    combineLatest([this.nature$, this.churches$, this.memorials$, this.manifestations$])
      .pipe(take(1))
      .subscribe({
        next: (response) => {
          this.showSpinner = false;

          const [natureList, churchList, memorialList, manifestationsList] = response;
          memorialList.sort((a, b) => a.name!.localeCompare(b.name!));
          natureList.sort((a, b) => a.name!.localeCompare(b.name!));
          churchList.sort((a, b) => a.name!.localeCompare(b.name!));
          manifestationsList.sort((a, b) => a.name!.localeCompare(b.name!));
          this.flattenSights = this.flatSights([memorialList, churchList, natureList, manifestationsList]);

          this.sights = {
            monastery: churchList.filter(i => i.type === ChurchType.monastery),
            church: churchList.filter(i => i.type === ChurchType.church),
            other: churchList.filter(i => i.type === ChurchType.other),

            museums: memorialList.filter(i => i.type === MemorialType.museum),
            palaces: memorialList.filter(i => i.type === MemorialType.palace),
            buildings: memorialList.filter(i => i.type === MemorialType.building),
            monument: memorialList.filter(i => i.type === MemorialType.monument),
            streets: memorialList.filter(i => i.type === MemorialType.streets),
            location: memorialList.filter(i => i.type === MemorialType.location),
            sports: memorialList.filter(i => i.type === MemorialType.sports),

            mountain: natureList.filter(i => i.type === NatureType.mountain),
            spa: natureList.filter(i => i.type === NatureType.spa),
            cave: natureList.filter(i => i.type === NatureType.cave),
            park: natureList.filter(i => i.type === NatureType.park),
            water: natureList.filter(i => i.type === NatureType.water),
            river: natureList.filter(i => i.type === NatureType.river),
            etno: natureList.filter(i => i.type === NatureType.etno),
            zoo: natureList.filter(i => i.type === NatureType.zoo),

            culture: manifestationsList.filter(i => i.type === ManifestationType.culture),
            sportManifestation: manifestationsList.filter(i => i.type === ManifestationType.sport),
            tavern: manifestationsList.filter(i => i.type === ManifestationType.tavern),
            fair: manifestationsList.filter(i => i.type === ManifestationType.fair),
          };
          this.cdr.detectChanges();
        }
      });
  }

  onHeritageCard(item: any): void {
    const sheet = BasicSheetComponent<any>;
    const data = { item, items: this.flattenSights };

    const panelClass = 'basic-sheet-overlay';
    const scrollStrategy = new NoopScrollStrategy();
    const disableClose = false;

    const href = this.router.url;
    this.location.go(href + '#');

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, panelClass, scrollStrategy, disableClose
    });
    const scrollPosition = this.scroller.getScrollPosition();
    this.scroller.scrollToPosition(scrollPosition);

    sheetRef.afterDismissed()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((onX: boolean) => {
        UtilsService.scrollTo(this.scroller, scrollPosition);

        if (onX) {
          this.location.back();
        } else {
          if (window.location.href.endsWith('#')) {
            this.location.back();
          };
        }
      });
  }

  goToChurches() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.churches}`);
  }

  goToMemorials() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.memorials}`);
  }

  goToManifestations() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.manifestations}`);
  }

  goToNature() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.nature}`);
  }

  onAddPersonButton() {
    this.router.navigateByUrl(`admin/panel/person-form`);
  }


  onAddNatureButton() {
    this.router.navigateByUrl(`admin/panel/nature-form`);
  }


  onAddMonumentButton() {
    this.router.navigateByUrl(`admin/panel/memorial-form`);

  }

  onAddChurchButton() {
    this.router.navigateByUrl(`admin/panel/church-form`);
  }


  onAddManifestationButton() {
    this.router.navigateByUrl(`admin/panel/manifestation-form`);
  }

  onEditData(tag: Tag) {
    this.router.navigateByUrl(`admin/panel/tag-form/${tag.id}`);
  }

  onTabChange(tabIndex: number | null) {
    if (tabIndex === this.selectedIndex) {
      return;
    }
    const route = this.router.url;
    const splitRoute = route.split('/');
    switch (tabIndex) {
      case 0: splitRoute[3] = 'podaci'; break;
      case 1: splitRoute[3] = 'licnosti'; break;
      case 2: splitRoute[3] = 'znamenitosti'; break;
    }

    const newRoute = splitRoute.join('/');
    this.selectedIndex = tabIndex ?? 0;
    this.location.replaceState(newRoute);
  }

  onPrintscreenTags(grid = false) {
    const items = this.localHeroActive ? this.localHeroList : this.filteredList;
    const tag = this.tag;

    this.matDialog.open(PrintscreenTagPeopleDialogComponent, {
      data: { items, tag, grid },
      panelClass: 'social-dialog-container',
      scrollStrategy: new NoopScrollStrategy(),
    });
  }

  onPrintscreenFlags() {
    const href = this.router.url;
    this.location.go(href + '#');

    const dialogRef = this.matDialog.open(PrintscreenCustomDialogComponent, {
      data: { tag: this.tag },
      panelClass: 'social-dialog-container',
      scrollStrategy: new NoopScrollStrategy(),
    });

    dialogRef.afterClosed().subscribe((onX: boolean) => {
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  }

  flatSights(response: any[][]) {
    //1. memorials
    const culturalBuildings = response[0].filter(e => e.type === MemorialType.museum);
    const publicBuildings = response[0].filter(e => e.type === MemorialType.building);
    const palaces = response[0].filter(e => e.type === MemorialType.palace);
    const monuments = response[0].filter(e => e.type === MemorialType.monument);
    const fortresses = response[0].filter(e => e.type === MemorialType.location);
    const sportObjects = response[0].filter(e => e.type === MemorialType.sports);
    const squaresBridges = response[0].filter(e => e.type === MemorialType.streets);

    // churches
    const monasteries = response[1].filter(e => e.type === ChurchType.monastery);
    const churches = response[1].filter(e => e.type === ChurchType.church);
    const otherReligions = response[1].filter(e => e.type === ChurchType.other);

    // nature
    const mountains = response[2].filter(e => e.type === NatureType.mountain);
    const spas = response[2].filter(e => e.type === NatureType.spa);
    const caves = response[2].filter(e => e.type === NatureType.cave);
    const parks = response[2].filter(e => e.type === NatureType.park);
    const rivers = response[2].filter(e => e.type === NatureType.river);
    const waters = response[2].filter(e => e.type === NatureType.water);
    const etnos = response[2].filter(e => e.type === NatureType.etno);
    const zoos = response[2].filter(e => e.type === NatureType.zoo);

    // manifestations
    const cultural = response[3].filter(e => e.type === ManifestationType.culture);
    const sport = response[3].filter(e => e.type === ManifestationType.sport);
    const etno = response[3].filter(e => e.type === ManifestationType.tavern);
    const gastro = response[3].filter(e => e.type === ManifestationType.fair);

    return [
      ...culturalBuildings, ...publicBuildings, ...palaces, ...monuments,
      ...fortresses, ...sportObjects, ...squaresBridges,
      ...monasteries, ...churches, ...otherReligions,
      ...mountains, ...spas, ...caves, ...parks,
      ...rivers, ...waters, ...etnos, ...zoos,
      ...cultural, ...sport, ...etno, ...gastro,
    ];
  }

  onPrintscreenSights() {
    const tag = this.tag;

    const items = [...this.flattenSights];
    this.cdr.detectChanges();

    this.matDialog.open(PrintscreenCitySightsDialogComponent, {
      data: { items, tag },
      panelClass: 'social-dialog-container',
      scrollStrategy: new NoopScrollStrategy(),
    });
  }

  onFilterButton() {
    const data = {
      title: 'Категоријe',
      observable: this.categories$,
      selectedItems: this.selectedCategories
    };

    const disableClose = false;
    const sheet = MultiselectSheetComponent;
    const scrollStrategy = this.scroll.reposition();
    const panelClass = 'multiselect-sheet';
    const href = this.router.url;
    this.location.go(href + '#');

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, scrollStrategy, disableClose, panelClass
    });

    sheetRef.afterDismissed()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((selectedItems: string[]) => {
        this.selectedCategories = [...selectedItems];

        if (!selectedItems || !selectedItems.length) {
          this.filteredList = [...this.totalList];
          this.localHeroList = this.filteredList.filter(p => p.localHero || p.topTen);
          return;
        }

        this.filteredList = this.totalList
          .filter(e => selectedItems.includes(e.category!));
        this.localHeroList = this.filteredList.filter(p => p.localHero || p.topTen);

        this.cdr.detectChanges();
      });
  }

  getSameRegionCities(tag: Tag) {
    if (tag.region) {
      this.sameRegionCities = this.allTags.filter(t => t.region === tag.region);
      return;
    }

    this.sameRegionCities = [];
    return;
  }
}

