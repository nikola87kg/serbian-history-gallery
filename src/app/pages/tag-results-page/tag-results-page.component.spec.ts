import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { ActivatedRoute } from '@angular/router';

import { TagResultsPageComponent } from './tag-results-page.component';

describe('TagResultsPageComponent', () => {
  let component: TagResultsPageComponent;
  let fixture: ComponentFixture<TagResultsPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TagResultsPageComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
        { provide: ActivatedRoute, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(TagResultsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
