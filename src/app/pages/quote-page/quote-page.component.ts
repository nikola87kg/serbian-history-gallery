import { first } from 'rxjs';

import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Location, ViewportScroller } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import {
  MultiselectSheetComponent
} from '@app/dialogs/multiselect-sheet/multiselect-sheet.component';
import {
  PrintscreenQuoteDialogComponent
} from '@app/dialogs/printscreen-quote-dialog/printscreen-quote-dialog.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { TrimTextPipe } from '@app/pipes/trim-text.pipe';
import { PersonsService } from '@app/services/persons.service';
import { AuthService } from '@services/auth.service';
import { Quote, QuotesService, QuoteType, quoteTypes } from '@services/quotes.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-quote-page',
  imports: [
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    TrimTextPipe,
    TranslitPipe
  ],
  templateUrl: './quote-page.component.html',
  styleUrls: ['./quote-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuotePageComponent {
  private quotesService = inject(QuotesService);
  private router = inject(Router);
  private scroller = inject(ViewportScroller);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private isAdmin = false;
  private matBottomSheet = inject(MatBottomSheet);
  private location = inject(Location);
  private matDialog = inject(MatDialog);

  public authService = inject(AuthService);
  public personService = inject(PersonsService);
  public totalList: Quote[] = [];
  public filteredList: Quote[] = [];
  public quoteGroups: any[] = [];
  public selectedCategories: string[] = [];

  ngOnInit() {
    this.scroller.setOffset([0, 100]);
    this.handleQuotes();
    UtilsService.scrollTop();
    this.authService.loginAdmin$.subscribe({
      next: (isAdmin) => this.isAdmin = isAdmin,
    });
  }

  goToPerson(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onEdit(id: string | null | undefined, event: any) {
    event.stopPropagation();
    if (!id || !this.isAdmin) {
      return;
    }

    this.router.navigateByUrl(`admin/panel/quote-form/${id}`);
  }

  onAuthor(slug: string | null): void {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  handleQuotes(): void {
    this.quotesService.getItems()
      .subscribe({
        next: (quoteResponse: any) => {
          this.totalList = [...quoteResponse];

          this.selectedCategories = quoteTypes
            .map(t => t.id)
            .filter(id => id !== QuoteType.sport);

          this.filteredList = this.totalList
            .filter(e => this.selectedCategories.includes(e.type!));;

          this.groupByAuthor();
        },
      });
  }

  groupByAuthor() {
    this.quoteGroups = [];
    this.cdr.detectChanges();
    const quoteGroups = UtilsService.groupBy(this.filteredList, 'authorSlug');
    quoteGroups.sort((a: any, b: any) =>
      a[0].authorName.localeCompare(b[0].authorName));

    quoteGroups.forEach((quotes: []) => {
      quotes.sort((a: Quote, b: Quote) => {
        if (a.important && !b.important) {
          return -1;
        } else if (b.important && !a.important) {
          return 1;
        }
        return a.text!.localeCompare(b.text!)
      });
    });

    setTimeout(() => {
      this.quoteGroups = [...quoteGroups];
      this.cdr.detectChanges();
    }, 1);
  }

  scrollTo(elementId: string) {
    this.scroller.scrollToAnchor(elementId);
  }

  onQuoteText(quote: Quote) {
    if (this.isAdmin) {
      this.personService.getItemBySlug(quote.authorSlug!)
        .pipe(first())
        .subscribe({
          next: (author) => {
            const dialog = PrintscreenQuoteDialogComponent;
            const panelClass = 'social-dialog-container';
            const item = author;
            const scrollStrategy = new NoopScrollStrategy();

            const href = this.router.url;
            this.location.go(href + '#');

            const options = { data: { item, quote }, panelClass, scrollStrategy };
            const dialogRef = this.matDialog.open(dialog, options);

            dialogRef.afterClosed().subscribe((onX: boolean) => {
              if (onX) {
                this.location.back();
              } else {
                if (window.location.href.endsWith('#')) {
                  this.location.back();
                };
              }
            });
          }
        });
    }
  }


  onFilterButton() {
    const data = {
      title: 'Категоријe',
      items: quoteTypes,
      selectedItems: this.selectedCategories
    };

    const disableClose = false;
    const sheet = MultiselectSheetComponent;
    const panelClass = 'multiselect-sheet';
    const href = this.router.url;
    this.location.go(href + '#');

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, disableClose, panelClass
    });

    sheetRef.afterDismissed()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((selectedItems: any = this.selectedCategories) => {
        this.selectedCategories = [...selectedItems];

        const noFilter = !selectedItems || !selectedItems.length;

        this.filteredList = noFilter
          ? [...this.totalList]
          : this.totalList.filter(e => selectedItems.includes(e.type!));

        this.groupByAuthor();
      });
  }
}

