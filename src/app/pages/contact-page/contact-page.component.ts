import { GoogleAnalyticsService } from 'ngx-google-analytics';

import { ViewportScroller } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import { FormTextareaComponent } from '@app/components/form-textarea/form-textarea.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@services/auth.service';
import { ContactService, Message } from '@services/contact.service';
import { GlossaryService } from '@services/glossary.service';
import { ThemeService } from '@services/theme.service';
import { UserProfile as AppUserProfile } from '@services/user-profile.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-contact-page',
  imports: [MatButtonModule, FormSelectComponent, FormTextComponent, FormTextareaComponent, ReactiveFormsModule, TranslitPipe],
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss'],
  providers: [MatSnackBar],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContactPageComponent {
  private scroller = inject(ViewportScroller);
  private contactService = inject(ContactService);
  private destroyRef = inject(DestroyRef);
  private googleAnalyticsService = inject(GoogleAnalyticsService);

  public authService = inject(AuthService);
  public themeService = inject(ThemeService);
  public Glossary = GlossaryService.Glossary;
  public contactTypes: any[] = [
    { id: 'error', name: this.Glossary.contactError },
    { id: 'missing_item', name: 'Недостајућa личност или знаменитост' },
    { id: 'suggestion', name: this.Glossary.contactSuggestion },
    { id: 'cooperation', name: this.Glossary.contactCooperation },
    { id: 'other', name: this.Glossary.other },
  ];

  public form = new FormGroup({
    type: new FormControl<string>('', Validators.required),
    text: new FormControl<string>('', Validators.required),
    name: new FormControl<string>('', Validators.required),
    email: new FormControl<string>('', Validators.required),
  });

  public faqList = [
    { q: 'Како су прикупљани подаци за Галерију?', a: 'Подаци су прикупљани из јавно доступних извора, књига, веб сајтова и блогова.' },
    { q: 'Да ли сте сигурни да су подаци веродостојни?', a: 'Укратко, нисмо. Подаци јесу проверавани у историјски релевантним изворима, али могуће да смо и ми грешили, али и да су ти извори недовољно верификовани и историјски прецизни.' },
    { q: 'Какав је ваш поглед на све заступљеније алтернативне погледе на српску историју?', a: 'Историјa, као научна дисциплина, бави се пре свега истраживањем прошлости народа. Сигурно да званична историја нема одговоре на сва питања и да не може да тврди шта се прецизно дешавало пре 1000 година, али овде смо се водили тиме да је боље да ослонац буде таква званична историја, него креативна алтернативна схватања.' },
    { q: 'Зашто сте простора у овој галерији дали и негативним личностима наше историје, за које је доказано да су били злочинци?', a: 'Док смо креирали ову Галерију, трудили смо се да се не приклањамо ниједној страни, јер су често одређењене значајне историјске личности у очима једних били хероји, а у очима других злочинци. Коначно, било да су једно или друго, несумљиво су оставили траг у нашој историји и стога је било битно поставити их у одређени историјски контекст.' },
  ];

  ngOnInit() {
    UtilsService.scrollTop();
    this.scroller.setOffset([0, 100]);
    this.getUserData();
    this.addGoogleAnalytics();
  }

  addGoogleAnalytics() {
    this.googleAnalyticsService.event(
      'Contact Page Init Action',
      'Page Init Category',
      'Contact Page Label',
      Date.now(),
    );
  }

  getUserData() {
    this.authService.getUserProfile()
      ?.pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (user: AppUserProfile | null) => {
          if (user) {
            this.form.controls.name.setValue(user.name);
            this.form.controls.email.setValue(user.email);
          }
        }
      });
  }

  onSend() {
    if (!this.form.valid) {
      UtilsService.openSnackbar(this.Glossary.contactSendError, true);
      return;
    }
    let payload: Message = this.form.getRawValue();
    payload.created = Date.now();
    payload.updated = Date.now();
    this.contactService.createItem(payload).then(
      _ => {
        this.form.reset();
        UtilsService.openSnackbar(this.Glossary.contactSendMessage);
      }
    );
  }

  onAnchor(anchor: any) {
    this.scroller.scrollToAnchor(anchor);
  }

}
