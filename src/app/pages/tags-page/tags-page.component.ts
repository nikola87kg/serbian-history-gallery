

import { first } from 'rxjs';

import { AsyncPipe } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@services/auth.service';
import { Tag, tagGroups, TagsService } from '@services/tags.service';

interface TagGroup {
  id: string,
  title: string,
  items: Tag[],
}

@Component({
  selector: 'app-tags-page',
  imports: [AsyncPipe, MatButtonModule, MatIconModule, TranslitPipe],
  templateUrl: './tags-page.component.html',
  styleUrls: ['./tags-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagsPageComponent {
  private tagsService = inject(TagsService);
  private cdr = inject(ChangeDetectorRef);
  private router = inject(Router);

  public authService = inject(AuthService);
  public tagGroups: TagGroup[] = [];

  ngOnInit() {
    this.handleTagGroups();
    this.handleItems();
  }

  handleTagGroups() {
    this.tagGroups = tagGroups.map((g) => {
      return {
        id: g.id,
        title: g.name,
        items: [] as Tag[],
      };
    });
  }

  handleItems() {
    this.tagsService.getItemsSorted('name')
      .pipe(first())
      .subscribe({
        next: (response) => {
          this.tagGroups.forEach((group: any) => {
            group.items = response.filter(t => t.group === group.id);
            group.items.sort((a: any, b: any) => a.name.localeCompare(b.name));
          });
          this.cdr.detectChanges();
        }
      });
  }

  onTag(slug: string | null) {
    this.router.navigateByUrl(`tag/${slug}/licnosti`);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/tag-form`);
  }
}
