import { map, take } from 'rxjs';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { GameFiveComponent } from '@app/components/game-five/game-five.component';
import { GameFourComponent } from '@app/components/game-four/game-four.component';
import { GameIntroComponent } from '@app/components/game-intro/game-intro.component';
import { GameOneComponent } from '@app/components/game-one/game-one.component';
import { GameResultsComponent } from '@app/components/game-results/game-results.component';
import { GameThreeComponent } from '@app/components/game-three/game-three.component';
import { GameTwoComponent } from '@app/components/game-two/game-two.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { Chronology, ChronologyService, ChronologyType } from '@services/chronology.service';
import { Person, PersonsService } from '@services/persons.service';
import { Quote, QuotesService } from '@services/quotes.service';
import { Holiday, HolidayType, SaintsService } from '@services/saints.service';
import { UtilsService } from '@services/utils.service';

@Component({
    selector: 'app-quiz-page',
    imports: [DragDropModule, MatButtonModule, MatProgressSpinnerModule, GameResultsComponent, TranslitPipe, GameIntroComponent, GameOneComponent, GameTwoComponent, GameThreeComponent, GameFourComponent, GameFiveComponent],
    templateUrl: './quiz-page.component.html',
    styleUrls: ['./quiz-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuizPageComponent {
  private personsService = inject(PersonsService);
  private quotesService = inject(QuotesService);
  private saintsService = inject(SaintsService);
  private chronologyService = inject(ChronologyService);
  private cdr = inject(ChangeDetectorRef);

  public persons: Person[] = [];
  public quotes: Quote[] = [];
  public saints: Holiday[] = [];
  public chronology: Chronology[] = [];
  public showSpinner = false;
  public showIntro = false;
  public storageChecked = false;

  public gamePoints = [0, 0, 0, 0, 0];

  ngOnInit() {
    UtilsService.scrollTop();
    this.checkDailyQuiz();
  }

  checkDailyQuiz() {
    const today = UtilsService.getTodayFormatted();
    const storageName = `quiz-${today}`;
    const storageQuiz = localStorage.getItem(storageName);
    if (storageQuiz) {
      this.gamePoints = JSON.parse(storageQuiz);
      this.showIntro = false;
      this.loadNextGame();
      this.cdr.detectChanges();

    } else {
      this.showIntro = true;
    }

    this.storageChecked = true;
    this.cdr.detectChanges();
  }

  getPersons() {
    this.showSpinner = true;

    this.personsService.getItemsSorted('commonName')
      .subscribe({
        next: response => {
          this.persons = response;
          this.showSpinner = false;
          this.cdr.detectChanges();
        }
      });
  }

  loadNextGame() {
    if (this.gamePoints[4]) {
      this.cdr.detectChanges();
      return;
    } else if (this.gamePoints[3]) {
      this.getChronology();
      this.cdr.detectChanges();
      return;
    } else if (this.gamePoints[2]) {
      this.getSaints();
      this.cdr.detectChanges();
      return;
    } else if (this.gamePoints[1]) {
      this.getQuotes();
      this.cdr.detectChanges();
      return;
    } else if (this.gamePoints[0]) {
      this.getPersons();
      this.cdr.detectChanges();
      return;
    }
  }

  onGameOneFinish(event: number) {
    this.gamePoints[0] = event;
    UtilsService.scrollTop();
    this.cdr.detectChanges();
  }

  onGameTwoFinish(event: number) {
    this.gamePoints[1] = event;
    UtilsService.scrollTop();
    this.getQuotes();
  }

  onGameThreeFinish(event: number) {
    this.gamePoints[2] = event;
    UtilsService.scrollTop();
    this.getSaints();
  }

  onGameFourFinish(event: number) {
    this.gamePoints[3] = event;
    UtilsService.scrollTop();
    this.getChronology();
  }

  onGameFiveFinish(event: number) {
    this.gamePoints[4] = event;
    UtilsService.scrollTop();
  }

  getQuotes() {
    this.showSpinner = true;

    this.quotesService.getItemsQuery('important', '==', true)
      .pipe(take(1))
      .subscribe({
        next: response => {
          this.quotes = response;
          this.showSpinner = false;
          this.cdr.detectChanges();
        }
      });
  }

  getSaints() {
    this.showSpinner = true;

    this.saintsService.getItemsQuery('type', '==', HolidayType.saint)
      .pipe(
        map((holidays) => {
          return this.removeMovableHolidays(holidays);
        }),
        take(2)
      )
      .subscribe({
        next: response => {
          this.saints = response;
          this.showSpinner = false;
          this.cdr.detectChanges();
        }
      });
  }

  removeMovableHolidays(holidays: Holiday[]): Holiday[] {
    const fixedHolidays = holidays.filter(h => h.saintMonth);
    return fixedHolidays;
  }

  getChronology() {
    this.showSpinner = true;
    this.chronologyService.getItemsSorted('age').pipe(
      map(arr => arr.filter(e => e.typeId === ChronologyType.state)))
      .subscribe({
        next: response => {
          this.chronology = response;
          this.showSpinner = false;
          this.cdr.detectChanges();
        }
      });
  }
}
