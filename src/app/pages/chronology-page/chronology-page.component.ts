import { firstValueFrom, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { ScrollStrategyOptions } from '@angular/cdk/overlay';
import { CommonModule, Location, ViewportScroller } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, ViewChild
} from '@angular/core';
import { MatBottomSheet, MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { BasicSheetComponent } from '@app/dialogs/basic-sheet/basic-sheet.component';
import {
  PersonTimelineSheetComponent
} from '@app/dialogs/person-timeline-sheet/person-timeline-sheet.component';
import { RemoveHtmlTagsPipe } from '@app/pipes/remove-html-tags.pipe';
import { TimelineTopPipe } from '@app/pipes/timeline-top.pipe';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { ThemeService } from '@app/services/theme.service';
import { AuthService } from '@services/auth.service';
import { CategoriesService, Category } from '@services/categories.service';
import {
  Chronology, chronologyBoxes, ChronologyService, ChronologyType, ChronologyUrl
} from '@services/chronology.service';
import { Epoch, EpochsService } from '@services/epochs.service';
import { PersonsService } from '@services/persons.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-chronology-page',
  imports: [CommonModule, MatIconModule,
    MatProgressSpinnerModule, MatBottomSheetModule,
    MatButtonModule, TranslitPipe, RemoveHtmlTagsPipe, TimelineTopPipe],
  templateUrl: './chronology-page.component.html',
  styleUrls: ['./chronology-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChronologyPageComponent {
  @ViewChild('tabGroup') tabGroup!: MatTabGroup;
  private router = inject(Router);
  private activatedRoute = inject(ActivatedRoute);
  private chronologyService = inject(ChronologyService);
  private personsService = inject(PersonsService);
  private epochService = inject(EpochsService);
  private categoryService = inject(CategoriesService);
  private themeService = inject(ThemeService);
  private scroll = inject(ScrollStrategyOptions);
  private matBottomSheet = inject(MatBottomSheet);
  private location = inject(Location);
  private scroller = inject(ViewportScroller);
  private cdr = inject(ChangeDetectorRef);

  public authService = inject(AuthService);
  public chronologies$!: Observable<Chronology[]>;

  public get imagePrefix(): string {
    return this.themeService.isDarkMode()
      ? `radial-gradient(circle, rgb(0 0 0 / 85%) 0%, black 95%), linear-gradient(#353535, #222222), `
      : `radial-gradient(circle, rgb(255 255 255 / 80%) 0%, white 95%), linear-gradient(#c8c8c8, #c8c8c8), `;
  };

  public items$!: Observable<any[]>;

  public showSpinner = true;
  public epochs: Epoch[] = [];
  public categories: Category[] = [];


  public boxPath = '';
  public currentBox: any;
  public currentStep = 0;
  public showStep2Data = false;
  public zoomDefault = 600;

  public chronologyBoxes = chronologyBoxes;

  ngOnInit() {
    UtilsService.scrollTop();
    this.scroller.setOffset([0, 100]);
    this.handleSlug();
  }

  async getData() {
    if (!this.categories.length) {
      const epochs$ = this.epochService.getItemsSorted('order');
      this.epochs = await firstValueFrom(epochs$);

      const categories$ = this.categoryService.getItemsSorted('order');
      this.categories = await firstValueFrom(categories$);
    }

    if (!this.chronologies$) {
      this.chronologies$ = this.chronologyService.getItemsSorted('age');
    }

    let type = '';
    let yearDifference = 1000;

    switch (this.boxPath) {
      case ChronologyUrl.state:
        type = ChronologyType.state;
        this.zoomDefault = 600;
        break;
      case ChronologyUrl.battles:
        type = ChronologyType.battles;
        this.zoomDefault = 1000;
        break;
      case ChronologyUrl.sport:
        type = ChronologyType.sport;
        this.zoomDefault = 600;
        break;
      case ChronologyUrl.art:
        type = ChronologyType.art;
        this.zoomDefault = 600;
        break;
      case ChronologyUrl.persons:
        this.zoomDefault = 600;
        break;
      case ChronologyUrl.movies:
        type = ChronologyType.movies;
        this.zoomDefault = 800;
        break;
    }

    if (type) {
      this.items$ = this.chronologies$
        .pipe(
          map(arr => arr.filter(e => e.typeId === type)),
          map(arr => arr.map((x: any, i) => {
            const y: any = arr[i - 1];
            const minYear = arr[0].age;
            const maxYear = arr[arr.length - 1].age;
            const divRemHeight = 8.2;
            const maxItems = Math.ceil(this.zoomDefault / divRemHeight);
            yearDifference = maxYear! - minYear!;
            x.minDistance = Math.ceil(1000 * yearDifference / maxItems) / 1000;
            if (y && x.age - y.top < x.minDistance) {
              x.top = Math.max(x.age, y.top) + x.minDistance;
            } else {
              x.top = x.age;
            }
            return x;
          })),
        );
    } else {
      this.items$ = this.personsService
        .getItemsQuery('keyNotes', '!=', '')
        .pipe(
          tap(arr => arr.sort((a, b) => a.birthYear! - b.birthYear!)),
          map(arr => arr.filter(e => e.keyNotes)),
          map(arr => arr.map((x: any, i) => {
            const y: any = arr[i - 1];
            const minYear = arr[0].birthYear;
            const maxYear = arr[arr.length - 1].birthYear;
            const divRemHeight = 8.2;
            const maxItems = Math.ceil(this.zoomDefault / divRemHeight);
            yearDifference = maxYear! - minYear!;
            x.minDistance = Math.ceil(1000 * yearDifference / maxItems) / 1000;
            if (y && x.birthYear - y.top < x.minDistance) {
              x.top = Math.max(x.birthYear, y.top) + x.minDistance;
            } else {
              x.top = x.birthYear;
            }
            return x;
          })),
        );
    }

    this.showSpinner = false;
    this.showStep2Data = true;
    this.cdr.detectChanges();
  }

  getEpochsByCategory(order: number): Epoch[] {
    const categories = this.categories.find(c => c.order === order);
    const epochs = this.epochs.filter(e => categories?.epochs?.includes(e.id!));

    epochs.sort((a, b) => (a.order as number) - (b.order as number));

    return [...epochs];
  }

  handleSlug() {
    this.showStep2Data = false;
    const slug = this.activatedRoute.snapshot?.paramMap?.get('slug');
    this.boxPath = slug || '';

    if (slug) {
      this.currentBox = this.chronologyBoxes.find(b => b.path === this.boxPath);
    }
    this.currentStep = slug ? 2 : 1;

    if (this.currentStep === 2) {
      this.getData();
    }
  }

  onEdit(e: any, id: string | null | undefined): void {
    e.stopPropagation();
    const url = `admin/panel/chronology-form/${id}`;
    this.router.navigateByUrl(url);
  };

  onEditAux(id: string | null | undefined) {
    const url = `admin/panel/chronology-form/${id}`;
    UtilsService.openOnMiddleClick(url);
  }

  onEditPerson(e: any, id: string | null | undefined): void {
    e.stopPropagation();
    const url = `admin/panel/person-form/${id}`;
    this.router.navigateByUrl(url);
  };

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/chronology-form`);
  }

  onPerson(slug: string | null | undefined): void {
    const data = { slug };

    const sheet = PersonTimelineSheetComponent;
    const disableClose = false;
    const scrollStrategy = this.scroll.reposition();
    const scrollPosition = this.scroller.getScrollPosition();

    const href = this.router.url;
    this.location.go(href + '#');

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, scrollStrategy, disableClose
    });

    sheetRef.afterDismissed().subscribe((onX: boolean) => {
      UtilsService.scrollTo(this.scroller, scrollPosition);
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  };

  onItem(item: any, items: any[]): void {
    const data = { item, items };
    const sheet = BasicSheetComponent<any>;

    const panelClass = 'basic-sheet-overlay';
    const scrollStrategy = this.scroll.reposition();
    const scrollPosition = this.scroller.getScrollPosition();
    const disableClose = false;

    const href = this.router.url;
    this.location.go(href + '#');

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, panelClass, scrollStrategy, disableClose
    });

    sheetRef.afterDismissed().subscribe((onX: boolean) => {
      UtilsService.scrollTo(this.scroller, scrollPosition);
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });

  };

  goToStep1() {
    this.router.navigateByUrl('vremeplov');
  }

  setStep2(boxPath: string) {
    this.router.navigateByUrl(`vremeplov/${boxPath}`);
  }

}
