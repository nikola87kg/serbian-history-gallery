import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { ActivatedRoute } from '@angular/router';

import { ChronologyPageComponent } from './chronology-page.component';

describe('ChronologyPageComponent', () => {
  let component: ChronologyPageComponent;
  let fixture: ComponentFixture<ChronologyPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ChronologyPageComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
        { provide: ActivatedRoute, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(ChronologyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
