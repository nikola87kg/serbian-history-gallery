import { GoogleAnalyticsService } from 'ngx-google-analytics';
import { first, map } from 'rxjs';

import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { CommonModule, Location } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {
  DailyEventCardComponent
} from '@app/components/daily-event-card/daily-event-card.component';
import { PersonCardComponent } from '@app/components/person-card/person-card.component';
import { SaintCardComponent } from '@app/components/saint-card/saint-card.component';
import {
  PrintscreenBirthdayDialogComponent
} from '@app/dialogs/printscreen-birthday-dialog/printscreen-birthday-dialog.component';
import {
  PrintscreenEventsDialogComponent
} from '@app/dialogs/printscreen-events-dialog/printscreen-events-dialog.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { HistoricalEvent, HistoricalEventsService } from '@app/services/historical-events.service';
import { Holiday, SaintsService } from '@app/services/saints.service';
import { NumbersOnlyDirective } from '@directives/numbers-only.directive';
import { AuthService } from '@services/auth.service';
import { Person, PersonsService } from '@services/persons.service';
import { months, UtilsService } from '@services/utils.service';

enum Segment {
  rodjeni = 'rodjeni',
  umrli = 'umrli',
  dogadjaji = 'dogadjaji',
}
@Component({
  selector: 'app-calendar-page',
  imports: [CommonModule, MatFormFieldModule, MatSelectModule,
    SaintCardComponent, TranslitPipe, NumbersOnlyDirective,
    MatInputModule, NumbersOnlyDirective,
    DailyEventCardComponent, MatInputModule,
    FormsModule, PersonCardComponent, MatIconModule, MatButtonModule],
  templateUrl: './calendar-page.component.html',
  styleUrls: ['./calendar-page.component.scss'],
  providers: [MatSnackBar],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CalendarPageComponent {
  private snackbar = inject(MatSnackBar);
  private service = inject(PersonsService);
  private router = inject(Router);
  private saintsService = inject(SaintsService);
  private destroyRef = inject(DestroyRef);
  private googleAnalyticsService = inject(GoogleAnalyticsService);
  private activatedRoute = inject(ActivatedRoute);
  private matDialog = inject(MatDialog);
  private location = inject(Location);
  private historicalEventsService = inject(HistoricalEventsService);
  private cdr = inject(ChangeDetectorRef);

  public showCalendarYears = false;
  public authService = inject(AuthService);
  public dateControl: string | undefined;
  public monthControl: string | undefined;
  public yearControl: string | undefined;
  public selectedYearControl: string | undefined;
  public persons: Person[] = [];
  public dayControl: string | undefined;
  public dayOptions = this.getDaysinArray();
  public monthOptions = UtilsService.getMonthOptions();
  public dateOverlayDisplayed = false;
  public months = months;
  public dailyEvents: HistoricalEvent[] = [];
  public dailySaints: Holiday[] = [];
  public allDailyCards: any[] = [];
  public activeStep = 1;
  public Segment = Segment;
  public segmentUrl = Segment.rodjeni;
  public currentYear = new Date().getFullYear();

  public isYearOn = false;

  get minYearError(): boolean {
    return Number(this.selectedYearControl) < 600;
  }

  get maxYearError(): boolean {

    return Number(this.selectedYearControl) > this.currentYear;
  }

  get searchDisabled(): boolean {
    const result = this.showCalendarYears &&
      (!this.selectedYearControl || this.minYearError || this.maxYearError);
    return result;
  }

  ngOnInit() {
    this.initSegment();
    this.initDate();
    this.listenRouteChanges();
  }

  getDaysinArray(max = 31) {
    return Array.from(Array(max).keys(), num => String(num + 1));
  }

  listenRouteChanges() {
    this.router.events
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.initSegment();
          this.initDate();
        }
      });
  }

  initSegment() {
    this.segmentUrl = this.activatedRoute.snapshot?.paramMap?.get('segment') as Segment;

    switch (this.segmentUrl) {
      case Segment.rodjeni: this.activeStep = 1; break;
      case Segment.umrli: this.activeStep = 2; break;
      case Segment.dogadjaji: this.activeStep = 3; break;
    }
  }

  addGoogleAnalytics(date: string) {
    this.googleAnalyticsService.event(
      `Birthday Page Action - ${date}`,
      'Birthday Page Init Category',
      `Birthday Page - ${date}`,
      Date.now(),
    );
  }

  initDate() {
    UtilsService.scrollTop();
    this.allDailyCards = [];
    const dateUrl: any = this.activatedRoute.snapshot?.paramMap?.get('date');
    let day = UtilsService.getTodayDay();
    const monthString = UtilsService.getTodayMonth();
    let month = `${UtilsService.getMonthTitleFromOrder(monthString)}`;

    this.dateControl = `${day}. ${month}`;

    if (dateUrl) {
      const splitter = dateUrl.split('-');
      this.isYearOn = splitter.length === 1;

      if (this.isYearOn) {
        this.showCalendarYears = true;
        this.yearControl = splitter[0];
      } else {
        day = splitter[0];
        const monthString = splitter[1];
        month = `${UtilsService.getMonthTitleFromId(monthString)}`;
        this.dateControl = `${day}. ${month}`;
      }
    }

    month = UtilsService.getMonthOrderFromTitle(month);

    if (this.showCalendarYears) {
      if (this.yearControl) {
        this.selectedYearControl = this.yearControl;
        this.activeStep === 2 ? this.getDeathsByYear() : this.getYearPeople();
      }
    } else {
      this.activeStep === 2 ? this.getDeathsByDate() : this.getBirthdays();
      this.getDailyEvent(+day, +month);
      this.getDailySaint(day, month);
    }
  }

  getDailyEvent(day: number, month: number) {
    this.dailyEvents = [];
    const args = [];

    args.push('day');
    args.push(+day);

    args.push('month');
    args.push(+month);

    this.historicalEventsService
      .getItemsQueries(...args)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response) => {
          this.dailyEvents = [...response];
          const dailyCards: any[] = [...this.dailyEvents];
          dailyCards.forEach(c => c.kind = 'event');
          this.allDailyCards = UtilsService.removeDuplicatesByKey([
            ...this.allDailyCards.filter(c => c.kind !== 'event'),
            ...dailyCards], 'slug');
          this.cdr.detectChanges();
        }
      });
  }

  getDailySaint(day: string, month: string) {
    this.dailySaints = [];
    this.saintsService.getItemsSorted('saintDate')
      .pipe(
        map((holidays) => {
          const fixHolidays = UtilsService.setMovableHolidays(holidays);
          const filteredItems = fixHolidays.filter(h => {
            return String(h.saintMonth) === String(month)
              && String(h.saintDay) === String(day);
          });
          return filteredItems;
        }),

        takeUntilDestroyed(this.destroyRef),
      )
      .subscribe({
        next: (response) => {
          this.dailySaints = [...response];
          const dailyCards: any[] = [...this.dailySaints];
          dailyCards.forEach(c => c.kind = 'holiday');
          this.allDailyCards = UtilsService.removeDuplicatesByKey([
            ...this.allDailyCards.filter(c => c.kind !== 'holiday'),
            ...dailyCards], 'slug');
          this.cdr.detectChanges();
        }
      });
  }

  changePage() {
    const splitter = this.dateControl?.split('. ');
    if (splitter) {
      const day = splitter[0];
      const month = UtilsService.getMonthIdFromTitle(splitter[1]);
      const path = `${day}-${month}`;
      this.addGoogleAnalytics(path);
      this.router.navigateByUrl(`kalendar/${path}/${this.segmentUrl}`);
      this.onOverlayCancel();
    }
  }

  setYear(year: string) {
    if (year) {
      this.isYearOn = true;
      this.addGoogleAnalytics(year);
      if (this.segmentUrl === Segment.dogadjaji) {
        this.segmentUrl = Segment.rodjeni;
      }
      this.router.navigateByUrl(`kalendar/${year}/${this.segmentUrl}`);
      this.onOverlayCancel();
    }
  }

  getYearPeople(): void {
    this.onOverlayCancel();

    const args = [];
    args.push('birthYear');
    args.push(+this.yearControl!);

    this.service.getItemsQueries(...args)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Person[]) => {
          response.sort((a: Person, b: Person) => {
            if (a.frontPage && !b.frontPage) {
              return -1;
            } else if (b.frontPage && !a.frontPage) {
              return 1;
            } else if (a.topTen && !b.topTen) {
              return -1;
            } else if (b.topTen && !a.topTen) {
              return 1;
            } else if (a.localHero && !b.localHero) {
              return -1;
            } else if (b.localHero && !a.localHero) {
              return 1;
            }
            return (a.category!.localeCompare(b.category!));
          });

          this.persons = response || [];
          this.dailyEvents = [];
          this.dailySaints = [];
          this.cdr.detectChanges();
        }
      });
  }

  getDeathsByDate() {
    this.service.getItems()
      .pipe(first())
      .subscribe({
        next: (response) => {
          this.dayControl;
          const splitter: any = this.dateControl ? this.dateControl.split(' ') : [];
          const day = splitter[0];
          const month = UtilsService.getMonthOrderFromTitle(splitter[1]);
          const phrase = `- ${day}${month}.`;

          const deaths = response.filter(person => !!person.birthDeath?.includes(phrase));

          deaths.sort((a: Person, b: Person) => {
            if (a.birthYear === b.birthYear && a.birthDay && b.birthDay
              && a.birthMonth === b.birthMonth) {
              return (a.birthDay as number) - (b.birthDay as number);
            }
            if (a.birthYear === b.birthYear && a.birthMonth && b.birthMonth) {
              return (a.birthMonth as number) - (b.birthMonth as number);
            }

            return (a.birthYear as number) - (b.birthYear as number);
          });

          this.persons = deaths || [];
          this.cdr.detectChanges();
        }
      });
  }

  getDeathsByYear() {
    this.service.getItems()
      .pipe(first())
      .subscribe({
        next: (response) => {
          this.dayControl;
          const phrase = `${this.yearControl}`;
          const separator = ' - ';

          const deaths = response.filter(person =>
            !!person.birthDeath?.includes(phrase)
            && !!person.birthDeath?.includes(separator)
            && !person.birthDeath?.includes(phrase + separator)
          );

          deaths.sort((a: Person, b: Person) => {
            if (a.birthYear === b.birthYear && a.birthDay && b.birthDay
              && a.birthMonth === b.birthMonth) {
              return (a.birthDay as number) - (b.birthDay as number);
            }
            if (a.birthYear === b.birthYear && a.birthMonth && b.birthMonth) {
              return (a.birthMonth as number) - (b.birthMonth as number);
            }

            return (a.birthYear as number) - (b.birthYear as number);
          });

          this.persons = deaths || [];
          this.cdr.detectChanges();
        }
      });
  }

  getBirthdays(): void {
    this.onOverlayCancel();

    const args = [];
    const split = this.dateControl?.split('. ') || [];
    const day = split[0];
    const month = UtilsService.getMonthOrderFromTitle(split[1]);
    if (day) {
      args.push('birthDay');
      args.push(+day);
    }
    if (month) {
      args.push('birthMonth');
      args.push(+month);
    }

    this.service.getItemsQueries(...args)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Person[]) => {
          response.sort((a: Person, b: Person) => {
            if (a.birthYear === b.birthYear && a.birthDay && b.birthDay
              && a.birthMonth === b.birthMonth) {
              return (a.birthDay as number) - (b.birthDay as number);
            }
            if (a.birthYear === b.birthYear && a.birthMonth && b.birthMonth) {
              return (a.birthMonth as number) - (b.birthMonth as number);
            }

            return (a.birthYear as number) - (b.birthYear as number);
          });

          this.persons = response || [];
          this.cdr.detectChanges();
        }
      });
  }

  onMonthChange(month: string) {
    this.monthControl = month;
    this.dayControl = '1';
    let maxDays = '31';
    switch (month) {
      case '2': maxDays = '29'; break;
      case '4':
      case '6':
      case '9':
      case '11': maxDays = '30'; break;
      default: maxDays = '31'; break;
    }
    this.dayOptions = this.getDaysinArray(Number(maxDays));
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/person-form`);
  }

  onPrintscreenMonth() {
    this.router.navigateByUrl(`admin/panel/event-form`);
  }

  onOverlayCancel() {
    this.dateOverlayDisplayed = false;
    if (!!this.authService) {
      this.cdr.detectChanges();
    }

  }

  showDateOverlay() {
    this.dateOverlayDisplayed = true;

    if (this.dateControl) {
      const splitter = this.dateControl.split('. ');
      this.dayControl = splitter[0];
      this.monthControl = UtilsService.getMonthOrderFromTitle(splitter[1]);
      this.onMonthChange(this.monthControl);
    }
  }

  onSearch() {
    this.yearControl = this.selectedYearControl;
    if (this.showCalendarYears && this.yearControl) {
      this.setYear(this.yearControl);
      return;
    }

    if (this.dayControl && this.monthControl) {
      const day = `${this.dayControl}.`;
      const monthString = String(this.monthControl);
      const month = `${UtilsService.getMonthTitleFromOrder(monthString)}`;
      this.dateControl = `${day} ${month}`;
      this.isYearOn = false;
      this.changePage();
    }
  }

  onClose() {
    this.onOverlayCancel();
  }

  openSnackbar(message: string, metadata: any) {
    const panelClass =
      metadata.error ? ['custom-snackbar', 'error']
        : metadata.info ? ['custom-snackbar', 'info']
          : ['custom-snackbar', 'success'];

    this.snackbar.open(message, undefined, {
      panelClass,
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
    });
  }

  onBackArrow($event: any) {
    $event.stopPropagation();

    if (this.yearControl && this.showCalendarYears) {
      const year = String(+this.yearControl - 1);
      this.setYear(year);
      return;
    }

    if (!this.dateControl) {
      return;
    }

    const parts = this.dateControl.split('. ');
    let day = +parts[0];
    let serbianMonth = parts[1];
    let month = +UtilsService.getMonthOrderFromTitle(serbianMonth);

    const months30 = [4, 6, 9, 11];
    const months31 = [1, 3, 5, 7, 8, 10, 12];

    if (day > 1) {
      day--;

    } else if (day === 1) {
      month === 1 ? month = 12 : month--;
      serbianMonth = UtilsService.getMonthTitleFromOrder(String(month));
      day = months31.includes(month) ? 31 : months30.includes(month) ? 30 : 29;
    }

    this.dateControl = `${day}. ${serbianMonth}`;
    this.changePage();
  }

  onForwardArrow($event: any) {
    $event.stopPropagation();

    if (this.yearControl && this.showCalendarYears) {
      const year = String(+this.yearControl + 1);
      this.setYear(year);
      return;
    }


    if (!this.dateControl) {
      return;
    }

    const parts = this.dateControl.split('. ');
    let day = +parts[0];
    let serbianMonth = parts[1];
    let month = +UtilsService.getMonthOrderFromTitle(serbianMonth);

    const months30 = [4, 6, 9, 11];
    const months31 = [1, 3, 5, 7, 8, 10, 12];

    if (day === 29 && month === 2) {
      day = 1; month = 3;
    } else if (day === 30 && months30.includes(month)) {
      day = 1; month++;
    } else if (day === 31 && months31.includes(month)) {
      day = 1; month = month === 12 ? 1 : month + 1;
    } else {
      day++;
    }

    serbianMonth = UtilsService.getMonthTitleFromOrder(String(month));
    this.dateControl = `${day}. ${serbianMonth}`;
    this.changePage();
  }

  async onPrintscreenBirthdays() {
    const href = this.router.url;
    this.location.go(href + '#');

    const items = this.persons;
    const dialogRef = this.matDialog.open(PrintscreenBirthdayDialogComponent, {
      data: { items },
      panelClass: 'social-dialog-container',
      scrollStrategy: new NoopScrollStrategy(),
    });

    dialogRef.afterClosed().subscribe((onX: boolean) => {
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  }

  async onPrintscreenSaint() {
    const href = this.router.url;
    this.location.go(href + '#');

    const items = this.dailySaints;
    const dialogRef = this.matDialog.open(PrintscreenEventsDialogComponent, {
      data: { items, saint: true },
      panelClass: 'social-dialog-container',
      scrollStrategy: new NoopScrollStrategy(),
    });

    dialogRef.afterClosed().subscribe((onX: boolean) => {
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  }

  setSegment(segment: Segment) {
    this.segmentUrl = segment;
    const url = this.router.url.split('/');
    this.router.navigateByUrl(`${url[1]}/${url[2]}/${segment}`);
    switch (segment) {
      case Segment.rodjeni: this.onBornDeath(false); break;
      case Segment.umrli: this.onBornDeath(true); break;
      case Segment.dogadjaji: this.onEvent(); break;
    }
  }

  onBornDeath(showDeaths: boolean) {
    this.activeStep = showDeaths ? 2 : 1;
    if (this.showCalendarYears) {
      this.activeStep === 2 ? this.getDeathsByYear() : this.getYearPeople();
    } else {
      this.activeStep === 2 ? this.getDeathsByDate() : this.getBirthdays();
    }
  }

  onEvent() {
    this.activeStep = 3;
  }

  async onPrintscreenEvent() {
    const href = this.router.url;
    this.location.go(href + '#');

    const items = this.dailyEvents;
    const dialogRef = this.matDialog.open(PrintscreenEventsDialogComponent, {
      data: { items, saint: false },
      panelClass: 'social-dialog-container',
      scrollStrategy: new NoopScrollStrategy(),
    });

    dialogRef.afterClosed().subscribe((onX: boolean) => {
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  }

}
