import { lastValueFrom, Observable, tap } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { DefaultLogoDirective } from '@directives/default-logo.directive';
import { CategoriesService, Category } from '@services/categories.service';
import { PersonsService } from '@services/persons.service';
import { ThemeService } from '@services/theme.service';
import { UtilsService } from '@services/utils.service';

@Component({
    selector: 'app-category-picker-page',
    imports: [CommonModule, DefaultLogoDirective,
        TranslitPipe,],
    templateUrl: './category-picker-page.component.html',
    styleUrls: ['./category-picker-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryPickerPageComponent {
  private categoriesService = inject(CategoriesService);
  private router = inject(Router);
  private personsService = inject(PersonsService);
  private cdr = inject(ChangeDetectorRef);

  public categoryCounter: any[] = [];
  public themeService = inject(ThemeService);
  public categories$!: Observable<Category[]>;

  ngOnInit() {
    this.getData();
    UtilsService.scrollTop();
  }

  getData() {
    this.categories$ = this.categoriesService.getItemsSorted('order')
      .pipe(

        tap((categories: Category[]) => {
          categories.forEach(c => {
            lastValueFrom(this.personsService.countItemsFiltered('category', '==', c.id))
              .then(response => {
                const counter = response.data().count;
                this.categoryCounter.push({ id: c.id, counter });
                this.cdr.detectChanges();
              });
          });
        }));
  }

  onCategory(category: Category) {
    this.router.navigateByUrl(`kategorija/${category.slug}`);
  }

  getCounterById(id: string | null | undefined): number {
    if (!id) {
      return 0;
    }
    const item = this.categoryCounter.find(i => i.id === id);
    return item?.counter || 0;
  }
}

