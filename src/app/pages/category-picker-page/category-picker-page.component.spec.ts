import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { CategoryPickerPageComponent } from './category-picker-page.component';

describe('CategoryPickerPageComponent', () => {
  let component: CategoryPickerPageComponent;
  let fixture: ComponentFixture<CategoryPickerPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CategoryPickerPageComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(CategoryPickerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
