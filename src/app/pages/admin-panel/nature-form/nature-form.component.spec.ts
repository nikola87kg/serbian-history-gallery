import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { NatureFormComponent } from './nature-form.component';

describe('NatureFormComponent', () => {
  let component: NatureFormComponent;
  let fixture: ComponentFixture<NatureFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NatureFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(NatureFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
