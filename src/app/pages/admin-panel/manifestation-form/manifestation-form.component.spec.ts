import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManifestationFormComponent } from './manifestation-form.component';

describe('ManifestationFormComponent', () => {
  let component: ManifestationFormComponent;
  let fixture: ComponentFixture<ManifestationFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ManifestationFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ManifestationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
