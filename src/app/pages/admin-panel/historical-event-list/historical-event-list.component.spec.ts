import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalEventListComponent } from './historical-event-list.component';

describe('HistoricalEventListComponent', () => {
  let component: HistoricalEventListComponent;
  let fixture: ComponentFixture<HistoricalEventListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HistoricalEventListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HistoricalEventListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
