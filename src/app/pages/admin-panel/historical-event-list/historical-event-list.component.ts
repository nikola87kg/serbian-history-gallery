import { Observable, tap } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { HistoricalEventsService } from '@app/services/historical-events.service';

@Component({
    selector: 'app-historical-event-list',
    imports: [CommonModule, TableComponent, MatButtonModule],
    templateUrl: './historical-event-list.component.html',
    styleUrl: './historical-event-list.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HistoricalEventListComponent {
  private router = inject(Router);
  private service = inject(HistoricalEventsService);

  public events$!: Observable<any>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.events$ = this.service.getItemsSorted('month', 'asc')
      .pipe(
        tap(response => response.sort((a: any, b: any) => {
          if (a.month === b.month) {
            return a.day - b.day;
          }
          return a.month - b.month;
        }))
      );

    this.columns = [
      { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
      { def: 'title', title: 'Назив', type: ColumnType.Text },
      { def: 'month', title: 'Месец', type: ColumnType.Number },
      { def: 'day', title: 'Дан', type: ColumnType.Number },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/event-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/event-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onShortcut() {
    this.router.navigateByUrl(`srpsko-nasledje/zdanja-i-spomenici`);
  }
}
