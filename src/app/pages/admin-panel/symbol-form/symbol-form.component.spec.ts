import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SymbolFormComponent } from './symbol-form.component';

describe('SymbolFormComponent', () => {
  let component: SymbolFormComponent;
  let fixture: ComponentFixture<SymbolFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SymbolFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(SymbolFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
