import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormImageComponent } from '@app/components/form-image/form-image.component';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import { FormTextareaComponent } from '@app/components/form-textarea/form-textarea.component';
import { ImageDialogComponent } from '@app/dialogs/image-dialog/image-dialog.component';
import { GlossaryService } from '@services/glossary.service';
import { Symbol, SymbolsService, SymbolType, symbolTypes } from '@services/symbols.service';

@Component({
  selector: 'app-symbol-form',
  imports: [
    FormTextComponent,
    MatButtonModule,
    FormImageComponent,
    ReactiveFormsModule,
    MatDialogModule,
    FormTextareaComponent,
    FormSelectComponent
  ],
  templateUrl: './symbol-form.component.html',
  styleUrls: ['./symbol-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SymbolFormComponent {
  private service = inject(SymbolsService);
  private router = inject(Router);
  private matDialog = inject(MatDialog);
  private destroyRef = inject(DestroyRef);
  private location = inject(Location);

  public id!: string;
  public buttonLabel!: string;
  public Glossary = GlossaryService.Glossary;
  public symbolTypes = symbolTypes;

  public form = new FormGroup({
    name: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    imagePath: new FormControl<string>(''),
    author: new FormControl<string>(''),
    authorSlug: new FormControl<string>(''),
    author2: new FormControl<string>(''),
    authorSlug2: new FormControl<string>(''),
    date: new FormControl<string>(''),
    dateOrder: new FormControl<string>(''),
    type: new FormControl<SymbolType | null>(null),
    description: new FormControl<string>(''),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Symbol) => {
          this.form.patchValue(response);
        },
      });
  }

  updateItem(): void {
    const payload: Symbol = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload)
      .then((_) => this.location.back());
  }

  createItem(): void {
    const payload: Symbol = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then((response) => {
      this.openImageDialog(payload, response);
    });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  onImage(): void {
    const payload: Symbol = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.openImageDialog(payload, { id: this.id }, true);
  }

  openImageDialog(item: Symbol, response: any, edited = false): void {
    const { id } = response;

    if (!id) {
      return;
    }

    const service = this.service;
    const defaultFormat = 'box';
    const dialogRef = this.matDialog.open(ImageDialogComponent, {
      width: '60rem',
      data: { item, service, defaultFormat, id },
      scrollStrategy: new NoopScrollStrategy(),
    });
    dialogRef.afterClosed().subscribe({
      next: (confirm: boolean) => {
        if (confirm && !edited) {
          this.location.back();
        }
      },
    });
  }
}
