import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { SymbolsService } from '@services/symbols.service';

@Component({
    selector: 'app-symbol-list',
    imports: [CommonModule, TableComponent, MatButtonModule],
    templateUrl: './symbol-list.component.html',
    styleUrls: ['./symbol-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SymbolListComponent {
  private router = inject(Router);
  private service = inject(SymbolsService);
  private destroyRef = inject(DestroyRef);

  public symbols$!: Observable<any>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.symbols$ = this.service.getItemsSorted('updated', 'desc')
      .pipe(takeUntilDestroyed(this.destroyRef));
    this.columns = [
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
      { def: 'type', title: 'Тип', type: ColumnType.Text },
      { def: 'date', title: 'Датум', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/symbol-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/symbol-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onShortcut() {
    this.router.navigateByUrl(`srpsko-nasledje/drzavni-simboli`);
  }
}
