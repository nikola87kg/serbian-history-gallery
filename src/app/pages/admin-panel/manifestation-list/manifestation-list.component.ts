import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { ManifestationService } from '@app/services/manifestations.service';

@Component({
    selector: 'app-manifestation-list',
    imports: [CommonModule, TableComponent, MatButtonModule],
    templateUrl: './manifestation-list.component.html',
    styleUrl: './manifestation-list.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManifestationListComponent {
  private router = inject(Router);
  private service = inject(ManifestationService);

  public manifestations$!: Observable<any>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.manifestations$ = this.service.getItemsSorted('updated', 'desc');
    this.columns = [
      { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'externalLink', title: 'Спољна веза', type: ColumnType.Text },
      { def: 'place', title: 'Локација', type: ColumnType.Text },
      { def: 'type', title: 'Тип', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/manifestation-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/manifestation-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onShortcut() {
    this.router.navigateByUrl(`srpsko-nasledje/manifestacije`);
  }
}
