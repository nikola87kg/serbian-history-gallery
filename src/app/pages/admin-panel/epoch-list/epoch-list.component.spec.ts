import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { EpochListComponent } from './epoch-list.component';

describe('EpochListComponent', () => {
  let component: EpochListComponent;
  let fixture: ComponentFixture<EpochListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [EpochListComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(EpochListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
