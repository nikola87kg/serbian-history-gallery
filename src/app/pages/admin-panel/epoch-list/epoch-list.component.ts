import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@components/table/table.component';
import { EpochsService } from '@services/epochs.service';

@Component({
    selector: 'app-epoch-list',
    imports: [CommonModule, MatButtonModule, TableComponent],
    templateUrl: './epoch-list.component.html',
    styleUrls: ['./epoch-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EpochListComponent {
  private router = inject(Router);
  private service = inject(EpochsService);

  public epochs$!: Observable<any>;

  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.epochs$ = this.service.getItemsSorted('order');
    this.columns = [
      { def: 'id', title: 'ИД', type: ColumnType.Text },
      { def: 'order', title: 'Редослед', type: ColumnType.Number },
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'color', title: 'Боја', type: ColumnType.Color },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/epoch-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/epoch-form/${e.id}`);
  }

  onDeleteButton(id: string) {
    this.service.deleteItem(id);
  }
}
