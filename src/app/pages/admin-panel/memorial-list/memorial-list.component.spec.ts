import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { MemorialListComponent } from './memorial-list.component';

describe('MemorialListComponent', () => {
  let component: MemorialListComponent;
  let fixture: ComponentFixture<MemorialListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MemorialListComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(MemorialListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
