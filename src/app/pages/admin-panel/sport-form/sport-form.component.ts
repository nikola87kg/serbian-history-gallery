import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import {
  SportService, SportSuccess, SportSuccessType, sportSuccessTypes
} from '@services/sport.service';

@Component({
    selector: 'app-sport-form',
    imports: [
        FormTextComponent,
        FormSelectComponent,
        MatButtonModule,
        ReactiveFormsModule
    ],
    templateUrl: './sport-form.component.html',
    styleUrls: ['./sport-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SportFormComponent {
  private service = inject(SportService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);

  public id!: string;
  public buttonLabel!: string;
  public sportSuccessTypes = sportSuccessTypes;

  public form = new FormGroup({
    name: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    yearMonth: new FormControl<string>(''),
    dates: new FormControl<string>(''),
    creator: new FormControl<string>(''),
    creatorSlug: new FormControl<string>(''),
    type: new FormControl<SportSuccessType | null>(null),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: SportSuccess) => {
          this.form.patchValue(response);
        },
      });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  updateItem(): void {
    const payload: SportSuccess = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload).then((_) => {
      this.router.navigateByUrl('admin/panel/sport-list');
    });
  }

  createItem(): void {
    const payload: SportSuccess = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then((response) => {
      this.router.navigateByUrl('admin/panel/sport-list');
    });
  }
}

