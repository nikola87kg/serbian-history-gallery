import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { ContactService, Message } from '@services/contact.service';
import { GlossaryService } from '@services/glossary.service';

@Component({
  selector: 'app-message-list',
  imports: [CommonModule, TableComponent, MatButtonModule],
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageListComponent {
  private service = inject(ContactService);

  public Glossary = GlossaryService.Glossary;
  public messages$!: Observable<Message[]>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.messages$ = this.service.getItemsSorted('created');
    this.columns = [
      { def: 'id', title: this.Glossary.id, type: ColumnType.Text },
      { def: 'email', title: 'Мејл', type: ColumnType.LongText },
      { def: 'type', title: 'Текст поруке', type: ColumnType.Text },
      { def: 'text', title: 'Текст поруке', type: ColumnType.LongText },
      { def: 'created', title: 'Датум', type: ColumnType.Date },
      { def: 'actions', title: this.Glossary.actions, type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);

    this.messages$.subscribe(result => {
      console.log("🚀 ~ MessageListComponent ~ getData ~ result:", result)
    })
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onEditButton(_: any) {
  }
}
