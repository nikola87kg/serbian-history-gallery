import { ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { AdminStateService } from '@app/services/admin-state.service';
import { Song, SongsService } from '@app/services/songs.service';
import { UtilsService } from '@app/services/utils.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-song-list',
  imports: [MatButtonModule, TableComponent, MatFormFieldModule, MatInputModule, ReactiveFormsModule],
  templateUrl: './song-list.component.html',
  styleUrl: './song-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SongListComponent {
  private router = inject(Router);
  private songsService = inject(SongsService);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private adminStateService = inject(AdminStateService);
  private totalList: Song[] = [];

  public songs$!: Observable<any>;
  public items: Song[] = [];
  public searchControl = new FormControl('');
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  setFromStorage() {
    const storageValue = this.adminStateService.getSearchSong();
    this.searchControl.setValue(storageValue);
  }

  getData() {
    this.songs$ = this.songsService.getItemsSorted('updated', 'desc');
    this.songs$.pipe(
      takeUntilDestroyed(this.destroyRef),
    ).subscribe({
      next: (response) => {
        this.totalList = response;
        this.items = [...this.totalList];
        this.listenSearch();
      }
    });

    this.columns = [
      { def: 'title', title: 'Наслов', type: ColumnType.Text },
      { def: 'authorName', title: 'Назив аутора', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'type', title: 'Тип поезије', type: ColumnType.Text },
      { def: 'important', title: 'Издвојени', type: ColumnType.Boolean },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  listenSearch() {
    setTimeout(() => {
      this.setFromStorage();
    }, 5);
    this.searchControl.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe({
        next: text => {
          this.adminStateService.setSearchSong(text || '');
          const textString = text?.toLowerCase() || '';
          this.items = [];
          this.cdr.detectChanges();

          this.items = this.totalList.filter(p => {
            const name = p.authorName?.toLowerCase() || '';
            const startsWithString = name.includes(textString);

            return startsWithString;
          });

          this.cdr.detectChanges();
        }
      });
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/song-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/song-form/${e.id}`);
  }

  onDeleteButton(id: string) {
    this.songsService.deleteItem(id);
  }

  onContextMenuButton(e: any) {
    UtilsService.openOnMiddleClick(`admin/panel/song-form/${e.id}`);
  }
}
