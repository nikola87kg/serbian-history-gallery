import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ArtFormComponent } from './art-form.component';

describe('ArtFormComponent', () => {
  let component: ArtFormComponent;
  let fixture: ComponentFixture<ArtFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ArtFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(ArtFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
