import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@components/table/table.component';
import { CategoriesService } from '@services/categories.service';
import { EpochsService } from '@services/epochs.service';

@Component({
    selector: 'app-category-list',
    imports: [CommonModule, MatButtonModule, TableComponent],
    templateUrl: './category-list.component.html',
    styleUrls: ['./category-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryListComponent {
  private router = inject(Router);
  private service = inject(CategoriesService);
  private epochService = inject(EpochsService);

  public categories$!: Observable<any>;
  public epochs$!: Observable<any>;

  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.categories$ = this.service.getItemsSorted('name');
    this.epochService.getItems().subscribe({
      next: (epochs: any) => {
        const list = [...epochs];
        this.columns = [
          { def: 'order', title: 'Редослед', type: ColumnType.Number },
          { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
          { def: 'name', title: 'Назив', type: ColumnType.Text },
          { def: 'slug', title: 'Слуг', type: ColumnType.Text },
          { def: 'epochs', title: 'Епохе', type: ColumnType.List, list },
          { def: 'actions', title: 'Акције', type: ColumnType.Actions },
        ];
        this.displayedColumns = this.columns.map(c => c.def);
      },
    });
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/category-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/category-form/${e.id}`);
  }

  onDeleteButton(id: string) {
    this.service.deleteItem(id);
  }
}
