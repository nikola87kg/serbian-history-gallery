import { Observable } from 'rxjs';

import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@components/table/table.component';
import { AdminStateService } from '@services/admin-state.service';
import { Quote, QuotesService } from '@services/quotes.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-quote-list',
  imports: [MatButtonModule, TableComponent, MatFormFieldModule, MatInputModule, ReactiveFormsModule],
  templateUrl: './quote-list.component.html',
  styleUrls: ['./quote-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class QuoteListComponent {
  private router = inject(Router);
  private quotesService = inject(QuotesService);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private adminStateService = inject(AdminStateService);
  private totalList: Quote[] = [];

  public quotes$!: Observable<any>;
  public items: Quote[] = [];
  public searchControl = new FormControl('');
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  setFromStorage() {
    const storageValue = this.adminStateService.getSearchQuote();
    this.searchControl.setValue(storageValue);
  }

  getData() {
    this.quotes$ = this.quotesService.getItemsSorted('updated', 'desc');
    this.quotes$.pipe(
      takeUntilDestroyed(this.destroyRef),
    ).subscribe({
      next: (response) => {
        this.totalList = response;
        this.items = [...this.totalList];
        this.listenSearch();
      }
    });

    this.columns = [
      { def: 'authorName', title: 'Назив аутора', type: ColumnType.Text },
      { def: 'authorSlug', title: 'Слуг аутора', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'text', title: 'Текст', type: ColumnType.Text },
      { def: 'type', title: 'Тип цитата', type: ColumnType.Text },
      { def: 'important', title: 'Издвојени', type: ColumnType.Boolean },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  listenSearch() {
    setTimeout(() => {
      this.setFromStorage();
    }, 5);
    this.searchControl.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe({
        next: text => {
          this.adminStateService.setSearchQuote(text || '');
          const textString = text?.toLowerCase() || '';
          this.items = [];
          this.cdr.detectChanges();

          this.items = this.totalList.filter(p => {
            const name = p.authorName?.toLowerCase() || '';
            const startsWithString = name.includes(textString);

            return startsWithString;
          });

          this.cdr.detectChanges();
        }
      });
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/quote-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/quote-form/${e.id}`);
  }

  onDeleteButton(id: string) {
    this.quotesService.deleteItem(id);
  }

  onContextMenuButton(e: any) {
    UtilsService.openOnMiddleClick(`admin/panel/quote-form/${e.id}`);
  }
}
