import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { AdminStateService } from '@app/services/admin-state.service';
import { UtilsService } from '@app/services/utils.service';
import { ColumnType, TableColumn, TableComponent } from '@components/table/table.component';
import { Tag, TagsService } from '@services/tags.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-tag-list',
  imports: [ReactiveFormsModule, TableComponent, MatButtonModule, MatInputModule, MatFormFieldModule],
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagListComponent {
  private router = inject(Router);
  private adminStateService = inject(AdminStateService);
  private service = inject(TagsService);
  private cdr = inject(ChangeDetectorRef);
  private destroyRef = inject(DestroyRef);

  public tags$!: Observable<Tag[]>;
  public epochs$!: Observable<any>;

  public searchControl = new FormControl('');
  public list: Tag[] = [];
  public items: Tag[] = [];
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();

    setTimeout(() => {
      this.listenSearch();
    }, 1000);
  }

  setFromStorage() {
    const storedValue = this.adminStateService.getSearchTag();
    this.searchControl.setValue(storedValue);
  }

  sortDate = (a: any, b: any) => {
    if (a.area && !b.area) {
      return -1;
    }
    if (a.area && b.area && a.cityDate && !b.cityDate) {
      return -1;
    }
    if (a.area && b.area && a.cityDate && b.cityDate) {
      const aSplit = a.cityDate.split('.');
      const bSplit = b.cityDate.split('.');

      if (aSplit[1] === bSplit[1]) {

        return Number(aSplit[0]) - Number(bSplit[0]);
      }

      return Number(aSplit[1]) - Number(bSplit[1]);
    }

    return a.slug!.localeCompare(b.slug!);
  };

  getData() {
    this.tags$ = this.service.getItemsSorted('updated', 'desc');
    this.tags$.pipe(
      takeUntilDestroyed(this.destroyRef),
    ).subscribe({
      next: (response) => {
        this.list = response;
        this.items = response;
        this.listenSearch();
      }
    });

    this.columns = [
      { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'population', title: 'Популација', type: ColumnType.Text },
      { def: 'cityDate', title: 'Дан града', type: ColumnType.Text },
      { def: 'region', title: 'Округ', type: ColumnType.Text },
      { def: 'area', title: 'Површина', type: ColumnType.Text },
      { def: 'height', title: 'Висина', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  listenSearch() {
    setTimeout(() => {
      this.setFromStorage();
    }, 5);
    this.searchControl.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe({
        next: text => {
          this.adminStateService.setSearchTag(text || '');
          const textString = text?.toLowerCase() || '';
          this.items = [];
          this.cdr.detectChanges();

          this.items = this.list.filter(p => {
            const name = p.name?.toLowerCase() || '';
            const startsWithString = name.includes(textString);

            return startsWithString;
          });

          this.cdr.detectChanges();
        }
      });
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/tag-form`);
  }

  onContextMenuButton(e: any) {
    UtilsService.openOnMiddleClick(`admin/panel/tag-form/${e.id}`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/tag-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }
}
