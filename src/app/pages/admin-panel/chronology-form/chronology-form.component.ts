import { QuillModule } from 'ngx-quill';

import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormNumberComponent } from '@app/components/form-number/form-number.component';
import { ImageDialogComponent } from '@app/dialogs/image-dialog/image-dialog.component';
import { FormSelectComponent } from '@components/form-select/form-select.component';
import { FormTextComponent } from '@components/form-text/form-text.component';
import { CategoriesService } from '@services/categories.service';
import {
  Chronology, ChronologyService, ChronologyType, chronologyTypes
} from '@services/chronology.service';
import { EpochsService } from '@services/epochs.service';

import { FormImageComponent } from '../../../components/form-image/form-image.component';

@Component({
  selector: 'app-chronology-form',
  templateUrl: './chronology-form.component.html',
  styleUrls: ['./chronology-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FormTextComponent,
    FormSelectComponent,
    FormNumberComponent,
    MatButtonModule,
    ReactiveFormsModule,
    QuillModule,
    FormImageComponent
  ]
})
export class ChronologyFormComponent {
  private service = inject(ChronologyService);
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private location = inject(Location);
  private cdr = inject(ChangeDetectorRef);
  private matDialog = inject(MatDialog);

  public id!: string;
  public allEpochs: any[] = [];
  public epochs: any[] = [];
  public categories: any[] = [];
  public buttonLabel!: string;
  public chronologyTypes = chronologyTypes;

  get categoryControl(): FormControl {
    return this.form.controls.categoryId;
  }

  get epochControl(): FormControl {
    return this.form.controls.epochId;
  }

  public form = new FormGroup({
    imagePath: new FormControl<string>(''),
    innerHtml: new FormControl<string>(''),
    age: new FormControl<number>(0),
    typeId: new FormControl<ChronologyType | null>(null),
    title: new FormControl<string | null>(null),
    wikipedia: new FormControl<string | null>(null),
    imdb: new FormControl<string | null>(null),
    categoryId: new FormControl<string | null>(null),
    epochId: new FormControl<string | null>(null),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    } else {
      this.handleCategories();
    }
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name').subscribe({
      next: (response: any) => {
        this.categories = [...response];
        this.handleEpochs();
      },
    });
  }

  handleEpochs(): void {
    this.epochsService.getItemsSorted('order').subscribe({
      next: (response: any) => {
        this.allEpochs = [...response];
        this.cdr.detectChanges();
        this.setListeners();
        this.updateEpochs(this.categoryControl.value);
      },
    });
  }

  setListeners() {
    this.categoryControl.valueChanges.subscribe({
      next: (newValue: string) => {
        this.updateEpochs(newValue);
        this.epochControl.setValue(undefined);
        this.cdr.detectChanges();
      },
    });
  }

  updateEpochs(categoryId: string) {
    const selectedCategory = this.categories.find((c) => c.id === categoryId);
    this.epochs = this.allEpochs.filter((e) =>
      selectedCategory?.epochs.includes(e.id)
    );
  }

  onImage(): void {
    const payload: Chronology = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.openImageDialog(payload, { id: this.id }, true);
  }

  openImageDialog(item: Chronology, response: any, edited = false): void {
    const { id } = response;

    if (!id) {
      return;
    }

    const service = this.service;
    const defaultFormat = 'box';
    const dialogRef = this.matDialog.open(ImageDialogComponent, {
      width: '60rem',
      data: { item, service, id, defaultFormat },
      scrollStrategy: new NoopScrollStrategy(),
    });
    dialogRef.afterClosed().subscribe({
      next: (confirm: boolean) => {
        if (confirm && !edited) {
          this.location.back();
        }
      },
    });
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Chronology) => {
          this.form.patchValue(response);
          this.handleCategories();
        },
      });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  updateItem(): void {
    const payload: Chronology = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();

    this.service.updateItem(this.id, payload)
      .then((_) => this.location.back());
  }

  createItem(): void {
    const payload: Chronology = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();

    this.service.createItem(payload)
      .then((_) => this.location.back());
  }
}
