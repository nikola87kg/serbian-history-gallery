import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ChronologyFormComponent } from './chronology-form.component';

describe('ChronologyFormComponent', () => {
  let component: ChronologyFormComponent;
  let fixture: ComponentFixture<ChronologyFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ChronologyFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(ChronologyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
