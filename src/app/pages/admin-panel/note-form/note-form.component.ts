import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { Location } from '@angular/common';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import { FormTextareaComponent } from '@app/components/form-textarea/form-textarea.component';
import { Note, NotesService, NoteType, noteTypes } from '@app/services/notes.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-note-form',
    imports: [FormTextComponent,
        FormSelectComponent,
        FormTextareaComponent,
        MatButtonModule,
        ReactiveFormsModule],
    templateUrl: './note-form.component.html',
    styleUrl: './note-form.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoteFormComponent {
  private service = inject(NotesService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private location = inject(Location);

  public types = noteTypes;
  public id!: string;
  public buttonLabel!: string;
  public form = new FormGroup({
    title: new FormControl<string>(''),
    text: new FormControl<string>(''),
    type: new FormControl<NoteType | null>(null),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Note) => {
          this.form.patchValue(response);
        },
      });
  }

  updateItem(): void {
    const payload: Note = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload)
      .then((_) => this.location.back());
  }

  createItem(): void {
    const payload: Note = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload)
      .then((_) => this.location.back());
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }
}
