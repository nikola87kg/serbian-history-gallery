import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { FormNumberComponent } from '@app/components/form-number/form-number.component';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import {
  Instagram, InstagramPostType, InstagramService, instragramTypes
} from '@app/services/instagram.service';

@Component({
  selector: 'app-instagram-form',
  imports: [FormTextComponent,
    FormSelectComponent,
    FormNumberComponent,
    MatButtonModule,
    ReactiveFormsModule],
  templateUrl: './instagram-form.component.html',
  styleUrl: './instagram-form.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstagramFormComponent {
  private service = inject(InstagramService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private location = inject(Location);

  public types = instragramTypes;
  public id!: string;
  public buttonLabel!: string;

  public form = new FormGroup({
    title: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    embeddedCode: new FormControl<string>(''),
    type: new FormControl<InstagramPostType | null>(null),
    likes: new FormControl<number | null>(null),
    comments: new FormControl<number | null>(null),
    shares: new FormControl<number | null>(null),
    saves: new FormControl<number | null>(null),
    reach: new FormControl<number | null>(null),
    views: new FormControl<number | null>(null),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Instagram) => {
          this.form.patchValue(response);
        },
      });
  }

  updateItem(): void {
    const payload: Instagram = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload)
      .then((_) => this.location.back());
  }

  createItem(): void {
    const payload: Instagram = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload)
      .then((_) => this.location.back());
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }
}
