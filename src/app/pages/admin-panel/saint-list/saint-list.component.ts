import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { SaintsService } from '@services/saints.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-saint-list',
  imports: [CommonModule, TableComponent, MatButtonModule],
  templateUrl: './saint-list.component.html',
  styleUrls: ['./saint-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SaintListComponent {
  private router = inject(Router);
  private service = inject(SaintsService);

  public saints$!: Observable<any>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.saints$ = this.service.getItemsSorted('updated', 'desc');
    this.columns = [
      { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'type', title: 'Тип', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/saint-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/saint-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onContextMenuButton(e: any) {
    UtilsService.openOnMiddleClick(`admin/panel/saint-form/${e.id}`);
  }

  onShortcut() {
    this.router.navigateByUrl(`srpsko-nasledje/slave-i-praznici`);
  }
}

