import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SaintListComponent } from './saint-list.component';

describe('SaintListComponent', () => {
  let component: SaintListComponent;
  let fixture: ComponentFixture<SaintListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SaintListComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(SaintListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
