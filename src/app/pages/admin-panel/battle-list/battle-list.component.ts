import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@components/table/table.component';
import { BattleService } from '@services/battle.service';

@Component({
    selector: 'app-battle-list',
    imports: [CommonModule, TableComponent, MatButtonModule],
    templateUrl: './battle-list.component.html',
    styleUrls: ['./battle-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BattleListComponent {
  private router = inject(Router);
  private service = inject(BattleService);

  public battles$!: Observable<any>;
  public displayedColumns: string[] = [];
  public columns: TableColumn[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.battles$ = this.service.getItemsSorted('updated', 'desc');
    this.columns = [
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'leader', title: 'Лидер', type: ColumnType.Text },
      { def: 'dates', title: 'Датуми', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/battle-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/battle-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }
}
