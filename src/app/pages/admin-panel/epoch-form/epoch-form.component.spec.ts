import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { EpochFormComponent } from './epoch-form.component';

describe('EpochFormComponent', () => {
  let component: EpochFormComponent;
  let fixture: ComponentFixture<EpochFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [EpochFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(EpochFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
