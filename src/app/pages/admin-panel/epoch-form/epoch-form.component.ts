
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { FormNumberComponent } from '@app/components/form-number/form-number.component';
import { FormColorComponent } from '@components/form-color/form-color.component';
import { FormTextComponent } from '@components/form-text/form-text.component';
import { Epoch, EpochsService } from '@services/epochs.service';

@Component({
  selector: 'app-epoch-form',
  imports: [
    FormColorComponent,
    FormNumberComponent,
    FormTextComponent,
    ReactiveFormsModule,
    MatButtonModule
  ],
  templateUrl: './epoch-form.component.html',
  styleUrls: ['./epoch-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EpochFormComponent {
  private service = inject(EpochsService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);

  public id!: string;
  public buttonLabel!: string;

  public form = new FormGroup({
    name: new FormControl<string>(''),
    order: new FormControl<number>(0),
    slug: new FormControl<string>(''),
    color: new FormControl<string>(''),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Epoch) => {
          this.form.patchValue(response);
        },
      });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  updateItem(): void {
    const payload: Epoch = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload).then((_) => {
      this.router.navigateByUrl('admin/panel/epoch-list');
    });
  }

  createItem(): void {
    const payload: Epoch = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then(_ => {
      this.router.navigateByUrl('admin/panel/epoch-list');
    });
  }
}
