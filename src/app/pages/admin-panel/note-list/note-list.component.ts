import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { BasicSheetComponent } from '@app/dialogs/basic-sheet/basic-sheet.component';
import { GlossaryService } from '@app/services/glossary.service';
import { NotesService } from '@app/services/notes.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-note-list',
  imports: [CommonModule, TableComponent, MatButtonModule],
  templateUrl: './note-list.component.html',
  styleUrl: './note-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoteListComponent {
  private service = inject(NotesService);
  private router = inject(Router);
  private matBottomSheet = inject(MatBottomSheet);

  public notes$!: Observable<any>;
  public Glossary = GlossaryService.Glossary;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.notes$ = this.service.getItemsSorted('title', 'desc');

    this.columns = [
      { def: 'title', title: 'Назив', type: ColumnType.Text },
      { def: 'text', title: 'Текст', type: ColumnType.BreakText },
      { def: 'actions', title: this.Glossary.actions, type: ColumnType.Actions },
    ];

    this.displayedColumns = this.columns.map(c => c.def);

  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/note-form/${e.id}`);
  }

  onCellClick(text: string) {
    const sheet = BasicSheetComponent<any>;
    const panelClass = 'basic-sheet-overlay';
    const scrollStrategy = new NoopScrollStrategy();
    const disableClose = false;
    const data = { item: text };

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, panelClass, scrollStrategy, disableClose
    });

    sheetRef.afterDismissed().subscribe();
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/note-form`);
  }

}
