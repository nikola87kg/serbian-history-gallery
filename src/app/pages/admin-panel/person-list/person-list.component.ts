import { Observable } from 'rxjs';

import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@components/table/table.component';
import { AdminStateService } from '@services/admin-state.service';
import { Person, PersonsService } from '@services/persons.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-person-list',
  imports: [MatButtonModule, TableComponent, ReactiveFormsModule, MatInputModule, MatFormFieldModule, MatIconModule],
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonListComponent {
  private router = inject(Router);
  private service = inject(PersonsService);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private adminStateService = inject(AdminStateService);

  public persons$!: Observable<Person[]>;
  public list: Person[] = [];
  public items: Person[] = [];
  public searchControl = new FormControl('');

  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  setFromStorage() {
    const storedValue = this.adminStateService.getSearchPerson();
    this.searchControl.setValue(storedValue);
  }

  listenSearch() {
    setTimeout(() => {
      this.setFromStorage();
    }, 5);
    this.searchControl.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe({
        next: text => {
          this.adminStateService.setSearchPerson(text || '');
          const textString = text?.toLowerCase() || '';
          this.items = [];
          this.cdr.detectChanges();

          this.items = this.list.filter(p => {
            const city = p.city?.toLowerCase() || '';
            const commonName = p.commonName?.toLowerCase() || '';
            const fullTitle = p.fullTitle?.toLowerCase() || '';
            const result = city.includes(textString)
              || commonName.includes(textString)
              || fullTitle.includes(textString);

            return result;
          });

          this.cdr.detectChanges();
        }
      });
  }


  getData() {
    this.persons$ = this.service.getItemsSorted('updated', 'desc');
    this.persons$.pipe(
      takeUntilDestroyed(this.destroyRef),
    ).subscribe({
      next: (response) => {
        this.list = response;
        this.listenSearch();
      }
    });

    this.columns = [
      { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
      { def: 'commonName', title: 'Име', type: ColumnType.Text },
      { def: 'nickName', title: 'Надимак', type: ColumnType.Text },
      { def: 'fullTitle', title: 'Занимање', type: ColumnType.Text },
      { def: 'city', title: 'Град', type: ColumnType.Text },
      { def: 'tags', title: 'Тагови', type: ColumnType.Text },
      { def: 'birthDeath', title: 'Датуми', type: ColumnType.Text },
      { def: 'age', title: 'Године', type: ColumnType.Number },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },];

    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/person-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/person-form/${e.id}`);
  }

  onContextMenuButton(e: any) {
    UtilsService.openOnMiddleClick(`admin/panel/person-form/${e.id}`);
  }

  onDeleteButton(id: string) {
    this.service.deleteItem(id);
  }

  onExtraButton(e: any) {
    this.router.navigateByUrl(`biografija/${e.slug}`);
  }

  onShortcut() {
    const path = UtilsService.getTodaySlug();
    this.router.navigateByUrl(`kalendar/${path}/rodjeni`);
  }
}
