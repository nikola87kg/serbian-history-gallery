import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { Art, ArtService } from '@services/art.service';

@Component({
    selector: 'app-art-list',
    imports: [CommonModule, TableComponent, MatButtonModule],
    templateUrl: './art-list.component.html',
    styleUrls: ['./art-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArtListComponent {
  private router = inject(Router);
  private service = inject(ArtService);

  public arts$!: Observable<Art[]>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.arts$ = this.service.getItemsSorted('updated', 'desc');
    this.columns = [
      { def: 'id', title: 'ИД', type: ColumnType.Text },
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'creator', title: 'Креатор', type: ColumnType.Text },
      { def: 'dates', title: 'Датуми', type: ColumnType.Text },
      { def: 'type', title: 'Тип', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/art-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/art-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }
}
