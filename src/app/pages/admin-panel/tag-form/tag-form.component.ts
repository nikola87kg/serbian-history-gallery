import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormCheckboxComponent } from '@app/components/form-checkbox/form-checkbox.component';
import { FormImageComponent } from '@app/components/form-image/form-image.component';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextareaComponent } from '@app/components/form-textarea/form-textarea.component';
import { ImageDialogComponent } from '@app/dialogs/image-dialog/image-dialog.component';
import { FormTextComponent } from '@components/form-text/form-text.component';
import { Tag, TagGroup, tagGroups, TagsService } from '@services/tags.service';

@Component({
    selector: 'app-tag-form',
    templateUrl: './tag-form.component.html',
    styleUrls: ['./tag-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [
        FormTextComponent,
        FormImageComponent,
        FormSelectComponent,
        FormCheckboxComponent,
        FormTextareaComponent,
        MatButtonModule,
        ReactiveFormsModule
    ]
})
export class TagFormComponent {
  private service = inject(TagsService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private matDialog = inject(MatDialog);
  private location = inject(Location);

  public id!: string;
  public buttonLabel!: string;
  public tagItems = tagGroups;

  public form = new FormGroup({
    name: new FormControl<string>(''),
    description: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    population: new FormControl<string>(''),
    imagePath: new FormControl<string>(''),
    region: new FormControl<string>(''),
    height: new FormControl<string>(''),
    postNumber: new FormControl<string>(''),
    cityDate: new FormControl<string>(''),
    callPrefix: new FormControl<string>(''),
    trending: new FormControl<boolean>(false),
    licencePlate: new FormControl<string>(''),
    area: new FormControl<string>(''),
    cityHistory: new FormControl<string>(''),
    cityGeography: new FormControl<string>(''),
    cityDemography: new FormControl<string>(''),
    cityCulture: new FormControl<string>(''),
    group: new FormControl<TagGroup | null>(null),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Tag) => {
          this.form.patchValue(response);
        },
      });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  updateItem(): void {
    const payload: Tag = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload)
      .then((_) => this.location.back());
  }

  createItem(): void {
    const payload: Tag = this.form.getRawValue();

    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then((response) => {
      this.openImageDialog(payload, response);
    });
  }

  onImage(): void {
    const payload: Tag = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.openImageDialog(payload, { id: this.id }, true);
  }

  openImageDialog(item: Tag, response: any, edited = false): void {
    const { id } = response;

    if (!id) {
      return;
    }

    const service = this.service;
    const dialogRef = this.matDialog.open(ImageDialogComponent, {
      width: '60rem',
      data: { item, service, id },
      scrollStrategy: new NoopScrollStrategy(),
    });
    dialogRef.afterClosed().subscribe({
      next: (confirm: boolean) => {
        if (confirm && !edited) {
          this.location.back();
        }
      },
    });
  }
}
