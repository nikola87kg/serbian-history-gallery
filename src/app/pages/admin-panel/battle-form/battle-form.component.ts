
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import { FormUrlComponent } from '@app/components/form-url/form-url.component';
import { Battle, BattleService } from '@services/battle.service';
import { GlossaryService } from '@services/glossary.service';

@Component({
  selector: 'app-battle-form',
  imports: [
    FormTextComponent,
    MatButtonModule,
    ReactiveFormsModule,
    FormUrlComponent
  ],
  templateUrl: './battle-form.component.html',
  styleUrls: ['./battle-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BattleFormComponent {
  private service = inject(BattleService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);

  public Glossary = GlossaryService.Glossary;
  public id!: string;
  public buttonLabel!: string;

  public form = new FormGroup({
    battleOutcome: new FormControl<string>(''),
    dates: new FormControl<string>(''),
    enemyEstimation: new FormControl<string>(''),
    enemyLeader: new FormControl<string>(''),
    enemySide: new FormControl<string>(''),
    leader: new FormControl<string>(''),
    leaderSlug: new FormControl<string>(''),
    name: new FormControl<string>(''),
    ourSide: new FormControl<string>(''),
    ourSideEstimation: new FormControl<string>(''),
    place: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    wikipedia: new FormControl<string>(''),
    yearMonth: new FormControl<string>(''),
    youtube: new FormControl<string>(''),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Battle) => {
          this.form.patchValue(response);
        },
      });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  updateItem(): void {
    const payload: Battle = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload).then((_) => {
      this.router.navigateByUrl('admin/panel/battle-list');
    });
  }

  createItem(): void {
    const payload: Battle = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then(_ => {
      this.router.navigateByUrl('admin/panel/battle-list');
    });
  }
}
