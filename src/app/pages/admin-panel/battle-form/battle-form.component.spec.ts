import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { BattleFormComponent } from './battle-form.component';

describe('BattleFormComponent', () => {
  let component: BattleFormComponent;
  let fixture: ComponentFixture<BattleFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BattleFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(BattleFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
