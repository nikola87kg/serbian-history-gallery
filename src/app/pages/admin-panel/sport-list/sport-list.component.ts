import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { SportService, SportSuccess } from '@services/sport.service';

@Component({
    selector: 'app-sport-list',
    imports: [CommonModule, TableComponent, MatButtonModule],
    templateUrl: './sport-list.component.html',
    styleUrls: ['./sport-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SportListComponent {
  private router = inject(Router);
  private service = inject(SportService);

  public sports$!: Observable<SportSuccess[]>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.sports$ = this.service.getItemsSorted('updated', 'desc');
    this.columns = [
      { def: 'id', title: 'ИД', type: ColumnType.Text },
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'slug', title: 'Слуг', type: ColumnType.Text },
      { def: 'creator', title: 'Освајач', type: ColumnType.Text },
      { def: 'dates', title: 'Датуми', type: ColumnType.Text },
      { def: 'type', title: 'Тип', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/sport-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/sport-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }
}
