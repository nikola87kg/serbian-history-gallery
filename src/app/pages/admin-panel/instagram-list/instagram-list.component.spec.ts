import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InstagramListComponent } from './instagram-list.component';

describe('InstagramListComponent', () => {
  let component: InstagramListComponent;
  let fixture: ComponentFixture<InstagramListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [InstagramListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(InstagramListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
