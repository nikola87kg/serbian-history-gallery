import { first, Observable, tap } from 'rxjs';

import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { BasicSheetComponent } from '@app/dialogs/basic-sheet/basic-sheet.component';
import { GlossaryService } from '@app/services/glossary.service';
import { InstagramService } from '@app/services/instagram.service';

@Component({
  selector: 'app-instagram-list',
  imports: [CommonModule, TableComponent, MatButtonModule],
  templateUrl: './instagram-list.component.html',
  styleUrl: './instagram-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InstagramListComponent {
  private service = inject(InstagramService);
  private router = inject(Router);

  public instagramPosts$!: Observable<any>;
  public Glossary = GlossaryService.Glossary;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];
  private matBottomSheet = inject(MatBottomSheet);

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.instagramPosts$ = this.service.getItemsSorted('created', 'desc');

    this.columns = [
      { def: 'embeddedCode', title: 'Линк', type: ColumnType.Embedded },
      { def: 'created', title: 'Направљен', type: ColumnType.Date },
      { def: 'title', title: 'Назив', type: ColumnType.Text },
      { def: 'reach', title: 'Статистика', type: ColumnType.Instagram },
      { def: 'updated', title: 'Ажурирано', type: ColumnType.Date },
      { def: 'actions', title: this.Glossary.actions, type: ColumnType.Actions },
    ];

    this.displayedColumns = this.columns.map(c => c.def);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/instagram-form/${e.id}`);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/instagram-form`);
  }

  getCumulativeStats(posts: any[]): any[] {
    return posts
      .reduce((acc, post) => {
        const date = new Date(post.created);
        const month = `${date.getFullYear()}-${String(date.getMonth() + 1).padStart(2, '0')}`;

        // Find existing entry for the month
        let existingEntry = acc.find((item: any) => item.month === month);

        if (!existingEntry) {
          existingEntry = {
            month,
            posts: 0,
            views: 0,
            views1: 0,
            reach: 0,
            reach1: 0,
            likesNO: 0,
            likes1: 0,
            sharesNO: 0,
            shares1: 0,
            savesNO: 0,
            saves1: 0
          };
          acc.push(existingEntry);
        }

        // Accumulate totals
        existingEntry.likesNO += post.likes;
        existingEntry.reach += post.reach;
        existingEntry.views += post.views;
        existingEntry.sharesNO += post.shares;
        existingEntry.savesNO += post.saves;
        existingEntry.posts += 1;

        // Compute per-post averages
        existingEntry.likes1 = existingEntry.likesNO / existingEntry.posts;
        existingEntry.reach1 = existingEntry.reach / existingEntry.posts;
        existingEntry.shares1 = existingEntry.sharesNO / existingEntry.posts;
        existingEntry.saves1 = existingEntry.savesNO / existingEntry.posts;
        existingEntry.views1 = existingEntry.views / existingEntry.posts;

        return acc;
      }, [] as any[]);
  }

  async onStatButton() {
    const sheet = BasicSheetComponent<any>;
    const panelClass = 'basic-sheet-overlay';
    const scrollStrategy = new NoopScrollStrategy();
    const disableClose = false;
    this.instagramPosts$.pipe(
      first(),
      tap((posts) => {
        const items = this.getCumulativeStats(posts);
        const columns = Object.keys(items[0]).map((post: any) => {
          return {
            def: post,
            title: post,
            type: post === 'month' ? ColumnType.Text : ColumnType.Number
          }
        })

        const displayedColumns = columns.map(c => c.def).filter(c => !c.includes('NO'));

        const data = { table: { columns, displayedColumns, items } };

        const sheetRef = this.matBottomSheet.open(sheet, {
          data, panelClass, scrollStrategy, disableClose
        });

        sheetRef.afterDismissed().subscribe();
      })
    ).subscribe();
  }
}
