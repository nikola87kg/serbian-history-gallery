import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { FormCheckboxComponent } from '@app/components/form-checkbox/form-checkbox.component';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import { Song, SongsService, SongType, songTypes } from '@app/services/songs.service';
import { QuillModule } from 'ngx-quill';

@Component({
  selector: 'app-song-form',
  imports: [
    FormTextComponent,
    FormSelectComponent,
    FormCheckboxComponent,
    QuillModule,
    MatButtonModule,
    ReactiveFormsModule
  ],
  templateUrl: './song-form.component.html',
  styleUrl: './song-form.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SongFormComponent {
  private service = inject(SongsService);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private location = inject(Location);

  public id!: string;
  public songTypes = songTypes;
  public buttonLabel!: string;

  public form = new FormGroup({
    innerHtml: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    type: new FormControl<SongType | null>(null),
    title: new FormControl<string | null>(null),
    authorName: new FormControl<string>(''),
    authorSlug: new FormControl<string>(''),
    important: new FormControl<boolean>(false),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Song) => {
          this.form.patchValue(response);
        },
      });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  onDelete(): void {
    this.service.deleteItem(this.id).then((_) => {
      this.location.back();
    });
  }

  updateItem(): void {
    const payload: Song = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload).then((_) => {
      this.location.back();
    });
  }

  createItem(): void {
    const payload: Song = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then((_) => {
      this.location.back();
    });
  }

}
