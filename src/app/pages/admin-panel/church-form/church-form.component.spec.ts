import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ChurchFormComponent } from './church-form.component';

describe('ChurchFormComponent', () => {
  let component: ChurchFormComponent;
  let fixture: ComponentFixture<ChurchFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ChurchFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(ChurchFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
