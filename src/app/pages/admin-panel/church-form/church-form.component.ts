import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Location } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormImageComponent } from '@app/components/form-image/form-image.component';
import { FormSelectComponent } from '@app/components/form-select/form-select.component';
import { FormTextComponent } from '@app/components/form-text/form-text.component';
import { FormTextareaComponent } from '@app/components/form-textarea/form-textarea.component';
import { ImageDialogComponent } from '@app/dialogs/image-dialog/image-dialog.component';
import { Church, ChurchService, ChurchType, churchTypes } from '@services/church.service';

@Component({
    selector: 'app-church-form',
    imports: [
        FormTextComponent,
        FormSelectComponent,
        MatButtonModule,
        FormImageComponent,
        FormTextareaComponent,
        ReactiveFormsModule,
        MatDialogModule
    ],
    templateUrl: './church-form.component.html',
    styleUrls: ['./church-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChurchFormComponent {
  private service = inject(ChurchService);
  private router = inject(Router);
  private matDialog = inject(MatDialog);
  private location = inject(Location);

  public id!: string;
  public buttonLabel!: string;
  public churchTypes = churchTypes;

  public form = new FormGroup({
    dates: new FormControl<string>(''),
    description: new FormControl<string>(''),
    founder: new FormControl<string>(''),
    founderSlug: new FormControl<string>(''),
    imagePath: new FormControl<string>(''),
    name: new FormControl<string>(''),
    place: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    yearMonth: new FormControl<string>(''),
    type: new FormControl<ChurchType | null>(null),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id).subscribe({
      next: (response: Church) => {
        this.form.patchValue(response);
      },
    });
  }

  updateItem(): void {
    const payload: Church = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload)
      .then((_) => this.location.back());
  }

  createItem(): void {
    const payload: Church = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then((response) => {
      this.openImageDialog(payload, response);
    });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  onImage(): void {
    const payload: Church = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.openImageDialog(payload, { id: this.id }, true);
  }

  openImageDialog(item: Church, response: any, edited = false): void {
    const { id } = response;

    if (!id) {
      return;
    }

    const service = this.service;
    const defaultFormat = 'landscape';
    const dialogRef = this.matDialog.open(ImageDialogComponent, {
      width: '60rem',
      data: { item, service, defaultFormat, id },
      scrollStrategy: new NoopScrollStrategy(),
    });
    dialogRef.afterClosed().subscribe({
      next: (confirm: boolean) => {
        if (confirm && !edited) {
          this.location.back();
        }
      },
    });
  }
}
