import { NoopScrollStrategy } from '@angular/cdk/overlay';

import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormImageComponent } from '@app/components/form-image/form-image.component';
import { FormNumberComponent } from '@app/components/form-number/form-number.component';
import { ImageDialogComponent } from '@app/dialogs/image-dialog/image-dialog.component';
import { FormMultiselectComponent } from '@components/form-multiselect/form-multiselect.component';
import { FormTextComponent } from '@components/form-text/form-text.component';
import { CategoriesService, Category } from '@services/categories.service';
import { Epoch, EpochsService } from '@services/epochs.service';

@Component({
  selector: 'app-category-form',
  imports: [
    FormTextComponent,
    FormMultiselectComponent,
    FormImageComponent,
    FormNumberComponent,
    ReactiveFormsModule,
    MatButtonModule,
    MatDialogModule
  ],
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryFormComponent {
  private service = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private router = inject(Router);
  private matDialog = inject(MatDialog);
  private destroyRef = inject(DestroyRef);

  public id!: string;
  public buttonLabel!: string;
  public allEpochs: Epoch[] = [];

  public form = new FormGroup({
    name: new FormControl<string>(''),
    slug: new FormControl<string>(''),
    order: new FormControl<number>(0),
    imagePath: new FormControl<string>(''),
    epochs: new FormControl<string[]>([]),
    created: new FormControl<number | null>(null),
    updated: new FormControl<number | null>(null),
  });

  ngOnInit(): void {
    this.handleId();
    this.getEpochs();
  }

  getEpochs() {
    this.epochsService.getItemsSorted('order')
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: any) => {
          this.allEpochs = [...response];
        },
      });
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    if (this.id) {
      this.getItem();
    }
  }

  getItem(): void {
    this.service.getItem(this.id).subscribe({
      next: (response: Category) => {
        this.form.patchValue(response);
      },
    });
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  updateItem(): void {
    const payload: Category = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload).then((_) => {
      this.router.navigateByUrl('admin/panel/category-list');
    });
  }

  createItem(): void {
    const payload: Category = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then(_ => {
      this.router.navigateByUrl('admin/panel/category-list');
    });
  }

  onImage(): void {
    const payload: Category = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.openImageDialog(payload, { id: this.id }, true);
  }

  openImageDialog(item: Category, response: any, edited = false): void {
    const { id } = response;

    if (!id) {
      return;
    }

    const service = this.service;
    const dialogRef = this.matDialog.open(ImageDialogComponent, {
      width: '60rem',
      data: { item, service, id },
      scrollStrategy: new NoopScrollStrategy(),
    });
    dialogRef.afterClosed().subscribe({
      next: (confirm: boolean) => {
        if (confirm && !edited) {
          this.router.navigateByUrl('admin/panel/saint-list');
        }
      },
    });
  }
}
