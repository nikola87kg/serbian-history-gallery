import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { GlossaryService } from '@services/glossary.service';
import { UserProfile, UserProfileService } from '@services/user-profile.service';

@Component({
    selector: 'app-user-list',
    imports: [CommonModule, TableComponent, MatButtonModule],
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListComponent {
  private service = inject(UserProfileService);

  public Glossary = GlossaryService.Glossary;
  public users$!: Observable<UserProfile[]>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this.users$ = this.service.getItemsSorted('name');
    this.columns = [
      { def: 'image', title: 'Фото', type: ColumnType.Image },
      { def: 'name', title: 'Корисничко име', type: ColumnType.Text },
      { def: 'email', title: 'Имејл', type: ColumnType.Text },
      { def: 'isAdmin', title: 'Админ', type: ColumnType.Boolean },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];

    this.displayedColumns = this.columns.map(c => c.def);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onEditButton(_: any) {
  }
}
