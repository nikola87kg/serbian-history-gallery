import { QuillModule } from 'ngx-quill';
import { first } from 'rxjs';

import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FormCheckboxComponent } from '@app/components/form-checkbox/form-checkbox.component';
import { FormImageComponent } from '@components/form-image/form-image.component';
import { FormMultiselectComponent } from '@components/form-multiselect/form-multiselect.component';
import { FormNumberComponent } from '@components/form-number/form-number.component';
import { FormSelectComponent } from '@components/form-select/form-select.component';
import { FormTextComponent } from '@components/form-text/form-text.component';
import { FormTextareaComponent } from '@components/form-textarea/form-textarea.component';
import { FormUrlComponent } from '@components/form-url/form-url.component';
import { ImageDialogComponent } from '@dialogs/image-dialog/image-dialog.component';
import { CategoriesService } from '@services/categories.service';
import { EpochsService } from '@services/epochs.service';
import { GlossaryService } from '@services/glossary.service';
import { Person, PersonsService } from '@services/persons.service';
import { TagsService } from '@services/tags.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-person-form',
  imports: [
    FormTextComponent,
    FormTextareaComponent,
    FormSelectComponent,
    FormImageComponent,
    FormCheckboxComponent,
    FormMultiselectComponent,
    FormNumberComponent,
    FormUrlComponent,
    ReactiveFormsModule,
    MatButtonModule,
    QuillModule,
    MatDialogModule
  ],
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonFormComponent {
  private service = inject(PersonsService);
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private tagsService = inject(TagsService);
  private matDialog = inject(MatDialog);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private location = inject(Location);
  private cdr = inject(ChangeDetectorRef);

  public id!: string;
  public buttonLabel!: string;
  public categories: any[] = [];
  public allEpochs: any[] = [];
  public epochs: any[] = [];
  public allTags: any[] = [];
  public Glossary = GlossaryService.Glossary;

  get categoryControl(): FormControl {
    return this.form.controls.category;
  }

  get epochControl(): FormControl {
    return this.form.controls.epoch;
  }

  public form = new FormGroup({
    age: new FormControl<number | null>(null),
    biographyUrl: new FormControl<string>(''),
    birthDay: new FormControl<number | null>(null),
    birthDeath: new FormControl<string>('', Validators.required),
    birthMonth: new FormControl<number | null>(null),
    birthYear: new FormControl<number | null>(null, Validators.required),
    category: new FormControl<string>('', Validators.required),
    city: new FormControl<string>('', Validators.required),
    commonName: new FormControl<string>('', Validators.required),
    created: new FormControl<number | null>(null),
    epoch: new FormControl<string>('', Validators.required),
    firstName: new FormControl<string>('', Validators.required),
    frontPage: new FormControl<boolean>(false, { nonNullable: true }),
    topTen: new FormControl<boolean>(false, { nonNullable: true }),
    localHero: new FormControl<boolean>(false, { nonNullable: true }),
    fullName: new FormControl<string>('', Validators.required),
    fullTitle: new FormControl<string>('', Validators.required),
    imagePath: new FormControl<string>(''),
    instagramUrl: new FormControl<string>(''),
    instagramPosts: new FormControl<string>(''),
    keyNotes: new FormControl<string>(''),
    lastName: new FormControl<string>('', Validators.required),
    nickName: new FormControl<string>(''),
    porekloUrl: new FormControl<string>(''),
    shortDescription: new FormControl<string>('', [
      UtilsService.validateDoubleSpace,
      Validators.required,
      Validators.maxLength(240)]
    ),
    slug: new FormControl<string>('', Validators.required),
    tags: new FormControl<string[]>([]),
    updated: new FormControl<number | null>(null),
    wikiUrl: new FormControl<string>('', Validators.required),
    workPieces: new FormControl<string>(''),
    workPiecesTitle: new FormControl<string>(''),
  });

  ngOnInit(): void {
    this.handleId();
    UtilsService.scrollTop();
  }

  handleId(): void {
    const chunks = this.router.url.split('/');
    this.id = chunks[4];
    this.buttonLabel = this.id ? 'Ажурирај' : 'Додај';
    this.handleEpochs();
  }

  handleTags(): void {
    this.tagsService.getItemsSorted('name')
      .pipe(first())
      .subscribe({
        next: (response: any) => {
          this.allTags = [...response];
          this.cdr.detectChanges();
          if (this.id) {
            this.getItem();
          } else {
            this.setListeners();
          }
        },
      });
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name')
      .pipe(first())
      .subscribe({
        next: (response: any) => {
          this.categories = [...response];
          this.cdr.detectChanges();
          this.handleTags();
        },
      });
  }

  handleEpochs(): void {
    this.epochsService.getItemsSorted('order')
      .pipe(first())
      .subscribe({
        next: (response: any) => {
          this.allEpochs = [...response];
          this.cdr.detectChanges();
          this.handleCategories();
        },
      });
  }

  getItem(): void {
    this.service.getItem(this.id)
      .pipe(first())
      .subscribe({
        next: (response: Person) => {
          this.form.patchValue(response);
          this.updateActiveEpochList(this.categoryControl.value, response.epoch);
          this.setListeners();
        },
      });
  }

  setListeners() {
    this.categoryControl.valueChanges
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (newValue: string) => {
          this.updateActiveEpochList(newValue, null);
        },
      });
  }

  updateActiveEpochList(categoryId: string, epoch: string | null) {
    const selectedCategory = this.categories.find((c) => c.id === categoryId);
    this.epochs = this.allEpochs.filter((e) =>
      selectedCategory?.epochs.includes(e.id)
    );
    this.epochControl.setValue(epoch);
    this.cdr;
  }

  onSave(): void {
    this.id ? this.updateItem() : this.createItem();
  }

  onDelete(): void {
    this.service.deleteItem(this.id).then((_) => {
      this.location.back();
    });
  }

  updateItem(): void {
    const payload: Person = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.updateItem(this.id, payload)
      .then((_) => this.location.back());
  }

  createItem(): void {
    const payload: Person = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.service.createItem(payload).then((response) => {
      this.cdr.detectChanges();
      this.openImageDialog(payload, response);
    });
  }

  onImage(): void {
    const payload: Person = this.form.getRawValue();
    if (!payload.created) {
      payload.created = Date.now();
    }
    payload.updated = Date.now();
    this.openImageDialog(payload, { id: this.id }, true);
  }

  openImageDialog(item: Person, response: any, edited = false): void {
    const { id } = response;

    if (!id) {
      return;
    }

    const service = this.service;
    const defaultFormat = 'portrait';
    const dialogRef = this.matDialog.open(ImageDialogComponent, {
      width: '60rem',
      data: { item, service, id, defaultFormat },
      scrollStrategy: new NoopScrollStrategy(),
    });

    dialogRef.afterClosed().subscribe({
      next: (confirm: boolean) => {
        if (confirm && !edited) {
          this.location.back();
        }
      },
    });
  }
}
