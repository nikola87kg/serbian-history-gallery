import { Observable } from 'rxjs';

import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { AdminStateService } from '@app/services/admin-state.service';
import { NatureService } from '@app/services/nature.service';

@Component({
    selector: 'app-nature-list',
    imports: [TableComponent, ReactiveFormsModule, MatButtonModule, MatInputModule, MatFormFieldModule],
    templateUrl: './nature-list.component.html',
    styleUrls: ['./nature-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NatureListComponent {
  private router = inject(Router);
  private service = inject(NatureService);
  private cdr = inject(ChangeDetectorRef);
  private destroyRef = inject(DestroyRef);
  private adminStateService = inject(AdminStateService);

  public nature$!: Observable<any>;
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];
  public list: any[] = [];
  public items: any[] = [];
  public searchControl = new FormControl('');

  ngOnInit(): void {
    this.getData();

    setTimeout(() => {
      this.listenSearch();
    }, 1000);
  }

  getData() {
    this.nature$ = this.service.getItemsSorted('updated', 'desc');

    this.nature$.pipe(
      takeUntilDestroyed(this.destroyRef),
    ).subscribe({
      next: (response) => {
        this.list = response;
        this.items = response;
        this.listenSearch();
      }
    });

    this.columns = [
      { def: 'imagePath', title: 'Слика', type: ColumnType.Image },
      { def: 'name', title: 'Назив', type: ColumnType.Text },
      { def: 'place', title: 'Локација', type: ColumnType.Text },
      { def: 'type', title: 'Тип', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },
    ];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/nature-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/nature-form/${e.id}`);
  }

  onDeleteButton(id: any) {
    this.service.deleteItem(id);
  }

  onShortcut() {
    this.router.navigateByUrl(`srpsko-nasledje/prirodna-dobra`);
  }
  setFromStorage() {
    const storedValue = this.adminStateService.getSearchMemorial();
    this.searchControl.setValue(storedValue);
  }

  listenSearch() {
    setTimeout(() => {
      this.setFromStorage();
    }, 5);
    this.searchControl.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe({
        next: text => {
          this.adminStateService.setSearchMemorial(text || '');
          const textString = text?.toLowerCase() || '';
          this.items = [];
          this.cdr.detectChanges();

          this.items = this.list.filter(p => {
            const name = p.name?.toLowerCase() || '';
            const startsWithString = name.includes(textString);

            return startsWithString;
          });

          this.cdr.detectChanges();
        }
      });
  }

}
