import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { NatureListComponent } from './nature-list.component';

describe('NatureListComponent', () => {
  let component: NatureListComponent;
  let fixture: ComponentFixture<NatureListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NatureListComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(NatureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
