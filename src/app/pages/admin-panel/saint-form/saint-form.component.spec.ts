import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SaintFormComponent } from './saint-form.component';

describe('SaintFormComponent', () => {
  let component: SaintFormComponent;
  let fixture: ComponentFixture<SaintFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SaintFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(SaintFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
