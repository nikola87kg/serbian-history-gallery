import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalEventFormComponent } from './historical-event-form.component';

describe('HistoricalEventFormComponent', () => {
  let component: HistoricalEventFormComponent;
  let fixture: ComponentFixture<HistoricalEventFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HistoricalEventFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(HistoricalEventFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
