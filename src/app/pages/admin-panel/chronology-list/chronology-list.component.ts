import { Observable } from 'rxjs';


import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { Router } from '@angular/router';
import { ColumnType, TableColumn, TableComponent } from '@app/components/table/table.component';
import { AdminStateService } from '@services/admin-state.service';
import { Chronology, ChronologyService } from '@services/chronology.service';

@Component({
    selector: 'app-chronology-list',
    imports: [MatButtonModule, TableComponent, ReactiveFormsModule, MatInputModule, MatFormFieldModule, MatIconModule],
    templateUrl: './chronology-list.component.html',
    styleUrls: ['./chronology-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChronologyListComponent {
  private router = inject(Router);
  private service = inject(ChronologyService);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private adminStateService = inject(AdminStateService);

  public chronologies$!: Observable<Chronology[]>;
  public list: Chronology[] = [];
  public items: Chronology[] = [];
  public searchControl = new FormControl('');
  public columns: TableColumn[] = [];
  public displayedColumns: string[] = [];

  ngOnInit(): void {
    this.getData();
  }

  setFromStorage() {
    const storageValue = this.adminStateService.getSearchChronology();
    this.searchControl.setValue(storageValue);
  }

  listenSearch() {
    setTimeout(() => {
      this.setFromStorage();
    }, 5);
    this.searchControl.valueChanges
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe({
        next: text => {
          this.adminStateService.setSearchChronology(text || '');
          const textString = text?.toLowerCase() || '';
          this.items = [];
          this.cdr.detectChanges();

          this.items = this.list.filter(p => {
            const name = p.innerHtml?.toLowerCase() || '';
            const startsWithString = name.includes(textString);

            return startsWithString;
          });

          this.cdr.detectChanges();
        }
      });
  }

  getData() {
    this.chronologies$ = this.service.getItemsSorted('updated', 'desc');
    this.chronologies$.pipe(
      takeUntilDestroyed(this.destroyRef),
    ).subscribe({
      next: (response) => {
        this.list = response;
        this.items = response;
        this.listenSearch();
      }
    });

    this.columns = [
      { def: 'age', title: 'Година', type: ColumnType.Number },
      { def: 'title', title: 'Наслов', type: ColumnType.Text },
      { def: 'typeId', title: 'Тип', type: ColumnType.Text },
      { def: 'actions', title: 'Акције', type: ColumnType.Actions },];
    this.displayedColumns = this.columns.map(c => c.def);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/chronology-form`);
  }

  onEditButton(e: any) {
    this.router.navigateByUrl(`admin/panel/chronology-form/${e.id}`);
  }

  onDeleteButton(id: string) {
    this.service.deleteItem(id);
  }

  onShortcut() {
    this.router.navigateByUrl(`vremeplov/slavne-bitke`);
  }

}
