import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ChronologyListComponent } from './chronology-list.component';

describe('ChronologyListComponent', () => {
  let component: ChronologyListComponent;
  let fixture: ComponentFixture<ChronologyListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ChronologyListComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(ChronologyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
