import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { Router, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-admin-panel',
  imports: [RouterOutlet, MatButtonModule],
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AdminPanelComponent {
  private router = inject(Router);

  onCategory() {
    this.router.navigateByUrl('admin/panel/category-list');
  }

  onEpoch() {
    this.router.navigateByUrl('admin/panel/epoch-list');
  }

  onHistoricalEvent() {
    this.router.navigateByUrl('admin/panel/event-list');
  }

  onInstagramEvent() {
    this.router.navigateByUrl('admin/panel/instagram-list');
  }

  onNoteEvent() {
    this.router.navigateByUrl('admin/panel/note-list');
  }

  onPerson() {
    this.router.navigateByUrl('admin/panel/person-list');
  }

  onTag() {
    this.router.navigateByUrl('admin/panel/tag-list');
  }

  onQuote() {
    this.router.navigateByUrl('admin/panel/quote-list');
  }

  onSong() {
    this.router.navigateByUrl('admin/panel/song-list');
  }

  onBattle() {
    this.router.navigateByUrl('admin/panel/battle-list');
  }

  onChurch() {
    this.router.navigateByUrl('admin/panel/church-list');
  }

  onArt() {
    this.router.navigateByUrl('admin/panel/art-list');
  }

  onMemorial() {
    this.router.navigateByUrl('admin/panel/memorial-list');
  }

  onManifestation() {
    this.router.navigateByUrl('admin/panel/manifestation-list');
  }

  onNature() {
    this.router.navigateByUrl('admin/panel/nature-list');
  }

  onChronology() {
    this.router.navigateByUrl('admin/panel/chronology-list');
  }

  onMessage() {
    this.router.navigateByUrl('admin/panel/message-list');
  }

  onSaint() {
    this.router.navigateByUrl('admin/panel/saint-list');
  }

  onSymbol() {
    this.router.navigateByUrl('admin/panel/symbol-list');
  }

  onUser() {
    this.router.navigateByUrl('admin/panel/user-list');
  }
}
