import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { MemorialFormComponent } from './memorial-form.component';

describe('MemorialFormComponent', () => {
  let component: MemorialFormComponent;
  let fixture: ComponentFixture<MemorialFormComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MemorialFormComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(MemorialFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
