import { CommonModule, ViewportScroller } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@app/services/auth.service';
import { Epoch, EpochsService } from '@app/services/epochs.service';
import { Person, PersonsService } from '@app/services/persons.service';
import { Song, SongsService } from '@app/services/songs.service';
import { UtilsService } from '@app/services/utils.service';
import { IdToNamePipe } from "../../pipes/id-to-name.pipe";

@Component({
  selector: 'app-song-page',
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    TranslitPipe,
    IdToNamePipe
  ],
  templateUrl: './song-page.component.html',
  styleUrl: './song-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SongPageComponent {
  private songsService = inject(SongsService);
  private router = inject(Router);
  private scroller = inject(ViewportScroller);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private activatedRoute = inject(ActivatedRoute);
  private personService = inject(PersonsService);
  private epochsService = inject(EpochsService);

  public authService = inject(AuthService);
  public totalList: Song[] = [];
  public filteredList: Song[] = [];
  public songGroups: any[] = [];
  public selectedCategories: string[] = [];
  public song!: Song;
  public otherSongs: Song[] = [];
  public author!: Person;
  public epochs: Epoch[] = [];


  ngOnInit() {
    this.scroller.setOffset([0, 100]);
    this.getSong();
    UtilsService.scrollTop();
    this.listenRouteChanges();
    this.getEpochs();
  }

  getEpochs() {
    this.epochsService.getItems().pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((response) => {
        this.epochs = response;
      });
  }

  listenRouteChanges() {
    this.router.events
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.getSong();
        }
      });
  }

  getSong() {
    const slug = this.activatedRoute.snapshot?.paramMap?.get('slug');

    this.songsService.getItemBySlug(slug!)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Song) => {
          this.song = response;
          this.cdr.detectChanges();
          this.getOtherSongs();
          this.getAuthorData();
        }
      })
  }

  getOtherSongs() {
    this.songsService.getItemsQuery('authorSlug', '==', this.song.authorSlug)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Song[]) => {
          this.otherSongs = response.filter(s => s.slug !== this.song.slug);
          this.cdr.detectChanges();
        }
      })
  }

  getAuthorData() {
    this.personService.getItemBySlug(this.song.authorSlug!)
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (response: Person) => {
          this.author = response;
          this.cdr.detectChanges();
        }
      })
  }

  onAuthor() {
    this.router.navigateByUrl('biografija/' + this.song.authorSlug!);
  }

  onOtherSong(song: Song) {
    this.router.navigateByUrl('stihovi/' + song.slug!);
  }

  onPoetry() {
    this.router.navigateByUrl('stihovi');
  }

  onEdit() {
    this.router.navigateByUrl(`admin/panel/song-form/${this.song.id}`);
  }

  onAddSong() {
    localStorage.setItem('last-author', this.song.authorName!);
    this.router.navigateByUrl(`admin/panel/song-form`);
  }

}
