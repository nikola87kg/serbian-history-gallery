import { Location, ViewportScroller } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { MultiselectSheetComponent } from '@app/dialogs/multiselect-sheet/multiselect-sheet.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@app/services/auth.service';
import { PersonsService } from '@app/services/persons.service';
import { Song, SongsService, songTypes } from '@app/services/songs.service';
import { UtilsService } from '@app/services/utils.service';
import { TrimTextPipe } from "../../pipes/trim-text.pipe";

@Component({
  selector: 'app-songs-page',
  imports: [
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    TranslitPipe,
    TrimTextPipe
  ],
  templateUrl: './songs-page.component.html',
  styleUrl: './songs-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SongsPageComponent {
  private songsService = inject(SongsService);
  private router = inject(Router);
  private scroller = inject(ViewportScroller);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  private isAdmin = false;
  private matBottomSheet = inject(MatBottomSheet);
  private location = inject(Location);

  public authService = inject(AuthService);
  public personService = inject(PersonsService);
  public totalList: Song[] = [];
  public filteredList: Song[] = [];
  public songGroups: any[] = [];
  public selectedCategories: string[] = [];


  ngOnInit() {
    this.setFilter();
    this.scroller.setOffset([0, 100]);
    this.handleSongs();
    UtilsService.scrollTop();
    this.authService.loginAdmin$.subscribe({
      next: (isAdmin) => this.isAdmin = isAdmin,
    });
  }

  setFilter() {
    const filterJson = localStorage.getItem('poetry-filter');
    if (filterJson) {
      const filters = JSON.parse(filterJson)
      this.selectedCategories = filters;
    }
  }

  goToPerson(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onEdit(id: string | null | undefined, event: any) {
    event.stopPropagation();
    if (!id || !this.isAdmin) {
      return;
    }

    this.router.navigateByUrl(`admin/panel/song-form/${id}`);
  }

  onAuthor(slug: string | null): void {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  handleSongs(): void {
    this.songsService.getItems()
      .subscribe({
        next: (songResponse: any) => {
          this.totalList = [...songResponse];

          if (!this.selectedCategories.length) {
            this.selectedCategories = songTypes.map(t => t.id);
          }

          this.filteredList = this.totalList
            .filter(e => this.selectedCategories.includes(e.type!));;

          this.groupByAuthor();
        },
      });
  }

  groupByAuthor() {
    this.songGroups = [];
    this.cdr.detectChanges();
    this.filteredList.sort((a, b) =>
      a.title!.localeCompare(b.title!));
    const songGroups = UtilsService.groupBy(this.filteredList, 'authorSlug');
    songGroups.sort((a: any, b: any) =>
      a[0].authorName.localeCompare(b[0].authorName));

    setTimeout(() => {
      this.songGroups = [...songGroups];
      this.cdr.detectChanges();
    }, 1);
  }

  scrollTo(elementId: string) {
    this.scroller.scrollToAnchor(elementId);
  }

  onSongTitle(song: Song) {
    this.router.navigateByUrl(`stihovi/${song.slug}`);
  }

  onNewSong() {
    this.router.navigateByUrl('admin/panel/song-form');
  }

  onFilterButton() {
    const data = {
      title: 'Категоријe',
      items: songTypes,
      selectedItems: this.selectedCategories
    };

    const disableClose = false;
    const sheet = MultiselectSheetComponent;
    const panelClass = 'multiselect-sheet';
    const href = this.router.url;
    this.location.go(href + '#');

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, disableClose, panelClass
    });

    sheetRef.afterDismissed()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe((selectedItems: any = this.selectedCategories) => {
        this.selectedCategories = [...selectedItems];

        const noFilter = !selectedItems || !selectedItems.length;

        this.filteredList = noFilter
          ? [...this.totalList]
          : this.totalList.filter(e => selectedItems.includes(e.type!));

        localStorage.setItem('poetry-filter', JSON.stringify(selectedItems));
        this.groupByAuthor();
      });
  }
}
