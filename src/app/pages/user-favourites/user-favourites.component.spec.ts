import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { UserFavouritesComponent } from './user-favourites.component';

describe('UserFavouritesComponent', () => {
  let component: UserFavouritesComponent;
  let fixture: ComponentFixture<UserFavouritesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [UserFavouritesComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(UserFavouritesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
