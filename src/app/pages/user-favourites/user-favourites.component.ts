

import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatButtonModule } from '@angular/material/button';
import { PersonCardComponent } from '@app/components/person-card/person-card.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@services/auth.service';
import { Person, PersonsService } from '@services/persons.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-user-favourites',
  imports: [CommonModule, MatButtonModule, PersonCardComponent, TranslitPipe],
  templateUrl: './user-favourites.component.html',
  styleUrls: ['./user-favourites.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserFavouritesComponent {
  private personService = inject(PersonsService);
  private destroyRef = inject(DestroyRef);

  public authService = inject(AuthService);
  public cards: Person[] = [];

  ngOnInit() {
    UtilsService.scrollTop();
    this.handleFavourites();
  }

  handleFavourites(): void {
    this.authService.userProfile$
      ?.pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe(user => {
        if (user?.favourites?.length) {
          if (this.cards.length) {
            this.removeMissingCard(user.favourites);
            return;
          }
          this.getCards(user.favourites);
        } else {
          this.cards = [];
        }
      });
  }

  removeMissingCard(favourites: string[]) {
    const slugs: string[] = this.cards.map(c => c.slug!);
    const missingCard = slugs.find(s => !favourites.includes(s));
    this.cards = this.cards.filter(c => c.slug !== missingCard);
  }

  getCards(favourites: string[]) {
    this.personService.getItemsQuery('slug', 'in', favourites)
      .subscribe({
        next: (cards: Person[]) => {
          cards.sort((a, b) => Number(a.birthYear) - Number(b.birthYear));
          this.cards = [...cards];
        },
      });
  }

}
