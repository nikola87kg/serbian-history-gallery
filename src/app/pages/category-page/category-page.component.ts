

import { Observable, of, switchMap, tap } from 'rxjs';

import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, inject
} from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonCardComponent } from '@app/components/person-card/person-card.component';
import {
  PersonMiniCardComponent
} from '@app/components/person-mini-card/person-mini-card.component';
import {
  PrintscreenTagPeopleDialogComponent
} from '@app/dialogs/printscreen-tag-people-dialog/printscreen-tag-people-dialog.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { SlugToIdPipe } from '@pipes/slug-to-id.pipe';
import { AuthService } from '@services/auth.service';
import { CategoriesService, Category } from '@services/categories.service';
import { Epoch, EpochsService } from '@services/epochs.service';
import { Person, PersonsService } from '@services/persons.service';
import { UtilsService } from '@services/utils.service';

enum View { epochs = 'epochs', top10 = 'top10', photos = 'photos' }

@Component({
  selector: 'app-category-page',
  imports: [
    CommonModule,
    FormsModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatProgressSpinnerModule,
    PersonCardComponent,
    PersonMiniCardComponent,
    TranslitPipe,
  ],
  templateUrl: './category-page.component.html',
  styleUrls: ['./category-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryPageComponent {
  private personService = inject(PersonsService);
  private categoriesService = inject(CategoriesService);
  private epochsService = inject(EpochsService);
  private activatedRoute = inject(ActivatedRoute);
  private matDialog = inject(MatDialog);
  private slugtoIdPipe = inject(SlugToIdPipe);
  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);

  public View = View;
  public selectedIndex!: number;
  public preferredView: View = View.epochs;
  public authService = inject(AuthService);
  public category: Category | undefined;
  public personList: Person[] = [];
  public starList: Person[] = [];
  public allEpochs: Epoch[] = [];
  public categoryEpochs: Epoch[] = [];
  private queryFragment = '';

  ngAfterViewInit() {
    this.setpreferredView();
    this.getPersonsForCategory();
  }

  setpreferredView() {
    const storagedView = localStorage.getItem('preferredCategoryView');
    if (storagedView) {
      this.preferredView = storagedView as View;
      this.selectedIndex = this.preferredView === View.epochs ? 0
        : this.preferredView === View.top10 ? 1 : 2;
    }
  }

  onTabChange(tabIndex: number | null) {
    const view = tabIndex === 0
      ? View.epochs
      : tabIndex === 1 ? View.top10
        : View.photos;
    this.preferredView = view;
    localStorage.setItem('preferredCategoryView', view);
    if (tabIndex === this.selectedIndex) {
      return;
    }

    this.selectedIndex = tabIndex ?? 0;
  }

  setScroll() {
    if (this.queryFragment) {
      const element = document.getElementById(this.queryFragment);
      if (element) {
        element.scrollIntoView({ behavior: 'smooth', block: 'start' });
      }
    } else {
      UtilsService.scrollTop();
    }
    this.cdr.detectChanges();
  }

  goToPerson(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onEdit(id: string | null | undefined, event: any) {
    event.stopPropagation();
    if (!id) {
      return;
    }

    this.router.navigateByUrl(`admin/panel/person-form/${id}`);
  }

  onAddButton() {
    this.router.navigateByUrl(`admin/panel/person-form`);
  }

  getCategoryId(categoryList: Category[]): Observable<string> {
    const slug = this.activatedRoute.snapshot?.paramMap?.get('slug');

    this.queryFragment = this.activatedRoute?.snapshot?.fragment || '';
    const categories = [...categoryList];
    const category = categories.find((c) => c.slug === slug);

    this.category = category as Category;
    this.categoryEpochs = this.allEpochs.filter(e => category?.epochs?.includes(e.id!));

    this.categoryEpochs
      .sort((a, b) => (a.order as number) - (b.order as number));

    const id = this.slugtoIdPipe
      .transform(category?.slug!, categories);

    return of(id);
  }

  getPersonsForCategory(): void {
    this.epochsService.getItemsSorted('order')
      .pipe(
        takeUntilDestroyed(this.destroyRef),
        tap(epochList => this.allEpochs = epochList),
        switchMap(_ => this.categoriesService.getItemsSorted('name')),
        switchMap(categoryList => this.getCategoryId(categoryList)),
        switchMap(id => this.personService.getItemsQuery('category', '==', id))
      )
      .subscribe({
        next: (personList: Person[]) => {
          const list = [...personList];
          list.sort((a: Person, b: Person) => {

            if (a.birthYear === b.birthYear
              && a.birthMonth && a.birthDay
              && b.birthMonth && b.birthDay
              && a.birthMonth === b.birthMonth) {
              const diff = (a.birthDay) - (b.birthDay);
              return diff;
            }

            if (a.birthYear === b.birthYear && a.birthMonth && b.birthMonth) {
              const diff = (a.birthMonth) - (b.birthMonth);
              return diff;
            }

            if (a.birthYear && b.birthYear) {
              const diff = (a.birthYear) - (b.birthYear);
              return diff;
            }

            return 1;
          });
          this.personList = list;
          this.starList = list.filter(i => i.topTen || i.localHero);
          setTimeout(() => {
            this.setScroll();
          }, 100);
        },
      });
  }

  onPrintscreenTags(grid = false) {
    const tag = { name: this.category?.name };
    const items = this.personList;

    this.matDialog.open(PrintscreenTagPeopleDialogComponent, {
      data: { items, tag, grid },
      panelClass: 'social-dialog-container',
      scrollStrategy: new NoopScrollStrategy(),
    });

  }
}
