import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurnamePageComponent } from './surname-page.component';

describe('SurnamePageComponent', () => {
  let component: SurnamePageComponent;
  let fixture: ComponentFixture<SurnamePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SurnamePageComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SurnamePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
