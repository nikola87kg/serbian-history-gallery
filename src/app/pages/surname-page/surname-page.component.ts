import { ViewportScroller } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, DestroyRef, ElementRef, inject, ViewChild } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { ActivatedRoute, Router } from '@angular/router';
import { Person, PersonsService } from '@app/services/persons.service';
import { UtilsService } from '@app/services/utils.service';
import { PersonCardComponent } from "../../components/person-card/person-card.component";
import { TranslitPipe } from "../../pipes/translit.pipe";

enum Segment {
  prezime = 'prezime',
  ime = 'ime',
}

@Component({
  selector: 'app-surname-page',
  imports: [
    FormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatIconModule,
    PersonCardComponent,
    TranslitPipe
  ],
  templateUrl: './surname-page.component.html',
  styleUrl: './surname-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SurnamePageComponent {
  private scroller = inject(ViewportScroller);
  private activatedRoute = inject(ActivatedRoute);
  private router = inject(Router);
  private slug: any;
  private personsService = inject(PersonsService);
  public dropdownList: any[] = [];
  public allPeople: Person[] = [];
  public filteredList: Person[] = [];
  private destroyRef = inject(DestroyRef);
  private cdr = inject(ChangeDetectorRef);
  public Segment = Segment;
  public activeSegment = Segment.prezime;

  @ViewChild('autoCompleteInput') autoCompleteInput!: ElementRef<HTMLInputElement>;
  inputControl = new FormControl('');
  dropdownOptions!: any[];

  ngOnInit() {
    UtilsService.scrollTop();
    this.scroller.setOffset([0, 100]);
    this.handleSlug();
  }

  displayOption(option: any): string {
    return option ? option.title : '';
  }

  onOptionSelected(event: MatAutocompleteSelectedEvent) {
    const selectedOption = event.option.value;
    this.filteredList = [...selectedOption.elements];

    this.filteredList.sort(
      (a: Person, b: Person) => {
        return Number(a.birthYear) - Number(b.birthYear);
      }
    );

    this.router.navigateByUrl(`${this.activeSegment}/${selectedOption.slug}`);

    setTimeout(() => {
      this.autoCompleteInput?.nativeElement.blur();
      this.cdr.detectChanges();
    }, 1);
  }

  filterList(): void {
    const textSlugify: string = UtilsService.slugify(this.autoCompleteInput?.nativeElement?.value);
    const filterValue = textSlugify.toLowerCase().trim();

    this.cdr.detectChanges();
    this.dropdownOptions = this.dropdownList
      .filter(e => {
        const titleSlugify = UtilsService.slugify(e.title);
        const nameMatches = titleSlugify.toLowerCase()
          .includes(filterValue);

        return nameMatches;
      }).slice(0, 200);

    this.cdr.detectChanges();
  }

  handleSlug() {
    const url = this.router.url.split('/')[1];
    this.activeSegment = url === 'prezime' ? Segment.prezime : Segment.ime;
    this.slug = this.activatedRoute.snapshot?.paramMap?.get('title');
    switch (this.activeSegment) {
      case Segment.prezime: this.getList(true); break;
      case Segment.ime: this.getList(false); break;
    }
    this.cdr.detectChanges();
  }

  clearSearch() {
    this.inputControl.reset();
    this.cdr.detectChanges();
  }

  getList(searchLastName = true) {
    const prop = searchLastName ? 'lastName' : 'firstName';

    this.personsService.getItems()
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe({
        next: (persons) => {
          const completeList = UtilsService
            .groupAndSortBy(persons, prop);
          this.dropdownList = [...completeList];
          this.dropdownList.forEach(item => {
            item.slug = UtilsService.slugify(item.title);
          });
          this.cdr.detectChanges();

          if (this.slug) {
            const name = this.dropdownList.find(s => s.slug === this.slug);
            this.inputControl.setValue(name);
            setTimeout(() => {
              this.filterList();
              this.filteredList = [...(name?.elements || [])];

              this.filteredList.sort(
                (a: Person, b: Person) => {
                  return Number(a.birthYear) - Number(b.birthYear);
                }
              );
              this.cdr.detectChanges();
            }, 1);
          }
          this.cdr.detectChanges();
        }
      });
  }

  setSegment(segment: Segment) {
    this.activeSegment = segment;
    const seg = segment === Segment.prezime ? 'prezime' : 'ime';
    this.router.navigateByUrl(`/${seg}`);
    switch (segment) {
      case Segment.prezime: this.getList(true); break;
      case Segment.ime: this.getList(false); break;
    }
  }

}
