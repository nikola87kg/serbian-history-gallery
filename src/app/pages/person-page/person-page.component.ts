import { GoogleAnalyticsService } from 'ngx-google-analytics';
import { of, zip } from 'rxjs';

import { Clipboard } from '@angular/cdk/clipboard';
import { NoopScrollStrategy } from '@angular/cdk/overlay';
import { CommonModule, Location } from '@angular/common';
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, computed, DestroyRef, effect, inject,
  signal
} from '@angular/core';
import { takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import {
  PersonMiniCardComponent
} from '@app/components/person-mini-card/person-mini-card.component';
import { TagItemComponent } from '@app/components/tag-item/tag-item.component';
import {
  PrintscreenPersonBiographyDialogComponent
} from '@app/dialogs/printscreen-person-biography-dialog/printscreen-person-biography-dialog.component';
import {
  PrintscreenPersonPostDialogComponent
} from '@app/dialogs/printscreen-person-post-dialog/printscreen-person-post-dialog.component';
import {
  PrintscreenPersonStoryDialogComponent
} from '@app/dialogs/printscreen-person-story-dialog/printscreen-person-story-dialog.component';
import { SplitBirthdayPipe } from '@app/pipes/split-birthday.pipe';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { InstagramService } from '@app/services/instagram.service';
import { Song, SongsService } from '@app/services/songs.service';
import { DefaultLogoDirective } from '@directives/default-logo.directive';
import { IdToNamePipe } from '@pipes/id-to-name.pipe';
import { IdToSlugPipe } from '@pipes/id-to-slug.pipe';
import { YearsOldPipe } from '@pipes/years-old.pipe';
import { AuthService } from '@services/auth.service';
import { CategoriesService, Category } from '@services/categories.service';
import { Epoch, EpochsService } from '@services/epochs.service';
import { GlossaryService } from '@services/glossary.service';
import { Person, PersonsService } from '@services/persons.service';
import { Quote, QuotesService } from '@services/quotes.service';
import { Tag, TagsService } from '@services/tags.service';
import { ThemeService } from '@services/theme.service';
import { UserProfile, UserProfileService } from '@services/user-profile.service';
import { UtilsService } from '@services/utils.service';
import { HorizontalScrollComponent } from "../../components/horizontal-scroll/horizontal-scroll.component";
import { MiniBoxComponent } from "../../components/mini-box/mini-box.component";
import { MiniTextComponent } from "../../components/mini-text/mini-text.component";
import { BirthMonthDatePipe } from "../../pipes/birth-month-date.pipe";
import { MiniBoxParserPipe } from "../../pipes/mini-box-parser.pipe";

@Component({
  selector: 'app-person-page',
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    DefaultLogoDirective,
    IdToNamePipe,
    PersonMiniCardComponent,
    YearsOldPipe,
    TagItemComponent,
    MatDialogModule,
    TranslitPipe,
    SplitBirthdayPipe,
    HorizontalScrollComponent,
    MiniBoxComponent,
    MiniBoxParserPipe,
    MiniTextComponent,
    BirthMonthDatePipe
  ],
  templateUrl: './person-page.component.html',
  styleUrls: ['./person-page.component.scss'],
  providers: [MatSnackBar],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonPageComponent {
  private service = inject(PersonsService);
  private categoriesService = inject(CategoriesService);
  private userProfileService = inject(UserProfileService);
  private instagramService = inject(InstagramService);
  private epochsService = inject(EpochsService);
  private tagsService = inject(TagsService);
  private router = inject(Router);
  private activatedRoute = inject(ActivatedRoute);
  private clipboard = inject(Clipboard);
  private idToSlugPipe = inject(IdToSlugPipe);
  private destroyRef = inject(DestroyRef);
  private quotesService = inject(QuotesService);
  private songsService = inject(SongsService);
  private matDialog = inject(MatDialog);
  private cdr = inject(ChangeDetectorRef);
  private userProfile!: UserProfile | null;
  private googleAnalyticsService = inject(GoogleAnalyticsService);
  private location = inject(Location);
  private routeEvents = toSignal(this.router.events, { rejectErrors: true });
  private isAdmin = false;
  private allInstagramPosts = toSignal(this.instagramService.getItems(), { rejectErrors: true });

  public themeService = inject(ThemeService);
  public authService = inject(AuthService);

  public get imagePrefix(): string {
    return this.themeService.isDarkMode()
      ? `radial-gradient(circle, rgb(0 0 0 / 85%) 0%, black 95%), linear-gradient(#353535, #222222), `
      : `radial-gradient(circle, rgb(255 255 255 / 80%) 0%, white 95%), linear-gradient(#c8c8c8, #c8c8c8), `;
  };

  public horoscopeSign = signal<string>('');
  public Glossary = GlossaryService.Glossary;
  public person = signal<Person | null>(null);
  public epochs = signal<Epoch[]>([]);
  public categories = signal<Category[]>([]);
  public relatedPersons = signal<Person[]>([]);
  public sameBirthdayList = signal<Person[]>([]);
  public sameBirthYearList = signal<Person[]>([]);
  public sameLastNameList = signal<Person[]>([]);
  public sameFirstNameList = signal<Person[]>([]);
  public tags = signal<Tag[]>([]);
  public personTags = signal<Tag[]>([]);
  public personKeynotes = signal<any[]>([]);
  public quotes = signal<Quote[]>([]);
  public songs = signal<Song[]>([]);
  public favouritedByUser = signal<boolean | null>(null);

  public personSlug = computed(() => this.person()?.slug);

  public instagramPosts = computed(() => {
    const allPosts = this.allInstagramPosts();
    const instagramLinks: any[] =
      this.person()?.instagramPosts?.trim()
        ? (this.person()?.instagramPosts?.split('\n') || [])
        : [];

    const result: any = allPosts
      ?.filter(p => {
        return instagramLinks.some(link => {
          return p.embeddedCode?.includes(link);
        });
      }).map(p => {
        return {
          ...p,
          link: instagramLinks.find(link => p.embeddedCode?.includes(link))
        };
      });
    return result;
  });

  public personEpoch = computed(() => this.person()?.epoch);

  public externalLinks = computed(() => {
    const links: any[] = [];
    if (this.person()?.wikiUrl) {
      links.push({
        imagePath: 'assets/images/wikipedia.png',
        commonName: 'wikipedia.org',
        link: this.decodeUrl(this.person()?.wikiUrl),
        fullLink: this.person()?.wikiUrl,
      });
    }
    if (this.person()?.instagramUrl) {
      links.push({
        imagePath: 'assets/images/Instagram_icon.png',
        commonName: 'instagram.com',
        link: this.decodeUrl(this.person()?.instagramUrl),
        fullLink: this.person()?.instagramUrl,
      });
    }
    if (this.person()?.porekloUrl) {
      links.push({
        imagePath: 'assets/images/poreklo.png',
        commonName: 'poreklo.rs',
        link: this.decodeUrl(this.person()?.porekloUrl),
        fullLink: this.person()?.porekloUrl,
      });
    }
    if (this.person()?.biographyUrl) {
      links.push({
        imagePath: 'assets/images/biografija.png',
        commonName: 'biografija.org',
        link: this.decodeUrl(this.person()?.biographyUrl),
        fullLink: this.person()?.biographyUrl,
      });
    }

    return links;
  });

  public cardColor = computed(() => {
    const epoch = this.epochs()!.find((e) => e.id === this.personEpoch());
    return String(epoch?.color) || '#ffffff';
  });

  constructor() {
    this.registerSignalEffects();
  }

  ngOnInit(): void {
    this.clearSignals();
    this.handlePerson();
    this.authService.loginAdmin$.subscribe({
      next: (isAdmin) => this.isAdmin = isAdmin,
    });
  }

  decodeUrl(encodedUrl: any = ''): string {
    const https = 'https://';
    if (encodedUrl.includes(https)) {
      encodedUrl = encodedUrl.split(https)[1];
    }
    return decodeURIComponent(encodedUrl);
  }

  registerSignalEffects(): void {
    effect(() => {
      const routeEvent = this.routeEvents();

      if (routeEvent instanceof NavigationEnd) {
        this.handlePerson();
        setTimeout(() => {
          UtilsService.scrollTop();
        }, 300);
      }
    }, { allowSignalWrites: true });
  }

  addGoogleAnalytics(slug: string) {
    this.googleAnalyticsService.event(
      `Person Page Action - ${slug}`,
      'Person Page Init Category',
      `Person Page - ${slug}`,
      Date.now(),
    );
  }

  handlePerson(): void {
    const slug = this.activatedRoute.snapshot?.paramMap?.get('slug');

    if (slug) {
      this.addGoogleAnalytics(slug);

      this.service.getItemBySlug(slug)
        .pipe(takeUntilDestroyed(this.destroyRef))
        .subscribe({
          next: (response: Person) => {
            if (!response) {
              return;
            }
            this.person.set(response);
            this.handleKeynotes(response.keyNotes);

            const argsBirth = [];
            const day = response.birthDay;
            const month = response.birthMonth;
            if (day) {
              argsBirth.push('birthDay');
              argsBirth.push(+day);
            }
            if (month) {
              argsBirth.push('birthMonth');
              argsBirth.push(+month);
            }

            const argsYear = [];
            const year = response.birthYear;

            if (year) {
              argsYear.push('birthYear');
              argsYear.push(+year);
            }

            const firstNameArgs = [];
            firstNameArgs.push('firstName');
            firstNameArgs.push(response.firstName?.trim());

            const lastNameArgs = [];
            lastNameArgs.push('lastName');
            lastNameArgs.push(response.lastName?.trim());

            const categories$ = this.categoriesService.getItemsSorted('name');
            const epochs$ = this.epochsService.getItemsSorted('order');
            const tags$ = this.tagsService.getItemsSorted('slug');
            const related$ = this.service.getItemsQuery('category', '==', response.category);
            const quotes$ = this.quotesService.getItemsQuery('authorSlug', '==', response.slug);
            const songs$ = this.songsService.getItemsQuery('authorSlug', '==', response.slug);
            const sameBirthday$ = argsBirth.length === 0 ? of([]) : this.service.getItemsQueries(...argsBirth);
            const sameBirthYear$ = this.service.getItemsQueries(...argsYear);
            const sameSurname$ = !response.lastName ? of([]) : this.service.getItemsQueries(...lastNameArgs);
            const sameFirstName$ = !response.firstName ? of([]) : this.service.getItemsQueries(...firstNameArgs);
            const user$ = this.authService.userProfile$.pipe(takeUntilDestroyed(this.destroyRef));

            zip([categories$, epochs$, tags$, related$, quotes$, songs$, sameBirthday$, sameBirthYear$, sameSurname$, sameFirstName$, user$])
              .subscribe({
                next: (result) => {
                  const [categories, epochs, tags, related, quotes, songs, sameBirthdays, sameBirthYear, sameSurnames, sameFirstNames] = result;

                  this.categories.set(categories);
                  this.epochs.set(epochs);
                  this.handleTags(tags);
                  this.handleRelated(related);
                  this.handleQuotes(quotes);
                  this.handleSongs(songs);
                  this.handleSameBirthday(sameBirthdays);
                  this.handleSameBirthYear(sameBirthYear);
                  this.handleSameLastName(sameSurnames);
                  this.handleSameFirstName(sameFirstNames);
                  this.handleFavourites();
                  this.handleHoroscope();
                  this.cdr?.detectChanges();
                },
              });
          },
        });
    }
  }

  handleFavourites(): void {
    this.authService.userProfile$.pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(user => {
        if (user) {
          this.userProfile = user!;
          const slug = this.personSlug()!;
          const isFavourited = !!user?.favourites?.includes(slug);
          this.favouritedByUser.set(isFavourited);
        } else {
          this.userProfile = null;
          this.favouritedByUser.set(null);
        }
        this.cdr.detectChanges();
      });
  }

  handleHoroscope() {
    const person = this.person()!;
    const isReligion = person.category === "fIZbw2bSvsj32xgu0gqg";

    if (person.birthDay && person.birthMonth && !isReligion) {
      const sign = UtilsService.getHoroscopeSign(person.birthDay, person.birthMonth);
      this.horoscopeSign.set(sign);

    } else {
      this.horoscopeSign.set('');
    }
  }

  handleSameBirthday(response: Person[] = []) {
    const person = this.person()!;
    response = response.filter(p => p.slug !== person.slug);
    this.sortByBirthday(response);
    this.sameBirthdayList.set(response);
  }


  handleSameBirthYear(response: Person[] = []) {
    const person = this.person()!;
    response = response.filter(p => p.slug !== person.slug);
    this.sortByBirthday(response);
    this.sameBirthYearList.set(response);
  }

  handleSameFirstName(response: Person[] = []) {
    const person = this.person()!;
    response = response.filter(p => p.slug !== person.slug);
    this.sortByBirthday(response);
    this.sameFirstNameList.set(response);
  }

  handleSameLastName(response: Person[] = []) {
    const person = this.person()!;
    response = response.filter(p => p.slug !== person.slug);
    this.sortByBirthday(response);
    this.sameLastNameList.set(response);
  }

  sortByBirthday(persons: Person[] = []) {
    persons.sort((a: Person, b: Person) => {
      if (a.birthYear === b.birthYear && a.birthDay && b.birthDay
        && a.birthMonth === b.birthMonth) {
        return (a.birthDay as number) - (b.birthDay as number);
      }
      if (a.birthYear === b.birthYear && a.birthMonth && b.birthMonth) {
        return (a.birthMonth as number) - (b.birthMonth as number);
      }

      return (a.birthYear as number) - (b.birthYear as number);
    });
  }

  sortByFirstName(response: Person[] = []) {
    response.sort((a: Person, b: Person) => {
      return a.firstName!.localeCompare(b.firstName!);
    });
  }

  handleRelated(response: any[]) {
    const slug = this.activatedRoute.snapshot?.paramMap?.get('slug');
    const filteredResponse = response
      .filter((p: any) => p.slug !== slug
        && p.epoch === this.person()?.epoch) as Person[];
    UtilsService.shuffleListOrder(filteredResponse);

    this.relatedPersons.set([...filteredResponse.slice(0, 40)]);
  }

  handleTags(response: any[]): void {
    const person = this.person()!;
    this.tags.set(response);

    const personTags = person.tags?.map((t: string) => {
      const tag = this.tags().find(tag => tag.id === t);
      return tag!;
    }) || [];

    this.personTags.set(personTags);
  };

  handleQuotes(quoteResponse: Quote[] = []): void {
    quoteResponse.sort((a, b) => {
      if (a.important && !b.important) {
        return -1;
      } else if (b.important && !a.important) {
        return 1;
      }
      return a.slug!.localeCompare(b.slug!)
    });

    this.quotes.set(quoteResponse);
  };

  handleSongs(songResponse: Song[] = []): void {
    songResponse.sort((a, b) => {
      if (a.important && !b.important) {
        return -1;
      } else if (b.important && !a.important) {
        return 1;
      }
      return a.slug!.localeCompare(b.slug!)
    });

    this.songs.set(songResponse);
  };

  onCopy() {
    this.clipboard.copy(window.location.href);
    UtilsService.openSnackbar(this.Glossary.coppiedSuccessfully);
  }

  onEdit() {
    const person = this.person()!;

    const id = person.id;
    this.router.navigateByUrl(`admin/panel/person-form/${id}`);
  }

  goToCategoryPage() {
    const person = this.person()!;

    const category = person.category!;
    const categories = this.categories;

    const slug = this.idToSlugPipe.transform(category, categories());
    this.router.navigateByUrl(`kategorija/${slug}`);
  }

  onPerson(slug: string | null) {
    if (!slug) {
      return;
    }

    this.clearSignals();
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  clearSignals() {
    this.tags.set([]);
    this.personTags.set([]);
    this.favouritedByUser.set(null);
    this.quotes.set([]);
    this.sameBirthdayList.set([]);
    this.sameLastNameList.set([]);
    this.categories.set([]);
    this.relatedPersons.set([]);
    this.epochs.set([]);
    this.horoscopeSign.set('');
    this.person.set(null);
  }

  onLink(url: string | null) {
    if (!url) {
      return;
    }
    if (!url.includes('https')) {
      window.open('https://' + url, '_blank');
      return;
    }

    window.open(url, '_blank');
  }

  onPrintscreen(story: boolean) {
    const dialog: any = story
      ? PrintscreenPersonStoryDialogComponent
      : PrintscreenPersonPostDialogComponent;

    const panelClass = 'social-dialog-container';
    const item = this.person();
    const scrollStrategy = new NoopScrollStrategy();

    const href = this.router.url;
    this.location.go(href + '#');

    const options = { data: { item }, panelClass, scrollStrategy };
    const dialogRef = this.matDialog.open(dialog, options);

    dialogRef.afterClosed().subscribe((onX: boolean) => {
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  }

  onBiograpgyScreen() {
    const dialog: any = PrintscreenPersonBiographyDialogComponent;
    const panelClass = 'social-dialog-container';
    const item = this.person();
    const scrollStrategy = new NoopScrollStrategy();

    const href = this.router.url;
    this.location.go(href + '#');

    const options = { data: { item }, panelClass, scrollStrategy };
    const dialogRef = this.matDialog.open(dialog, options);

    dialogRef.afterClosed().subscribe((onX: boolean) => {
      if (onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }
    });
  }

  onFullStar() {
    const person = this.person()!;

    if (this.userProfile) {
      const slug = `${person.slug}`;
      this.userProfileService.removeFromFavourites(this.userProfile, slug);
      const message = `${person.commonName} више није у фаворитима.`;
      UtilsService.openSnackbar(message, false, true);
    }
  }

  onEmptyStar() {
    const person = this.person()!;

    if (this.userProfile) {
      const slug = `${person.slug}`;
      let [isError, message] = this.userProfileService
        .addToFavourites(this.userProfile, slug);

      if (!isError) {
        message = `${person.commonName} је сада у фаворитима.`;
      }

      UtilsService.openSnackbar(message, isError, true);
    }
  }

  onNullStar() {
    const message = `Морате бити улоговани да бисте додавали фаворите.`;
    UtilsService.openSnackbar(message, true, true);
    this.router.navigateByUrl('nalog');
  }

  onQuoteHeadline = () => {
    this.router.navigateByUrl('citati');
  }

  onSongHeadline = () => {
    this.router.navigateByUrl('stihovi');
  }

  onSong(slug: string | null) {
    this.router.navigateByUrl(`stihovi/${slug}`);
  }

  onBirth(isYear = false) {
    const { birthDay, birthMonth, birthYear } = this.person()!;
    if (isYear) {
      this.router.navigateByUrl(`kalendar/${birthYear}/rodjeni`);
    } else if (birthDay && birthMonth) {
      const path = UtilsService.getDaySlug(birthDay, birthMonth);
      this.router.navigateByUrl(`kalendar/${path}/rodjeni`);
    } else {
      const path = UtilsService.getTodaySlug();
      this.router.navigateByUrl(`kalendar/${path}/rodjeni`);
    }

  }

  onCopyQuote(quote: string | null) {
    if (this.isAdmin) {
      navigator.clipboard.writeText(quote!);

    }
  }
  onEditQuote(id: string | null | undefined) {
    if (this.isAdmin) {
      this.router.navigateByUrl(`admin/panel/quote-form/${id}`);
    }
  }
  onSameEpoch = () => {
    const person = this.person();

    const category = this.idToSlugPipe.transform(person!.category!, this.categories());
    const epoch = this.idToSlugPipe.transform(person!.epoch!, this.epochs());
    const url = `kategorija/${category}#${epoch}`;
    this.router.navigateByUrl(url);
  }
  onAddQuote() {
    localStorage.setItem('last-author', this.person()?.commonName!);
    this.router.navigateByUrl(`admin/panel/quote-form`);
  }

  onAddSong() {
    localStorage.setItem('last-author', this.person()?.commonName!);
    this.router.navigateByUrl(`admin/panel/song-form`);
  }

  onInstragram = () => {
    this.router.navigateByUrl(`instagram`);
  }

  onInstagramBox(post: string) {
    window.open(post!, '_blank');
  }

  onKeynotes = () => {
    this.router.navigateByUrl(`vremeplov/biografije-velikana`);
  }

  onSurname = () => {
    const person = this.person();
    const surname = person?.lastName?.toLowerCase();

    const latinSurname = UtilsService.slugify(surname!)
    this.router.navigateByUrl(`prezime/${latinSurname}`);
  }

  onName = () => {
    const person = this.person();
    const name = person?.firstName?.toLowerCase();

    const latinName = UtilsService.slugify(name!)
    this.router.navigateByUrl(`ime/${latinName}`);
  }

  onBirthdayHeadline = () => {
    this.onBirth(false);
  }

  onBirthYearHeadline = () => {
    this.onBirth(true);
  }

  handleKeynotes(keynotes: string | null): any {
    if (!keynotes) {
      this.personKeynotes.set([]);
      return;
    }

    const elements = keynotes.split('</p>').filter(e => e);

    const notes: any[] = [];
    elements.forEach((html: string) => {
      const age = this.extractStrongContent(html);
      const innerHtml = this.clearBeforeDash(html);
      notes.push({ age, innerHtml })
    });

    this.personKeynotes.set(notes);
  }

  extractStrongContent(html: string): string {
    // Create a temporary DOM element to parse the string
    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = html;

    // Query the <strong> tag and extract its content
    const strongElement = tempDiv.querySelector('strong');
    return strongElement?.textContent?.trim() || '';
  }

  clearBeforeDash(html: string): string {
    const index = html.indexOf('- ');
    return index !== -1 ? html.substring(index + 2) : html;
  }

}
