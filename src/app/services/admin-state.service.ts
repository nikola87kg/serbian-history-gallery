import { Injectable, signal } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class AdminStateService {

  private searchTagSignal = signal<string>('');
  private searchChurchSignal = signal<string>('');
  private searchPersonSignal = signal<string>('');
  private searchQuotesSignal = signal<string>('');
  private searchSongsSignal = signal<string>('');
  private searchChronologySignal = signal<string>('');
  private searchMemorialSignal = signal<string>('');
  private searchNatureSignal = signal<string>('');

  setSearchPerson(value: string) {
    this.searchPersonSignal.set(value.trim());
  }

  getSearchPerson(): string {
    return this.searchPersonSignal();
  }

  setSearchTag(value: string) {
    this.searchTagSignal.set(value.trim());
  }

  getSearchTag(): string {
    return this.searchTagSignal();
  }

  setSearchChurch(value: string) {
    this.searchChurchSignal.set(value.trim());
  }

  getSearchChurch(): string {
    return this.searchChurchSignal();
  }

  setSearchMemorial(value: string) {
    this.searchMemorialSignal.set(value.trim());
  }

  getSearchMemorial(): string {
    return this.searchMemorialSignal();
  }

  setSearchQuote(value: string) {
    this.searchQuotesSignal.set(value.trim());
  }

  getSearchQuote(): string {
    return this.searchQuotesSignal();
  }

  setSearchSong(value: string) {
    this.searchSongsSignal.set(value.trim());
  }

  getSearchSong(): string {
    return this.searchSongsSignal();
  }

  setSearchChronology(value: string) {
    this.searchChronologySignal.set(value.trim());
  }

  getSearchChronology(): string {
    return this.searchChronologySignal();
  }

  setSearchNature(value: string) {
    this.searchNatureSignal.set(value.trim());
  }

  getSearchNature(): string {
    return this.searchNatureSignal();
  }
}
