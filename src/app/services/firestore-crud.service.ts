import {
  addDoc, AggregateField, AggregateQuerySnapshot, collection, deleteDoc, doc, DocumentReference,
  getCountFromServer, orderBy, OrderByDirection, query, setDoc, updateDoc, where
} from 'firebase/firestore';
import { getDownloadURL, uploadBytes } from 'firebase/storage';
import { from, map, Observable, of, tap } from 'rxjs';

import { Injectable } from '@angular/core';
import { collectionData, docSnapshots, Firestore } from '@angular/fire/firestore';
import { ref, Storage } from '@angular/fire/storage';

let localCache: any = {
};

@Injectable({ providedIn: 'root' })
export class CrudService<T> {
  collectionName = 'collection';

  constructor(protected firestore: Firestore, protected storage: Storage) { }

  getItems(): Observable<T[]> {
    const itemCollection = collection(this.firestore, this.collectionName);
    const cacheName = `get-items-${this.collectionName}`;

    if (localCache[cacheName]) {
      return of(localCache[cacheName]);
    }
    const obs$ = collectionData(itemCollection, { idField: 'id' }) as Observable<T[]>;

    return obs$.pipe(
      tap((data: any) => localCache[cacheName] = data)
    );
  }

  countItems(): Observable<AggregateQuerySnapshot<{ count: AggregateField<number>; }>> {
    const itemCollection = collection(this.firestore, this.collectionName);
    const query1 = query(itemCollection);
    const cacheName = `count-items-${this.collectionName}`;

    if (localCache[cacheName]) {
      return of(localCache[cacheName]);
    }
    const obs$ = from(getCountFromServer(query1));

    return obs$.pipe(
      tap((data: any) => localCache[cacheName] = data)
    );
  }

  countItemsFiltered(
    queryField: string,
    queryOperator: any,
    queryValue: any): Observable<AggregateQuerySnapshot<{ count: AggregateField<number>; }>> {
    const itemCollection = collection(this.firestore, this.collectionName);
    const cacheName = `count-items-filtered-${this.collectionName}-${queryField}-${queryOperator}-${queryValue}`;

    const filteredQuery = query(
      itemCollection,
      where(queryField, queryOperator, queryValue)
    );

    if (localCache[cacheName]) {
      return of(localCache[cacheName]);
    }
    const obs$ = from(getCountFromServer(filteredQuery));

    return obs$.pipe(
      tap((data: any) => localCache[cacheName] = data)
    );
  }

  getItemsQuery(
    queryField: string,
    queryOperator: any,
    queryValue: any
  ): Observable<T[]> {
    const itemCollection = collection(this.firestore, this.collectionName);
    const cacheName = `get-items-query-${this.collectionName}-${queryField}-${queryOperator}-${queryValue}`;

    if (localCache[cacheName]) {
      return of(localCache[cacheName]);
    }

    const myQuery = query(
      itemCollection,
      where(queryField, queryOperator, queryValue)
    );

    const obs$ = collectionData(myQuery, { idField: 'id' }) as Observable<T[]>;

    return obs$.pipe(
      tap((data: any) => localCache[cacheName] = data)
    );
  }

  getItemsQueries(...args: any[]): Observable<T[]> {
    const [queryField1, queryValue1, queryField2, queryValue2, queryField3, queryValue3] = args;
    const itemCollection = collection(this.firestore, this.collectionName);

    let myQuery;
    if (queryField2 && queryValue2 && queryValue3 && queryField3) {
      myQuery = query(
        itemCollection,
        where(queryField1, '==', queryValue1),
        where(queryField2, '==', queryValue2),
        where(queryField3, '==', queryValue3),
      );
    } else if (queryField2 && queryValue2) {
      myQuery = query(
        itemCollection,
        where(queryField1, '==', queryValue1),
        where(queryField2, '==', queryValue2),
      );
    } else {
      myQuery = query(
        itemCollection,
        where(queryField1, '==', queryValue1),
      );
    }

    const obs$ = collectionData(myQuery, { idField: 'id' }) as Observable<T[]>;

    return obs$;
  }

  getItemsSorted(
    sortColumn: string,
    sortOrder?: string
  ): Observable<T[]> {
    const itemCollection = collection(this.firestore, this.collectionName);
    const sort: OrderByDirection = sortOrder as OrderByDirection || 'asc';
    const cacheName = `get-items-sorted-${this.collectionName}-${sortColumn}-${sort}`;

    if (localCache[cacheName]) {
      return of(localCache[cacheName]);
    }

    const q1 = query(itemCollection, orderBy(sortColumn, sort));


    const obs$ = collectionData(q1, { idField: 'id' }) as Observable<T[]>;

    return obs$.pipe(
      tap((data: any) => {
        localCache[cacheName] = data;
      })
    );
  }

  getItem(id: string): Observable<T> {
    const path = `${this.collectionName}/${id}`;
    const ref = doc(this.firestore, path);
    return docSnapshots(ref).pipe(map((snap) => snap.data())) as Observable<T>;
  }

  getItemBySlug(slug: string): Observable<T> {
    const itemCollection = collection(this.firestore, this.collectionName);
    const myQuery = query(itemCollection, where('slug', '==', slug));
    return collectionData(myQuery, { idField: 'id' }).pipe(
      map((r) => r[0])
    ) as Observable<T>;
  }

  createItem(payload: T): Promise<DocumentReference<T>> {
    const ref = collection(this.firestore, this.collectionName);
    localCache = {};
    return addDoc(ref, payload as any);
  }

  setItem(payload: T, id: string): Promise<void> {
    const path = `${this.collectionName}/${id}`;
    const ref = doc(this.firestore, path);
    localCache = {};
    return setDoc(ref, payload as any);
  }

  updateItem(id: string, payload: Partial<T>): Promise<void> {
    const path = `${this.collectionName}/${id}`;
    const ref = doc(this.firestore, path);
    localCache = {};
    return updateDoc(ref, payload as any);
  }

  deleteItem(id: string): Promise<void> {
    const path = `${this.collectionName}/${id}`;
    const ref = doc(this.firestore, path);
    localCache = {};
    return deleteDoc(ref);
  }

  async uploadFile(file: any, filename: string) {
    const storageRef = ref(this.storage, `images/${filename}`);
    const metadata = { contentType: 'image/jpeg' };

    return uploadBytes(storageRef, file, metadata);
  }

  async getDownloadURL(filename: string) {
    return getDownloadURL(ref(this.storage, filename));
  }


}
