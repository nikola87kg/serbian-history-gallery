import { GoogleAuthProvider, signInWithPopup, signOut, User } from 'firebase/auth';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';

import { inject, Injectable } from '@angular/core';
import { Auth, user } from '@angular/fire/auth';
import { UserProfile, UserProfileService } from '@services/user-profile.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private provider = new GoogleAuthProvider();
  private auth: Auth = inject(Auth);
  private userProfileService = inject(UserProfileService);

  public user$!: Observable<User | null>;
  public userProfile$!: Observable<UserProfile | null>;
  public login$!: Observable<boolean>;
  public loginAdmin$!: Observable<boolean>;

  initAuth(): void {
    this.user$ = user(this.auth as any) || of(null);
    this.login$ = this.user$.pipe(map((value) => !!value));
    this.userProfile$ = this.getUserProfile();
    this.loginAdmin$ = this.getAdminProfile();
  }

  getUserProfile(): Observable<UserProfile | null> {
    return this.user$?.pipe(
      switchMap((user: User | null) => {
        if (!user) {
          return of(null);
        }
        return this.userProfileService.getItem(user.uid);
      })
    ) || of(null);
  }

  getAdminProfile(): Observable<boolean> {
    return this.getUserProfile().pipe(
      map((value: UserProfile | null) => !!value?.isAdmin)
    );
  }

  googleLogin(): void {
    signInWithPopup(this.auth as any, this.provider).then((result) => {
      const credential = GoogleAuthProvider.credentialFromResult(result);
      const user = result.user;
      const id = user.uid;
      const isAdmin = id === environment.adminId;

      const payload: UserProfile = {
        id: user.uid,
        name: user.displayName,
        token: credential?.idToken,
        email: user.email,
        phone: user.phoneNumber,
        image: user.photoURL,
        isAdmin,
        createdAt: Date.now(),
      };

      this.userProfileService.updateItem(id, payload).catch(_ => {
        this.userProfileService.setItem(payload, id);
      });
    });
  }

  googleLogout(): Promise<void> {
    return signOut(this.auth as any);
  }
}
