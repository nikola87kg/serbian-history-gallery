import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SymbolsService } from './symbols.service';

const firestoreMock = {};
const storageMock = {};

describe('SymbolsService', () => {
  let service: SymbolsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: firestoreMock },
        { provide: Storage, useValue: storageMock },
      ]
    });
    service = TestBed.inject(SymbolsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
