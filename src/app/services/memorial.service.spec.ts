import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { MemorialService } from './memorial.service';

describe('MonumentsService', () => {
  let service: MemorialService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    service = TestBed.inject(MemorialService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
