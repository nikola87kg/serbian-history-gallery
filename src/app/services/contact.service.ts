import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export interface Message {
  id?: string | null;
  type: string | null;
  text: string | null;
  name: string | null;
  email: string | null;
  created?: number | null;
  updated?: number | null;
}

@Injectable({ providedIn: 'root' })
export class ContactService extends CrudService<Message> {
  override collectionName = 'contact-messages';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}