import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ChronologyService } from './chronology.service';

const firestoreMock = {};
const storageMock = {};

describe('ChronologyService', () => {
  let service: ChronologyService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: firestoreMock },
        { provide: Storage, useValue: storageMock },
      ]
    });
    service = TestBed.inject(ChronologyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
