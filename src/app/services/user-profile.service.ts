import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export interface UserProfile {
  id: string;
  name: string | null;
  token?: string;
  email: string | null;
  phone: string | null;
  image: string | null;
  createdAt: number;
  favourites?: string[];
  isAdmin: boolean;
}

@Injectable({ providedIn: 'root' })
export class UserProfileService extends CrudService<UserProfile> {
  override collectionName = 'user-profiles';

  constructor(
    firestore: Firestore,
    storage: Storage) {
    super(firestore, storage);
  }

  addToFavourites(userProfile: UserProfile, slug: string): any[] {
    const id = userProfile.id;
    const favourites = (userProfile.favourites || []);

    if (favourites?.length >= 20) {
      const message = `Достигли сте максималан број личности у фаворитима.`;
      return [true, message];
    }

    favourites.push(slug);
    this.updateItem(id, { favourites });
    const message = `Личност успешно додата у фаворите.`;
    return [false, message];
  }

  removeFromFavourites(userProfile: UserProfile, slug: string): any[] {
    const id = userProfile.id;
    let favourites = (userProfile.favourites || []);

    favourites = favourites.filter(e => e !== slug);
    this.updateItem(id, { favourites });
    const message = `Личност успешно уклоњена из фаворита.`;
    return [false, message];
  }

}
