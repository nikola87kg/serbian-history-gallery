import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export enum HolidayType {
  saint = 'saint',
  religious = 'religious',
  national = 'national',
  world = 'world',
}

export function HolidayTypeToName(key: string): string {
  const holidayTypeMapping: any = {
    saint: 'Славе',
    religious: 'Верски празници',
    national: 'Државни празници',
    world: 'Међународни празници',
  };

  return holidayTypeMapping[key];
}

export const holidayTypes: any[] = [
  { 'id': HolidayType.national, 'name': HolidayTypeToName(HolidayType.national) },
  { 'id': HolidayType.world, 'name': HolidayTypeToName(HolidayType.world) },
  { 'id': HolidayType.religious, 'name': HolidayTypeToName(HolidayType.religious) },
  { 'id': HolidayType.saint, 'name': HolidayTypeToName(HolidayType.saint) },
];

export const movableHolidaysDates: any = {
  'vrbica-lazareva-subota': ['2023.04.08', '2024.04.27', '2025.04.12', '2026.04.04', '2027.04.24', '2028.04.08', '2029.03.31', '2030.04.20'],
  'cveti': ['2023.04.09', '2024.04.28', '2025.04.13', '2026.04.05', '2027.04.25', '2028.04.09', '2029.04.01', '2030.04.21'],
  'veliki-petak': ['2023.04.14', '2024.05.03', '2025.04.18', '2026.04.10', '2027.04.30', '2028.04.14', '2029.04.06', '2030.04.26'],
  'vaskrs': ['2023.04.16', '2024.05.05', '2025.04.20', '2026.04.12', '2027.05.02', '2028.04.16', '2029.04.08', '2030.04.28'],
  'katolicki-uskrs': ['2023.04.09', '2024.03.31', '2025.04.20', '2026.04.05', '2027.03.28', '2028.04.16', '2029.04.01', '2030.04.21'],
  'pasha': ['2023.04.06', '2024.04.23', '2025.04.13', '2026.04.02', '2027.04.22', '2028.04.11', '2029.03.31', '2030.04.18'],
  'spasovdan': ['2023.05.25', '2024.06.13', '2025.05.29', '2026.05.21', '2027.06.10', '2028.05.25', '2029.05.17', '2030.06.06'],
  'jom-kipur': ['2023.09.24', '2024.10.11', '2025.10.1', '2026.09.20', '2027.10.10'],
  'kurban-bajram': ['2023.06.28', '2024.06.16', '2025.06.06', '2026.05.27'],
  'ramazanski-bajram': ['2023.03.23', '2024.03.11', '2025.03.01', '2026.02.18', '2027.02.08', '2028.01.28', '2029.01.16', '2030.01.05'],
};

export interface Holiday {
  description: string | null;
  type: HolidayType | null;
  id?: string | null;
  imagePath?: string | null;
  name: string | null;
  saintDate: string | null;
  saintDay: number | null;
  saintMonth: number | null;
  movable: boolean | null;
  slug: string | null;
  created: number | null;
  updated: number | null;
}

export interface SaintGame extends Holiday {
  correctOrder: boolean;
}

@Injectable({ providedIn: 'root' })
export class SaintsService extends CrudService<Holiday> {
  override collectionName = 'saints';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}
