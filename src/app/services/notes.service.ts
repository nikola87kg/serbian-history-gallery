import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export enum NoteType {
  month = 'month',
  reminder = 'reminder',
  idea = 'idea',
  bug = 'bug',
  improvement = 'improvement',
  topics = 'topics',
  quotes = 'quotes',
  other = 'other',

}

export const noteTypes: any[] = [
  {
    id: NoteType.month,
    name: 'Месец',
    order: 1,
  },
  {
    id: NoteType.reminder,
    name: 'Подсетник',
    order: 2,
  },
  {
    id: NoteType.idea,
    name: 'Идеја',
    order: 3,
  },
  {
    id: NoteType.bug,
    name: 'Баг',
    order: 4,
  },
  {
    id: NoteType.improvement,
    name: 'Унапређење',
    order: 5,
  },
  {
    id: NoteType.topics,
    name: 'Слободна тема',
    order: 6,
  },
  {
    id: NoteType.quotes,
    name: 'Мој цитат',
    order: 7,
  },
  {
    id: NoteType.other,
    name: 'Остало',
    order: 8,
  }
];

export interface Note {
  id?: string | null;
  title: string | null;
  text: string | null;
  type: NoteType | null;
  created: number | null;
  updated: number | null;
}

@Injectable({ providedIn: 'root' })
export class NotesService extends CrudService<Note> {
  override collectionName = 'notes';
  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}