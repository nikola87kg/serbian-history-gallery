import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ManifestationsService } from './manifestations.service';

describe('ManifestationsService', () => {
  let service: ManifestationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    service = TestBed.inject(ManifestationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
