import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export interface Battle {
  id?: string | null;
  name: string | null;
  slug: string | null;
  leader: string | null;
  leaderSlug: string | null;
  yearMonth: string | null;
  dates: string | null;
  enemySide: string | null;
  enemyLeader: string | null;
  enemyEstimation: string | null;
  ourSide: string | null;
  ourSideEstimation: string | null;
  battleOutcome: string | null;
  place: string | null;
  youtube?: string | null;
  wikipedia?: string | null;
  created: number | null;
  updated: number | null;
}

@Injectable({ providedIn: 'root' })
export class BattleService extends CrudService<Battle> {
  override collectionName = 'battles';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}
