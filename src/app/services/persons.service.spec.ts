import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { PersonsService } from './persons.service';

const firestoreMock = {};
const storageMock = {};

describe('PersonsService', () => {
  let service: PersonsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: firestoreMock },
        { provide: Storage, useValue: storageMock },
      ]
    });
    service = TestBed.inject(PersonsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
