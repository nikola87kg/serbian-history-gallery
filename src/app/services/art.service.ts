import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export enum ArtType {
  painting = 'Уметничка слика',
  musical = 'Музичка композиција',
  literary = 'Књижевно дело'
}

export const artTypes = [
  { 'id': 'painting', 'name': ArtType.painting },
  { 'id': 'musical', 'name': ArtType.musical },
  { 'id': 'literary', 'name': ArtType.literary },
];

export interface Art {
  id?: string | null;
  name: string | null;
  slug: string | null;
  creator: string | null;
  creatorSlug: string | null;
  yearMonth: string | null;
  dates: string | null;
  type: ArtType | null;
  created: number | null;
  updated: number | null;
}

@Injectable({ providedIn: 'root' })
export class ArtService extends CrudService<Art> {
  override collectionName = 'arts';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}