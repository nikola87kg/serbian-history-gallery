
import { Observable, of, tap } from 'rxjs';

import { Injectable } from '@angular/core';
import {
  collection, collectionData, CollectionReference, Firestore
} from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export interface Person {
  age: number | null;
  biographyUrl: string | null;
  birthDay: number | null;
  birthDeath: string | null;
  birthMonth: number | null;
  birthYear: number | null;
  category: string | null;
  city: string | null;
  commonName: string | null;
  created: number | null;
  epoch: string | null;
  firstName: string | null;
  frontPage: boolean;
  fullName: string | null;
  fullTitle: string | null;
  id?: string | null;
  imagePath?: string | null;
  instagramUrl: string | null;
  instagramPosts: string | null;
  keyNotes: string | null;
  lastName: string | null;
  nickName: string | null;
  porekloUrl: string | null;
  shortDescription: string | null;
  slug: string | null;
  topTen: boolean;
  localHero: boolean;
  tags?: string[] | null;
  updated: number | null;
  wikiUrl: string | null;
  workPieces: string | null;
  workPiecesTitle: string | null;
}

export interface PersonGame extends Person {
  correctOrder?: boolean;
}

let localPersons: Person[] = [];

@Injectable({ providedIn: 'root' })
export class PersonsService extends CrudService<Person> {
  override collectionName = 'persons';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }

  getItemsServer(): Observable<Person[]> {
    const itemCollection: CollectionReference = collection(this.firestore, this.collectionName);
    const obs$ = collectionData(itemCollection, { idField: 'id' }) as Observable<Person[]>;
    /*     const currentDate = Date.now();
        const lastDate = localStorage.getItem('lastCacheDate');
    
        if (lastDate) {
          const lastList = localStorage.getItem('lastCachList');
    
          if (lastList) {
            const msDiff = currentDate - JSON.parse(lastDate!);
            const msCachePeriod = 100;
            if (lastDate && msDiff < msCachePeriod) {
              const personsStorage: Person[] = JSON.parse(lastList);
              localPersons = [...personsStorage];
              return of(personsStorage);
            }
          }
        } */

    return obs$.pipe(
      tap((personsFirebase: any) => {
        localPersons = [...personsFirebase];
      })
    );
  }

  override getItems(): Observable<Person[]> {
    const obs$ = of(localPersons);
    return obs$;
  }

  override getItemsQuery(
    queryField: string,
    queryOperator: any,
    queryValue: any
  ): Observable<Person[]> {
    const result = localPersons.filter((p: any) => {
      if (queryOperator === 'in') {
        const value = p[queryField as any] as string;
        const queryArray = queryValue || [];
        return queryArray.includes(value);
      }

      if (queryOperator === '!=') {
        const value = p[queryField as any] as string;
        return queryValue !== value;
      }

      if (queryOperator === 'array-contains') {
        const value = p[queryField as any] as string;
        return value.includes(queryValue);
      }

      const value = p[queryField as any] as string;
      return queryValue === value;
    });

    return of(result);
  }

  override getItemsQueries(...args: any[]): Observable<Person[]> {
    const [queryField1, queryValue1, queryField2, queryValue2, queryField3, queryValue3] = args;
    const result = localPersons.filter((p: any) => {
      const conditionOne = p[queryField1] === queryValue1;
      const conditionTwo = p[queryField2] === queryValue2;
      const conditionThree = p[queryField3] === queryValue3;
      if (queryField3) {
        return conditionOne && conditionTwo && conditionThree;
      }
      if (queryField2) {
        return conditionOne && conditionTwo;
      }

      return conditionOne;
    });

    return of(result);

  }

  override getItemsSorted(
    sortColumn: string,
    sortOrder?: string
  ): Observable<Person[]> {
    const listCopy: Person[] = [...localPersons];
    const result = listCopy.filter(p => !!(p as any)[sortColumn]);
    const sort = (!sortOrder || sortOrder === 'asc') ? 1 : -1;
    result.sort((a: any, b: any) => {
      const isNumber = typeof a === 'number' && typeof b === 'number';
      if (isNumber) {
        return sort > 0 ? (a - b) : (b - a);
      }

      const first = String(a[sortColumn]);
      const second = String(b[sortColumn]);

      return sort > 0
        ? first.localeCompare(second)
        : second.localeCompare(first);

    });

    return of(result);
  }

  override getItem(id: string): Observable<Person> {
    const item = localPersons.find(p => p.id === id);
    return of(item!);
  }

  override getItemBySlug(slug: string): Observable<Person> {
    const item = localPersons.find(p => p.slug === slug);
    return of(item!);
  }

}
