import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SaintsService } from './saints.service';

const firestoreMock = {};
const storageMock = {};

describe('SaintsService', () => {
  let service: SaintsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: firestoreMock },
        { provide: Storage, useValue: storageMock },
      ]
    });
    service = TestBed.inject(SaintsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
