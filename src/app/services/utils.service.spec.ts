import { TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';

import { Month, months, UtilsService } from './utils.service';

describe('UtilsService', () => {
  let service: UtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should slugify an input value', () => {
    const input1 = 'Ђорђе Ш. Жеравичић';
    const expected1 = 'djordje-s-zeravicic';

    const input2 = 'Djordje P. Džehvežovičić';
    const expected2 = 'djordje-p-dzehvezovicic';

    const output1 = UtilsService.slugify(input1);
    const output2 = UtilsService.slugify(input2);

    expect(output1).toBe(expected1);
    expect(output2).toBe(expected2);
  });

  it('should trim text', () => {
    const input = '   some test   ';
    const expected = 'some test';

    const output = UtilsService.trimText(input);
    expect(output).toBe(expected);
  });

  it('should get month id for the given month order', () => {
    const input = '1';
    const expected = Month.jan;

    const output = UtilsService.getMonthIdFromOrder(input);
    expect(output).toBe(expected);
  });

  it('should get month object for the given month id', () => {
    const input = Month.oct;
    const expectedId = Month.oct;
    const expectedOrder = '10';
    const expectedName = 'Октобар';

    const month = UtilsService.getMonthObjectFromId(input);
    expect(month.id).toBe(expectedId);
    expect(month.order).toBe(expectedOrder);
    expect(month.name).toBe(expectedName);
  });

  it('should get array of months in label/value format', () => {
    const output = UtilsService.getMonthOptions();

    expect(output[0].label).toBe(months[0].name);
    expect(output[5].value).toBe(months[5].order);
    expect(output.length).toBe(months.length);
  });

  it('should get title for the given order', () => {
    const input = '3';
    const expected = 'Март';

    const output = UtilsService.getMonthTitleFromOrder(input);
    expect(output).toBe(expected);
  });

  it('should get order for the given title', () => {
    const input = 'Март';
    const expected = '3';

    const output = UtilsService.getMonthOrderFromTitle(input);
    expect(output).toBe(expected);
  });

  it('should open new URL', () => {
    jest.spyOn(window, 'open');
    const input = 'biografija/karadjordje-petrovic';
    UtilsService.openOnMiddleClick(input);
    expect(window.open).toBeCalledTimes(1);
  });

  it('should group a given array by the given property', () => {
    const inputArray = [...mockQuotes];
    const inputProperty = 'authorSlug';
    const expectedAuthorSlug = 'author-1';

    const output = UtilsService.groupBy(inputArray, inputProperty);
    const firstAuthorQuotes = output[0];

    expect(firstAuthorQuotes.length).toBe(2);
    expect(firstAuthorQuotes[0].authorSlug).toBe(expectedAuthorSlug);
  });

  it('should get validation error when double space exists', () => {
    const input = new FormControl('Текст  има дупли размак.');

    const output = UtilsService.validateDoubleSpace(input);
    expect(output).not.toBe(null);

    if (output) {
      expect(output['doublespace']).toBe(true);
    }
  });

  it('should get null when double space does not exists', () => {
    const input = new FormControl('Текст је ок.');

    const output = UtilsService.validateDoubleSpace(input);
    expect(output).toBe(null);
  });

  it('should shuffle the given list order', () => {
    const inputArray = [...mockQuotes];
    const idOrder = inputArray.map(i => +i.id);

    UtilsService.shuffleListOrder(inputArray);
    const idOrderNew = inputArray.map(i => +i.id);
    expect(JSON.stringify(idOrder)).not.toBe(JSON.stringify(idOrderNew));
  });

  it('should remove entries with duplicate key', () => {
    const inputArray = [...mockQuotes];

    const newList = UtilsService.removeDuplicatesByKey(inputArray, 'authorSlug');
    expect(inputArray.length).not.toBe(newList.length);
    expect(newList.length).toBe(3);
  });

  it('should get today correctly formatted', () => {
    const today = UtilsService.getTodayFormatted();
    expect(today).toMatch(/\..*?\./);
    expect(today).toMatch(/^20/);
  });

  it('should get correct horoscope sign', () => {
    const day = 1;
    const month = 8;

    const output = UtilsService.getHoroscopeSign(day, month);
    expect(output).toBe('Лав');
  });
});

const mockQuotes = [
  { id: "1", authorSlug: "author-1" },
  { id: "2", authorSlug: "author-1" },
  { id: "3", authorSlug: "author-2" },
  { id: "4", authorSlug: "author-3" },
  { id: "5", authorSlug: "author-3" },
  { id: "6", authorSlug: "author-3" },
  { id: "7", authorSlug: "author-3" },
  { id: "8", authorSlug: "author-3" },
  { id: "9", authorSlug: "author-3" },
  { id: "10", authorSlug: "author-3" },
];