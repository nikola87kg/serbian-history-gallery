import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { NatureService } from './nature.service';

describe('NatureService', () => {
  let service: NatureService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
      ]
    });
    service = TestBed.inject(NatureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
