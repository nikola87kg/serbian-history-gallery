import * as speakingurl from 'speakingurl';

import { ViewportScroller } from '@angular/common';
import { Injectable, Injector } from '@angular/core';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Router } from '@angular/router';
import { Person } from './persons.service';
import { Holiday, movableHolidaysDates } from './saints.service';

export enum Month {
  jan = 'januar',
  feb = 'februar',
  mar = 'mart',
  apr = 'april',
  may = 'maj',
  jun = 'jun',
  jul = 'jul',
  aug = 'avgust',
  sep = 'septembar',
  oct = 'oktobar',
  nov = 'novembar',
  dec = 'decembar',
}

export interface MonthData {
  id: Month,
  order: string,
  name: string,
}

export const months: MonthData[] = [
  { id: Month.jan, order: '1', name: 'Јануар' },
  { id: Month.feb, order: '2', name: 'Фебруар' },
  { id: Month.mar, order: '3', name: 'Март' },
  { id: Month.apr, order: '4', name: 'Април' },
  { id: Month.may, order: '5', name: 'Мај' },
  { id: Month.jun, order: '6', name: 'Јун' },
  { id: Month.jul, order: '7', name: 'Јул' },
  { id: Month.aug, order: '8', name: 'Август' },
  { id: Month.sep, order: '9', name: 'Септембар' },
  { id: Month.oct, order: '10', name: 'Октобар' },
  { id: Month.nov, order: '11', name: 'Новембар' },
  { id: Month.dec, order: '12', name: 'Децембар' },
];

@Injectable({ providedIn: 'root' })
export class UtilsService {
  public static utilsInjector: Injector;
  public static utilsRouter: Router;

  constructor(public injector: Injector, public router: Router) {
    if (!UtilsService.utilsInjector) {
      UtilsService.utilsInjector = injector;
    }
    if (!UtilsService.utilsRouter) {
      UtilsService.utilsRouter = router;
    }
  }

  static scrollTop() {
    if (typeof window !== "undefined") {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }

  static scrollTo(scroller: ViewportScroller, scrollPosition: [number, number]) {
    scroller.scrollToPosition(scrollPosition);
    setTimeout(() => {
      scroller.scrollToPosition(scrollPosition);
    }, 5);
    setTimeout(() => {
      scroller.scrollToPosition(scrollPosition);
    }, 40);
    setTimeout(() => {
      scroller.scrollToPosition(scrollPosition);
    }, 70);
    setTimeout(() => {
      scroller.scrollToPosition(scrollPosition);
    }, 120);
    setTimeout(() => {
      scroller.scrollToPosition(scrollPosition);
    }, 150);
  }

  static openSnackbar(message: string, error = false, favourites = false) {
    if (!UtilsService.utilsInjector) {
      console.error('Injector is not initialized');
      return;
    }

    const snackBar = UtilsService.utilsInjector.get(MatSnackBar);
    const router = UtilsService.utilsInjector.get(Router);

    const panelClass = error
      ? ['custom-snackbar', 'error']
      : ['custom-snackbar', 'success'];

    snackBar.open(message, undefined, {
      panelClass,
      duration: 5000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
    });

    const snackBarContainer = document.querySelector('mat-snack-bar-container');
    if (snackBarContainer && favourites) {
      snackBarContainer.addEventListener('click', () => {
        router.navigateByUrl('favoriti');
      });
    }
  }

  // Cyrillic to latin
  static slugify(inputValue: string) {
    inputValue = inputValue || '';
    inputValue = inputValue.split(',').join('');
    inputValue = inputValue.split('Ч').join('ц');
    inputValue = inputValue.split('ч').join('ц');
    inputValue = inputValue.split('Ш').join('с');
    inputValue = inputValue.split('ш').join('с');
    inputValue = inputValue.split('ж').join('з');
    inputValue = inputValue.split('Ж').join('з');
    const name = this.trimText(inputValue);
    const options = { maintainCase: false, separator: '-', lang: 'sr' };
    const slugFunction = speakingurl.createSlug(options);
    let slug = slugFunction(name);
    slug = slug.split('kh').join('h');
    return slug;
  }

  static latinToCyrillic(text: string): string {
    const map: { [key: string]: string } = {
      'Lj': 'Љ', 'Nj': 'Њ', 'Dž': 'Џ', 'lj': 'љ', 'nj': 'њ', 'dž': 'џ',
      'A': 'А', 'B': 'Б', 'V': 'В', 'G': 'Г', 'D': 'Д', 'Đ': 'Ђ', 'E': 'Е', 'Ž': 'Ж', 'Z': 'З', 'I': 'И', 'J': 'Ј',
      'K': 'К', 'L': 'Л', 'M': 'М', 'N': 'Н', 'O': 'О', 'P': 'П', 'R': 'Р', 'S': 'С', 'T': 'Т', 'Ć': 'Ћ', 'U': 'У',
      'F': 'Ф', 'H': 'Х', 'C': 'Ц', 'Č': 'Ч', 'Š': 'Ш',
      'a': 'а', 'b': 'б', 'v': 'в', 'g': 'г', 'd': 'д', 'đ': 'ђ', 'e': 'е', 'ž': 'ж', 'z': 'з', 'i': 'и', 'j': 'ј',
      'k': 'к', 'l': 'л', 'm': 'м', 'n': 'н', 'o': 'о', 'p': 'п', 'r': 'р', 's': 'с', 't': 'т', 'ć': 'ћ', 'u': 'у',
      'f': 'ф', 'h': 'х', 'c': 'ц', 'č': 'ч', 'š': 'ш'
    };

    // Replace multi-character Latin sequences first
    return text.replace(/Lj|Nj|Dž|lj|nj|dž|./g, (match) => map[match] || match);
  }

  static trimText(text: string = '') {
    text = (text || ' ').trim();
    for (let char = 0; char < text.length; char++) {
      if (text.charAt(char) !== ' ' && text.charAt(char) !== '\t') {
        return text.substring(char);
      }
    }
    return text;
  }

  static getMonthIdFromOrder(monthOrder: string) {
    const month = months.find(m => m.order === monthOrder);
    return month?.id!;
  }

  static getMonthObjectFromId(id: string): MonthData {
    const month = months.find(m => m.id === id);
    return month as MonthData;
  }

  static getMonthOptions() {
    return months.map(m => {
      return {
        value: m.order,
        label: m.name,
      };
    });
  }

  static getMonthTitleFromOrder(monthOrder: string): string {
    const month = months.find(m => m.order === monthOrder);
    return month?.name!;
  }

  static getMonthTitleFromId(monthId: string): string {
    const month = months.find(m => m.id === monthId);
    return month?.name!;
  }

  static getMonthOrderFromTitle(serbianMonth: string): string {
    const month = months.find(m => m.name === serbianMonth);
    return month?.order!;
  }

  static getMonthIdFromTitle(serbianMonth: string): string {
    const month = months.find(m => m.name === serbianMonth);
    return month?.id!;
  }

  static getMonths(): MonthData[] {
    return [...months];
  }

  static openOnMiddleClick(url: string) {
    if (typeof window !== "undefined") {
      window.open(url);
    }
  }

  static groupBy(arr: any[], property: string): any {
    const groupedKeys = arr.reduce(
      (group: { [key: string]: any[]; }, item) => {
        if (!group[item[property]]) {
          group[item[property]] = [];
        }
        group[item[property]].push(item);
        return group;
      }, {});

    const groups = Object.keys(groupedKeys).map(k => groupedKeys[k as any]);
    return groups;
  }

  static groupAndSortBy(arr: any[], property: string): any {
    const groupedByItem = arr.reduce((acc, item) => {
      const prop = item[property].trim();

      if (prop === '') { return acc };
      if (!acc[prop]) {
        acc[prop] = [];
      }

      acc[prop].push(item);
      return acc;
    }, {});

    const sortedGroups = Object.entries(groupedByItem)
      .sort((a: any, b: any) => b[1].length - a[1].length)
      .map(([title, elements]) => ({ title, elements }));


    return sortedGroups;
  }

  static validateDoubleSpace(control: AbstractControl): ValidationErrors | null {
    if (!control.value) {
      return null;
    }
    const result = control.value && control.value.includes('  ')
      ? { doublespace: true }
      : null;
    return result;
  }

  static shuffleListOrder(list: any[]) {
    for (var i = list.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = list[i];
      list[i] = list[j];
      list[j] = temp;
    }
  }

  static removeDuplicatesByKey(list: any[], key: string): any[] {
    const keys: string[] = [];
    const newList: any[] = [];

    list.forEach(item => {
      const uniqueProperty = item[key];

      if (!keys.includes(uniqueProperty)) {
        keys.push(uniqueProperty);
        newList.push(item);
      }
    });

    return newList;
  }

  static getTodayFormatted() {
    var date = new Date();

    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    const format = `${year}.${month}.${day}`;
    return format;
  }

  static getHoroscopeSign(dayNumber: number, monthNumber: number) {
    if ((monthNumber === 3 && dayNumber >= 21) || (monthNumber === 4 && dayNumber <= 19)) {
      return "Ован";
    } else if ((monthNumber === 4 && dayNumber >= 20) || (monthNumber === 5 && dayNumber <= 20)) {
      return "Бик";
    } else if ((monthNumber === 5 && dayNumber >= 21) || (monthNumber === 6 && dayNumber <= 20)) {
      return "Близанци";
    } else if ((monthNumber === 6 && dayNumber >= 21) || (monthNumber === 7 && dayNumber <= 22)) {
      return "Рак";
    } else if ((monthNumber === 7 && dayNumber >= 23) || (monthNumber === 8 && dayNumber <= 22)) {
      return "Лав";
    } else if ((monthNumber === 8 && dayNumber >= 23) || (monthNumber === 9 && dayNumber <= 22)) {
      return "Девица";
    } else if ((monthNumber === 9 && dayNumber >= 23) || (monthNumber === 10 && dayNumber <= 22)) {
      return "Вага";
    } else if ((monthNumber === 10 && dayNumber >= 23) || (monthNumber === 11 && dayNumber <= 21)) {
      return "Шкорпија";
    } else if ((monthNumber === 11 && dayNumber >= 22) || (monthNumber === 12 && dayNumber <= 21)) {
      return "Стрелац";
    } else if ((monthNumber === 12 && dayNumber >= 22) || (monthNumber === 1 && dayNumber <= 19)) {
      return "Јарац";
    } else if ((monthNumber === 1 && dayNumber >= 20) || (monthNumber === 2 && dayNumber <= 18)) {
      return "Водолија";
    } else {
      return "Рибе";
    }
  }

  static getTodayDay(): string {
    const today = new Date();
    const day = today.getDate();
    return String(day); // String(22);
  }

  static getTodayMonth(): string {
    const today = new Date();
    const month = today.getMonth() + 1;
    return String(month); // String(12);
  }

  static getTodaySlug(): string {
    const day = this.getTodayDay();
    const month = this.getTodayMonth();
    const serbianMonth = months.find(m => m.order === month);
    const path = serbianMonth?.id;
    const slug = `${day}-${path}`;
    return slug;
  }

  static getDaySlug(birthDay: number, birthMonth: number): string {
    const serbianMonth = months.find(m => m.order === String(birthMonth));
    const path = serbianMonth?.id;
    const slug = `${birthDay}-${path}`;
    return slug;
  }

  static setMovableHolidays(holidays: Holiday[]): Holiday[] {
    const movableHolidays = holidays.filter(h => !h.saintMonth);
    movableHolidays.forEach(holiday => this.updateMovableHoliday(holiday));
    holidays.sort((a, b) => a.saintDate!.localeCompare(b.saintDate!));
    return holidays;
  }

  static updateMovableHoliday(holiday: Holiday) {
    const slug: string = holiday.slug!;
    const dates = movableHolidaysDates[slug];

    const year = String(new Date().getFullYear());
    const date = dates.find((d: string) => d.startsWith(year));

    if (!date) {
      return;
    }

    const dateParts: string[] = date.split('.');

    holiday.saintDate = `${dateParts[1]}.${dateParts[2]}`;
    holiday.saintMonth = Number(dateParts[1]);
    holiday.saintDay = Number(dateParts[2]);
    holiday.movable = true;
  }

  static groupByBirthDate(persons: Person[]): { [key: string]: string[]; } {
    // Sort persons by birthYear, birthMonth, and birthDay
    const sortedPersons = persons.filter(person =>
      person.birthDay !== null && person.birthMonth !== null && person.birthYear !== null
    ).sort((a, b) => {
      if (a.birthYear !== b.birthYear) {
        return a.birthYear! - b.birthYear!;
      } else if (a.birthMonth !== b.birthMonth) {
        return a.birthMonth! - b.birthMonth!;
      } else {
        return a.birthDay! - b.birthDay!;
      }
    });

    // Group sorted persons by birth date
    const grouped = sortedPersons.reduce((acc, person) => {
      const key = `${person.birthYear}-${person.birthMonth}-${person.birthDay}`;
      if (!acc[key]) {
        acc[key] = [];
      }
      acc[key].push(person.commonName!);
      return acc;
    }, {} as { [key: string]: string[]; });

    // Filter out groups with fewer than two members
    let filteredGrouped = Object.keys(grouped)
      .filter(key => grouped[key].length > 1)
      .reduce((acc, key) => {
        acc[key] = grouped[key];
        return acc;
      }, {} as { [key: string]: string[]; });

    // Convert filteredGrouped to an array of entries, sort, and convert back to object
    const sortedFilteredGrouped = Object.entries(filteredGrouped)
      .sort((a, b) => {
        const [aDay, aMonth, aYear] = a[0].split('-').map(Number);
        const [bDay, bMonth, bYear] = b[0].split('-').map(Number);
        if (aYear !== bYear) {
          return aYear - bYear;
        } else if (aMonth !== bMonth) {
          return aMonth - bMonth;
        } else {
          return aDay - bDay;
        }
      })
      .reduce((acc, [key, value]) => {
        acc[key] = value;
        return acc;
      }, {} as { [key: string]: string[]; });

    return sortedFilteredGrouped;
  }

  static checkMobileDevice(): boolean {
    return /Mobi|Android|iPhone|iPad|iPod/i.test(navigator.userAgent);
  }
}
