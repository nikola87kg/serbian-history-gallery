import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { EpochsService } from './epochs.service';

const firestoreMock = {};
const storageMock = {};

describe('EpochsService', () => {
  let service: EpochsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: firestoreMock },
        { provide: Storage, useValue: storageMock },
      ]
    });
    service = TestBed.inject(EpochsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
