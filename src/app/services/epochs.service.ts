import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export interface Epoch {
  id?: string | null;
  order?: number | null;
  name: string | null;
  slug: string | null;
  color: string | null;
  created: number | null;
  updated: number | null;
}

@Injectable({ providedIn: 'root' })
export class EpochsService extends CrudService<Epoch> {
  override collectionName = 'epochs';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}
