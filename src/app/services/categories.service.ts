import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export interface Category {
  id?: string | null;
  slug: string | null;
  name: string | null;
  order: number | null;
  epochs?: string[] | null;
  imagePath?: string | null;
  created: number | null;
  updated: number | null;
}

@Injectable({ providedIn: 'root' })
export class CategoriesService extends CrudService<Category> {
  override collectionName = 'categories';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}
