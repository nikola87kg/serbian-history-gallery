

import { BehaviorSubject } from 'rxjs';

import { computed, effect, Injectable, Signal, signal } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  private darkModeSignal = signal<boolean>(false);
  public adminHeadline = signal<string>('');
  public latinModeSignal = signal<boolean>(false);
  public translitTrigger$$ = new BehaviorSubject(false);

  constructor() {
    this.checkLocalStorage();
    this.setEffects();
  }

  checkLocalStorage() {
    const theme = localStorage.getItem('theme');
    const translit = localStorage.getItem('translit');

    if (theme === 'dark-mode') {
      this.toggleTheme();
      document.body.classList.add('dark-mode');
    } else if (theme === 'light-mode') {
      document.body.classList.remove('dark-mode');
    } else {
      if (this.devicePrefersDarkMode()) {
        this.toggleTheme();
        document.body.classList.add('dark-mode');
      }
    }

    if (translit === 'latin') {
      this.toggleTranslit();
    }
  }

  devicePrefersDarkMode() {
    return window.matchMedia
      && window.matchMedia('(prefers-color-scheme: dark)')?.matches;
  }

  setEffects() {
    effect(() => {
      if (this.isDarkMode()) {
        document.body.classList.add('dark-mode');
        localStorage.setItem('theme', 'dark-mode');
      } else {
        document.body.classList.remove('dark-mode');
        localStorage.setItem('theme', 'light-mode');
      }
    });

    effect(() => {
      const isLatin = this.isLatinMode();
      if (isLatin) {
        localStorage.setItem('translit', 'latin');
      } else {
        localStorage.setItem('translit', 'cyrillic');
      }

      this.translitTrigger$$.next(isLatin);
    });
  }

  toggleTheme(): void {
    this.darkModeSignal.update(currentValue => !currentValue);
  }

  setDarkTheme(): void {
    this.darkModeSignal.set(true);
  }

  setLightTheme(): void {
    this.darkModeSignal.set(false);
  }

  isDarkMode(): boolean {
    return this.darkModeSignal();
  }

  isLightMode(): boolean {
    const lightMode: Signal<boolean> = computed(() => !this.darkModeSignal());
    return lightMode();
  }

  toggleTranslit(): void {
    this.latinModeSignal.update(currentValue => !currentValue);
  }

  setLatin(): void {
    this.latinModeSignal.set(true);
  }

  setCyrillic(): void {
    this.latinModeSignal.set(false);
  }

  isLatinMode(): boolean {
    return this.latinModeSignal();
  }

  isCyrillicMode(): boolean {
    return !this.latinModeSignal();
  }
}
