import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export interface TagCoordinates extends Tag {
  left?: string,
  moveLeft?: boolean,
  top?: string,
  size?: string,
  color?: string | null,
}

export enum TagGroup {
  cities = 'cities',
  sports = 'sports',
  rulers = 'rulers',
  sceince = 'sceince',
  art = 'art',
  music = 'music',
  historic = 'historic',
  achievements = 'achievements',
  neighbours = 'neighbours',
}

export const tagGroups = [
  { id: TagGroup.rulers, name: 'Звања', color: 'var(--color-3)' },
  { id: TagGroup.achievements, name: 'Достигнућа', color: 'var(--color-6)' },
  { id: TagGroup.historic, name: 'Историја', color: 'var(--color-5)' },
  { id: TagGroup.music, name: 'Музика', color: 'var(--color-5)' },
  { id: TagGroup.art, name: 'Уметност', color: 'var(--color-4)' },
  { id: TagGroup.sports, name: 'Спортови', color: 'var(--color-2)' },
  { id: TagGroup.sceince, name: 'Научне области', color: 'var(--color-2)' },
  { id: TagGroup.neighbours, name: 'Суседне државе', color: 'var(--color-1)' },
  { id: TagGroup.cities, name: 'Градови и општине', color: 'var(--color-1)' },
];

export interface Tag {
  id?: string;
  name: string | null;
  description?: string | null;
  slug: string | null;
  group: TagGroup | null;
  color?: string | null;
  population?: string | null;
  cityDate?: string | null;
  postNumber?: string | null;
  callPrefix?: string | null;
  licencePlate?: string | null;
  height?: string | null;
  region?: string | null;
  trending?: boolean | null;
  imagePath?: string | null;
  area?: string | null;
  cityHistory?: string | null;
  cityGeography?: string | null;
  cityDemography?: string | null;
  cityCulture?: string | null;
  created: number | null;
  updated: number | null;
}

@Injectable({ providedIn: 'root' })
export class TagsService extends CrudService<Tag> {
  override collectionName = 'tags';
  coordinates: TagCoordinates[] = [];

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }

  setCityCoordinates(cityTags: Tag[], citySlug: string | null): TagCoordinates[] {
    if (!citySlug && this.coordinates?.length) {
      return this.coordinates;
    }

    const cityTagsCoordinates: TagCoordinates[] = [];

    cityTags.forEach(tag => {
      const x: TagCoordinates = { ...tag };
      let a = 1; //area
      let l = 0; //left
      let t = 0; //top
      let s = 1; //size

      switch (tag.slug) {
        case 'ada': s = 4; a = 2; l = 26; t = 9; break;
        case 'aleksandrovac': s = 4; a = 1; l = 50; t = 61; break;
        case 'aleksinac': s = 4; a = 3; l = 68; t = 61; break;
        case 'alibunar': s = 4; a = 2; l = 49; t = 24; break;
        case 'apatin': s = 4; a = 2; l = 5.5; t = 12; break;
        case 'arandjelovac': s = 4; a = 1; l = 39; t = 43; break;
        case 'arilje': s = 4; a = 1; l = 29; t = 57; break;
        case 'babusnica': s = 4; a = 3; l = 85; t = 72; break;
        case 'bac': s = 4; a = 2; l = 7; t = 18; break;
        case 'backa-palanka': s = 3; a = 2; l = 13; t = 21; break;
        case 'backa-topola': s = 4; a = 2; l = 18; t = 9; break;
        case 'backi-petrovac': s = 4; a = 2; l = 18; t = 20; break;
        case 'bajina-basta': s = 4; a = 1; l = 15; t = 52; break;
        case 'batocina': s = 4; a = 1; l = 52; t = 48; break;
        case 'becej': s = 4; a = 2; l = 26.5; t = 14; break;
        case 'bela-crkva': s = 4; a = 2; l = 58; t = 29; break;
        case 'bela-palanka': s = 4; a = 3; l = 80; t = 67; break;
        case 'beocin': s = 4; a = 2; l = 18; t = 22; break;
        case 'beograd': s = 0, a = 5; l = 36; t = 34; break;

        case 'obrenovac': s = 4, a = 5; l = 30; t = 36; break;
        case 'mladenovac': s = 4, a = 5; l = 43; t = 40; break;
        case 'zemun': s = 3, a = 5; l = 33; t = 30; break;
        case 'surcin': s = 4, a = 5; l = 32; t = 33; break;
        case 'grocka': s = 4, a = 5; l = 44; t = 36; break;
        case 'lazarevac': s = 4, a = 5; l = 35; t = 42; break;
        case 'palilula': s = 4, a = 5; l = 38; t = 29; break;

        case 'blace': s = 4; a = 3; l = 59; t = 66; break;
        case 'bogatic': s = 4; a = 1; l = 14; t = 31; break;
        case 'bojnik': s = 4; a = 3; l = 69; t = 73; break;
        case 'boljevac': s = 4; a = 3; l = 74; t = 54; break;
        case 'bor': s = 3; a = 3; l = 78; t = 47; break;
        case 'bosilegrad': s = 4; a = 3; l = 83; t = 85; break;
        case 'brus': s = 4; a = 1; l = 51; t = 65; break;
        case 'bujanovac': s = 4; a = 3; l = 70; t = 86; break;
        case 'cacak': s = 2; a = 1; l = 36; t = 53; break;
        case 'cajetina': s = 4; a = 1; l = 20; t = 57; break;
        case 'cicevac': s = 4; a = 1; l = 61; t = 57; break;
        case 'coka': s = 4; a = 2; l = 31; t = 7; break;
        case 'crna-trava': s = 4; a = 3; l = 84; t = 76; break;
        case 'cuprija': s = 4; a = 1; l = 61; t = 50; break;
        case 'decani': s = 4; a = 4; l = 33; t = 83; break;
        case 'despotovac': s = 4; a = 1; l = 63; t = 48; break;
        case 'dimitrovgrad': s = 4; a = 3; l = 95; t = 71; break;
        case 'djakovica': s = 4; a = 4; l = 36; t = 87; break;
        case 'doljevac': s = 4; a = 3; l = 71; t = 68; break;
        case 'gadzin-han': s = 4; a = 3; l = 77; t = 69; break;
        case 'glogovac': s = 4; a = 4; l = 48; t = 82; break;
        case 'gnjilane': s = 2; a = 4; l = 60; t = 84.5; break;
        case 'golubac': s = 4; a = 3; l = 67; t = 36; break;
        case 'gora': s = 4; a = 4; l = 43; t = 95; break;
        case 'gornji-milanovac': s = 4; a = 1; l = 36; t = 50; break;
        case 'indjija': s = 4; a = 2; l = 29; t = 24; break;
        case 'irig': s = 4; a = 2; l = 23; t = 24; break;
        case 'istok': s = 4; a = 4; l = 39; t = 78; break;
        case 'ivanjica': s = 4; a = 1; l = 33; t = 61; break;
        case 'jagodina': s = 3; a = 1; l = 56; t = 51; break;
        case 'kacanik': s = 4; a = 4; l = 57; t = 91; break;
        case 'kanjiza': s = 4; a = 2; l = 26; t = 2; break;
        case 'kikinda': s = 3; a = 2; l = 37; t = 9; break;
        case 'kladovo': s = 4; a = 3; l = 85; t = 36; break;
        case 'klina': s = 4; a = 4; l = 40; t = 83; break;
        case 'knic': s = 4; a = 1; l = 43; t = 52; break;
        case 'knjazevac': s = 4; a = 3; l = 81; t = 60; break;
        case 'koceljeva': s = 4; a = 1; l = 23; t = 40; break;
        case 'kosjeric': s = 4; a = 1; l = 23; t = 49; break;
        case 'kosovo-polje': s = 4; a = 4; l = 52; t = 81; break;
        case 'kosovska-kamenica': s = 4; a = 4; l = 65; t = 81; break;
        case 'kosovska-mitrovica': s = 2; a = 4; l = 47; t = 74; break;
        case 'kovacica': s = 4; a = 2; l = 43; t = 23; break;
        case 'kovin': s = 4; a = 2; l = 50; t = 31; break;
        case 'kragujevac': s = 1, a = 1; l = 48; t = 49; break;
        case 'kraljevo': s = 2; a = 1; l = 41.5; t = 56; break;
        case 'krupanj': s = 4; a = 1; l = 13; t = 40; break;
        case 'krusevac': s = 2; a = 1; l = 57; t = 60; break;
        case 'kucevo': s = 4; a = 3; l = 69; t = 40; break;
        case 'kula': s = 4; a = 2; l = 12; t = 12; break;
        case 'kursumlija': s = 4; a = 3; l = 60; t = 73; break;
        case 'lajkovac': s = 4; a = 1; l = 30; t = 43; break;
        case 'lapovo': s = 4; a = 1; l = 53; t = 47; break;
        case 'lebane': s = 4; a = 3; l = 69; t = 76; break;
        case 'leposavic': s = 4; a = 4; l = 47; t = 71; break;
        case 'leskovac': s = 2; a = 3; l = 72; t = 72; break;
        case 'lipljan': s = 3; a = 4; l = 52; t = 83; break;
        case 'ljig': s = 4; a = 1; l = 34; t = 46; break;
        case 'ljubovija': s = 4; a = 1; l = 13; t = 45; break;
        case 'loznica': s = 3; a = 1; l = 11; t = 36; break;
        case 'lucani': s = 4; a = 1; l = 34; t = 56; break;
        case 'majdanpek': s = 4; a = 3; l = 76; t = 41; break;
        case 'mali-idjos': s = 4; a = 2; l = 18.5; t = 11; break;
        case 'mali-zvornik': s = 4; a = 1; l = 8; t = 41; break;
        case 'malo-crnice': s = 4; a = 3; l = 57; t = 38; break;
        case 'medvedja': s = 4; a = 3; l = 63; t = 78; break;
        case 'merosina': s = 4; a = 3; l = 67; t = 66; break;
        case 'mionica': s = 4; a = 1; l = 30; t = 46; break;
        case 'negotin': s = 4; a = 3; l = 85; t = 44; break;
        case 'nis': s = 1, a = 3; l = 71; t = 64; break;
        case 'nova-crnja': s = 4; a = 2; l = 43; t = 11; break;
        case 'nova-varos': s = 4; a = 1; l = 23; t = 62; break;
        case 'novi-becej': s = 4; a = 2; l = 33; t = 14; break;
        case 'novi-knezevac': s = 4; a = 2; l = 32; t = 3; break;
        case 'novi-pazar': s = 2; a = 1; l = 38; t = 69.5; break;
        case 'novi-sad': s = 1, a = 2; l = 22; t = 19; break;
        case 'novo-brdo': s = 4; a = 4; l = 60; t = 82; break;
        case 'obilic': s = 4; a = 4; l = 52; t = 80; break;
        case 'odzaci': s = 4; a = 2; l = 9; t = 15; break;
        case 'opovo': s = 4; a = 2; l = 39, t = 24; break;
        case 'orahovac': s = 4; a = 4; l = 44; t = 85; break;
        case 'osecina': s = 4; a = 1; l = 17; t = 43; break;
        case 'pancevo': s = 2; a = 2; l = 45; t = 28; break;
        case 'paracin': s = 4; a = 1; l = 64; t = 53; break;
        case 'pec': s = 2; a = 4; l = 35; t = 80; break;
        case 'pecinci': s = 4; a = 2; l = 28; t = 31; break;
        case 'petrovac-na-mlavi': s = 4; a = 3; l = 62; t = 43; break;
        case 'pirot': s = 3; a = 3; l = 87; t = 66; break;
        case 'plandiste': s = 4; a = 2; l = 51; t = 22; break;
        case 'podujevo': s = 4; a = 4; l = 54; t = 75; break;
        case 'pozarevac': s = 3; a = 3; l = 55; t = 35; break;
        case 'pozega': s = 4; a = 1; l = 29; t = 53; break;
        case 'presevo': s = 4; a = 3; l = 68; t = 89; break;
        case 'priboj': s = 4; a = 1; l = 14; t = 62; break;
        case 'prijepolje': s = 4; a = 1; l = 22; t = 67; break;
        case 'pristina': s = 1, a = 4; l = 55; t = 79; break;
        case 'prizren': s = 2; a = 4; l = 44; t = 90; break;
        case 'prokuplje': s = 3; a = 1; a = 3; l = 62; t = 69; break;
        case 'raca': s = 4; a = 1; l = 49; t = 46; break;
        case 'raska': s = 4; a = 1; l = 42; t = 64; break;
        case 'razanj': s = 4; a = 3; l = 66; t = 56; break;
        case 'rekovac': s = 4; a = 1; l = 53; t = 53; break;
        case 'ruma': s = 3; a = 2; l = 23; t = 28; break;
        case 'sabac': s = 2; a = 1; l = 17; t = 35; break;
        case 'secanj': s = 4; a = 2; l = 45; t = 19; break;
        case 'senta': s = 4; a = 2; l = 26; t = 6; break;
        case 'sid': s = 4; a = 2; l = 8; t = 25; break;
        case 'sjenica': s = 4; a = 1; l = 30; t = 68; break;
        case 'smederevo': s = 3; a = 3; l = 49; t = 37; break;
        case 'smederevska-palanka': s = 4; a = 3; l = 49; t = 41; break;
        case 'sokobanja': s = 4; a = 3; l = 74; t = 58; break;
        case 'sombor': s = 3; a = 2; l = 7; t = 8; break;
        case 'srbica': s = 3; a = 4; l = 44; t = 79; break;
        case 'srbobran': s = 4; a = 2; l = 21; t = 15; break;
        case 'sremska-mitrovica': s = 3; a = 2; l = 16; t = 26; break;
        case 'sremski-karlovci': s = 4; a = 2; l = 25; t = 23; break;
        case 'stara-pazova': s = 3; a = 2; l = 31; t = 26; break;
        case 'stimlje': s = 4; a = 4; l = 51; t = 84.5; break;
        case 'strpce': s = 4; a = 4; l = 52; t = 90; break;
        case 'subotica': s = 2; a = 2; l = 16; t = 4; break;
        case 'surdulica': s = 4; a = 3; l = 81; t = 80; break;
        case 'suva-reka': s = 3; a = 4; l = 48; t = 86; break;
        case 'svilajnac': s = 4; a = 1; l = 57; t = 46; break;
        case 'svrljig': s = 4; a = 3; l = 76; t = 63; break;
        case 'temerin': s = 4; a = 2; l = 24; t = 17; break;
        case 'titel': s = 4; a = 2; l = 29; t = 20; break;
        case 'topola': s = 4; a = 1; l = 44; t = 45; break;
        case 'trgoviste': s = 4; a = 3; l = 77; t = 87; break;
        case 'trstenik': s = 4; a = 1; l = 51; t = 57; break;
        case 'tutin': s = 4; a = 1; l = 34; t = 72; break;
        case 'ub': s = 4; a = 1; l = 28; t = 40; break;
        case 'urosevac': s = 4; a = 4; l = 55; t = 86; break;
        case 'uzice': s = 3; a = 1; l = 23; t = 53; break;
        case 'valjevo': s = 3; a = 1; l = 23; t = 44; break;
        case 'varvarin': s = 4; a = 1; l = 57; t = 56; break;
        case 'velika-plana': s = 4; a = 3; l = 53; t = 44; break;
        case 'veliko-gradiste': s = 4; a = 3; l = 61; t = 34; break;
        case 'vitina': s = 3; a = 4; l = 60; t = 89; break;
        case 'vladicin-han': s = 4; a = 3; l = 78; t = 79; break;
        case 'vladimirci': s = 4; a = 1; l = 24; t = 37; break;
        case 'vlasotince': s = 4; a = 3; l = 79; t = 75; break;
        case 'vranje': s = 3; a = 3; l = 75; t = 83; break;
        case 'vrbas': s = 4; a = 2; l = 17; t = 16; break;
        case 'vrnjacka-banja': s = 4; a = 1; l = 47; t = 58; break;
        case 'vrsac': s = 3; a = 2; l = 54; t = 25; break;
        case 'vucitrn': s = 2; a = 4; l = 49; t = 78; break;
        case 'zabalj': s = 4; a = 2; l = 27; t = 18; break;
        case 'zabari': s = 4; a = 3; l = 56; t = 42; break;
        case 'zagubica': s = 4; a = 3; l = 68; t = 44; break;
        case 'zajecar': s = 3; a = 3; l = 80; t = 53; break;
        case 'zitiste': s = 4; a = 2; l = 41; t = 15; break;
        case 'zitoradja': s = 4; a = 3; l = 68; t = 70; break;
        case 'zrenjanin': s = 2; a = 2; l = 36; t = 18; break;
        case 'zubin-potok': s = 4; a = 4; l = 41; t = 75; break;
        case 'zvecan': s = 4; a = 4; l = 45; t = 73; break;
      }

      x.color = `var(--color-${a})`;
      x.left = `${l}%`;
      x.size = `size-${s}`;
      x.top = `${t}%`;
      x.moveLeft = l > 60;

      if (!citySlug || citySlug === tag.slug) {
        cityTagsCoordinates.push(x);
      }

      if (!citySlug) {
        this.coordinates = cityTagsCoordinates;
      }
    });

    return cityTagsCoordinates;
  }

  public cityList: any = [
    { slug: 'ada', name: 'Ада', },
    { slug: 'aleksandrovac', name: 'Александровац', },
    { slug: 'aleksinac', name: 'Алексинац', },
    { slug: 'alibunar', name: 'Алибунар', },
    { slug: 'apatin', name: 'Апатин', },
    { slug: 'arandjelovac', name: 'Аранђеловац', },
    { slug: 'arilje', name: 'Ариље', },
    { slug: 'babusnica', name: 'Бабушница', },
    { slug: 'bac', name: 'Бач', },
    { slug: 'backa-palanka', name: 'Бачка Паланка', },
    { slug: 'backa-topola', name: 'Бачка Топола', },
    { slug: 'backi-petrovac', name: 'Бачки Петровац', },
    { slug: 'bajina-basta', name: 'Бајина Башта', },
    { slug: 'batocina', name: 'Баточина', },
    { slug: 'becej', name: 'Бечеј', },
    { slug: 'bela-crkva', name: 'Бела Црква', },
    { slug: 'bela-palanka', name: 'Бела Паланка', },
    { slug: 'beocin', name: 'Беочин', },
    { slug: 'beograd', name: 'Београд' },
    { slug: 'obrenovac', name: 'Обреновац' },
    { slug: 'mladenovac', name: 'Младеновац' },
    { slug: 'zemun', name: 'Земун' },
    { slug: 'lazarevac', name: 'Лазаревац' },
    { slug: 'blace', name: 'Блаце' },
    { slug: 'bogatic', name: 'Богатић' },
    { slug: 'bojnik', name: 'Бојник' },
    { slug: 'bor', name: 'Бор' },
    { slug: 'bosilegrad', name: 'Босилеград' },
    { slug: 'brus', name: 'Брус' },
    { slug: 'bujanovac', name: 'Бујановац' },
    { slug: 'cacak', name: 'Чачак' },
    { slug: 'cajetina', name: 'Чајетина' },
    { slug: 'cicevac', name: 'Ћићевац' },
    { slug: 'coka', name: 'Чока' },
    { slug: 'crna-trava', name: 'Црна Трава' },
    { slug: 'cuprija', name: 'Ћуприја' },
    { slug: 'decani', name: 'Дечани' },
    { slug: 'despotovac', name: 'Деспотовац' },
    { slug: 'dimitrovgrad', name: 'Димитровград' },
    { slug: 'djakovica', name: 'Ђаковица' },
    { slug: 'doljevac', name: 'Дољевац' },
    { slug: 'gadzin-han', name: 'Гаџин Хан' },
    { slug: 'glogovac', name: 'Глоговац' },
    { slug: 'gnjilane', name: 'Гњилане' },
    { slug: 'golubac', name: 'Голубац' },
    { slug: 'gora', name: 'Гора' },
    { slug: 'gornji-milanovac', name: 'Горњи Милановац' },
    { slug: 'indjija', name: 'Инђија' },
    { slug: 'irig', name: 'Ириг' },
    { slug: 'istok', name: 'Исток' },
    { slug: 'ivanjica', name: 'Ивањица' },
    { slug: 'jagodina', name: 'Јагодина' },
    { slug: 'kacanik', name: 'Качаник' },
    { slug: 'kanjiza', name: 'Кањижа' },
    { slug: 'kikinda', name: 'Кикинда' },
    { slug: 'kladovo', name: 'Кладово' },
    { slug: 'klina', name: 'Клина' },
    { slug: 'knic', name: 'Кнић' },
    { slug: 'knjazevac', name: 'Књажевац' },
    { slug: 'koceljeva', name: 'Коцељева' },
    { slug: 'kosjeric', name: 'Косјерић' },
    { slug: 'kosovo-polje', name: 'Косово Поље' },
    { slug: 'kosovska-kamenica', name: 'Косовска Каменица' },
    { slug: 'kosovska-mitrovica', name: 'Косовска Митровица' },
    { slug: 'kovacica', name: 'Ковачица' },
    { slug: 'kovin', name: 'Ковин' },
    { slug: 'kragujevac', name: 'Крагујевац' },
    { slug: 'kraljevo', name: 'Краљево' },
    { slug: 'krupanj', name: 'Крупањ' },
    { slug: 'krusevac', name: 'Крушевац' },
    { slug: 'kucevo', name: 'Кучево' },
    { slug: 'kula', name: 'Кула' },
    { slug: 'kursumlija', name: 'Куршумлија' },
    { slug: 'lajkovac', name: 'Лајковац' },
    { slug: 'lapovo', name: 'Лапово' },
    { slug: 'lebane', name: 'Лебане' },
    { slug: 'leposavic', name: 'Лепосавић' },
    { slug: 'leskovac', name: 'Лесковац' },
    { slug: 'lipljan', name: 'Липљан' },
    { slug: 'ljig', name: 'Љиг' },
    { slug: 'ljubovija', name: 'Љубовија' },
    { slug: 'loznica', name: 'Лозница' },
    { slug: 'lucani', name: 'Лучани' },
    { slug: 'majdanpek', name: 'Мајданпек' },
    { slug: 'mali-idjos', name: 'Мали Иђош' },
    { slug: 'mali-zvornik', name: 'Мали Зворник' },
    { slug: 'malo-crnice', name: 'Мало Црниће' },
    { slug: 'medvedja', name: 'Медвеђа' },
    { slug: 'merosina', name: 'Мерошина' },
    { slug: 'mionica', name: 'Мионица' },
    { slug: 'negotin', name: 'Неготин' },
    { slug: 'nis', name: 'Ниш' },
    { slug: 'nova-crnja', name: 'Нова Црња' },
    { slug: 'nova-varos', name: 'Нова Варош' },
    { slug: 'novi-becej', name: 'Нови Бечеј' },
    { slug: 'novi-knezevac', name: 'Нови Кнежевац' },
    { slug: 'novi-pazar', name: 'Нови Пазар' },
    { slug: 'novi-sad', name: 'Нови Сад' },
    { slug: 'novo-brdo', name: 'Ново Брдо' },
    { slug: 'obilic', name: 'Обилић' },
    { slug: 'odzaci', name: 'Оџаци' },
    { slug: 'opovo', name: 'Опово' },
    { slug: 'orahovac', name: 'Ораховац' },
    { slug: 'osecina', name: 'Осечина' },
    { slug: 'pancevo', name: 'Панчево' },
    { slug: 'paracin', name: 'Параћин' },
    { slug: 'pec', name: 'Пећ' },
    { slug: 'pecinci', name: 'Пећинци' },
    { slug: 'petrovac-na-mlavi', name: 'Петровац на Млави' },
    { slug: 'pirot', name: 'Пирот' },
    { slug: 'plandiste', name: 'Пландиште' },
    { slug: 'podujevo', name: 'Подујево' },
    { slug: 'pozarevac', name: 'Пожаревац' },
    { slug: 'pozega', name: 'Пожега' },
    { slug: 'presevo', name: 'Прешево' },
    { slug: 'priboj', name: 'Прибој' },
    { slug: 'prijepolje', name: 'Пријепоље' },
    { slug: 'pristina', name: 'Приштина' },
    { slug: 'prizren', name: 'Призрен' },
    { slug: 'prokuplje', name: 'Прокупље' },
    { slug: 'raca', name: 'Рача' },
    { slug: 'raska', name: 'Рашка' },
    { slug: 'razanj', name: 'Ражањ' },
    { slug: 'rekovac', name: 'Рековац' },
    { slug: 'ruma', name: 'Рума' },
    { slug: 'sabac', name: 'Шабац' },
    { slug: 'secanj', name: 'Сечањ' },
    { slug: 'senta', name: 'Сента' },
    { slug: 'sid', name: 'Шид' },
    { slug: 'sjenica', name: 'Сјеница' },
    { slug: 'smederevo', name: 'Смедерево' },
    { slug: 'smederevska-palanka', name: 'Смедеревска Паланка' },
    { slug: 'sokobanja', name: 'Сокобања' },
    { slug: 'sombor', name: 'Сомбор' },
    { slug: 'srbica', name: 'Србица' },
    { slug: 'srbobran', name: 'Србобран' },
    { slug: 'sremska-mitrovica', name: 'Сремска Митровица' },
    { slug: 'sremski-karlovci', name: 'Сремски Карловци' },
    { slug: 'stara-pazova', name: 'Стара Пазова' },
    { slug: 'stimlje', name: 'Штимље' },
    { slug: 'strpce', name: 'Штрпце' },
    { slug: 'subotica', name: 'Суботица' },
    { slug: 'surdulica', name: 'Сурдулица' },
    { slug: 'suva-reka', name: 'Сува Река' },
    { slug: 'svilajnac', name: 'Свилајнац' },
    { slug: 'svrljig', name: 'Сврљиг' },
    { slug: 'temerin', name: 'Темерин' },
    { slug: 'titel', name: 'Тител' },
    { slug: 'topola', name: 'Топола' },
    { slug: 'trgoviste', name: 'Трговиште' },
    { slug: 'trstenik', name: 'Трстеник' },
    { slug: 'tutin', name: 'Тутин' },
    { slug: 'ub', name: 'Уб' },
    { slug: 'urosevac', name: 'Урошевац' },
    { slug: 'uzice', name: 'Ужице' },
    { slug: 'valjevo', name: 'Ваљево' },
    { slug: 'varvarin', name: 'Варварин' },
    { slug: 'velika-plana', name: 'Велика Плана' },
    { slug: 'veliko-gradiste', name: 'Велико Градиште' },
    { slug: 'vitina', name: 'Витина' },
    { slug: 'vladicin-han', name: 'Владичин Хан' },
    { slug: 'vladimirci', name: 'Владимирци' },
    { slug: 'vlasotince', name: 'Власотинце' },
    { slug: 'vranje', name: 'Врање' },
    { slug: 'vrbas', name: 'Врбас' },
    { slug: 'vrnjacka-banja', name: 'Врњачка Бања' },
    { slug: 'vrsac', name: 'Вршац' },
    { slug: 'vucitrn', name: 'Вучитрн' },
    { slug: 'zabalj', name: 'Жабаљ' },
    { slug: 'zabari', name: 'Жабари' },
    { slug: 'zagubica', name: 'Жагубица' },
    { slug: 'zajecar', name: 'Зајечар' },
    { slug: 'zitiste', name: 'Житиште' },
    { slug: 'zitoradja', name: 'Житорађа' },
    { slug: 'zrenjanin', name: 'Зрењанин' },
    { slug: 'zubin-potok', name: 'Зубин Поток' },
    { slug: 'zvecan', name: 'Звечан' },
  ];
}
