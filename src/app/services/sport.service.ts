import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export enum SportSuccessType {
  individual = 'Индивидуални успех',
  club = 'Клупски успех',
  nationalTeam = 'Репрезентативни успех'
}

export const sportSuccessTypes = [
  { 'id': 'individual', 'name': SportSuccessType.individual },
  { 'id': 'club', 'name': SportSuccessType.club },
  { 'id': 'nationalTeam', 'name': SportSuccessType.nationalTeam },
];

export interface SportSuccess {
  id?: string | null;
  name: string | null;
  slug: string | null;
  creator: string | null;
  creatorSlug: string | null;
  yearMonth: string | null;
  dates: string | null;
  type: SportSuccessType | null;
  created: number | null;
  updated: number | null;
}

@Injectable({ providedIn: 'root' })
export class SportService extends CrudService<SportSuccess> {
  override collectionName = 'sports';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}