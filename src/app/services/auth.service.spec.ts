import { TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';

import { AuthService } from './auth.service';
import { UserProfileService } from './user-profile.service';

const authMock = {};
const userMock = {};

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Auth, useValue: authMock },
        { provide: UserProfileService, useValue: userMock },
      ]
    });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
