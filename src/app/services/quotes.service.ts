import { Injectable } from '@angular/core';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';
import { CrudService } from '@services/firestore-crud.service';

export enum QuoteType {
  patriot = 'patriot',
  religious = 'religious',
  love = 'love',
  success = 'success',
  philosophical = 'philosophical',
  sport = 'sport',
}

export interface Quote {
  id?: string;
  text: string | null;
  slug: string | null;
  type: QuoteType | null;
  authorSlug: string | null;
  authorName: string | null;
  important: boolean | null;
  created: number | null;
  updated: number | null;
}

export const quoteTypes: any[] = [
  { id: QuoteType.patriot, name: 'Патриотски цитати' },
  { id: QuoteType.religious, name: 'Духовни цитати' },
  { id: QuoteType.love, name: 'Љубавни цитати' },
  { id: QuoteType.success, name: 'Цитати о успеху' },
  { id: QuoteType.philosophical, name: 'Филозофски цитати' },
  { id: QuoteType.sport, name: 'Шаљиви цитати' },
];


export interface QuoteGame extends Quote {
  correctOrder: boolean;
}

@Injectable({ providedIn: 'root' })
export class QuotesService extends CrudService<Quote> {
  override collectionName = 'quotes';

  constructor(firestore: Firestore, storage: Storage) {
    super(firestore, storage);
  }
}
