import { TestBed } from '@angular/core/testing';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ChurchService } from './church.service';

const firestoreMock = {};
const storageMock = {};

describe('ChurchService', () => {
  let service: ChurchService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: Firestore, useValue: firestoreMock },
        { provide: Storage, useValue: storageMock },
      ]
    });
    service = TestBed.inject(ChurchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
