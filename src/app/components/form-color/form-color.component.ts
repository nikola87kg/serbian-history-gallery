import { ColorPickerModule } from 'ngx-color-picker';

import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

'@angular/core';
@Component({
    selector: 'app-form-color',
    imports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        ColorPickerModule
    ],
    templateUrl: './form-color.component.html',
    styleUrls: ['./form-color.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormColorComponent {
  @Input({ required: true }) control!: string;
  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public form!: FormGroup;
  public color = '#ffffff';


  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
    this.form?.get(this.control)?.setValue(this.color);
  }

  setColor() {
    this.form.get(this.control)?.setValue(this.color);
  }
}
