import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroupDirective } from '@angular/forms';

import { FormColorComponent } from './form-color.component';

describe('FormColorComponent', () => {
  let component: FormColorComponent;
  let fixture: ComponentFixture<FormColorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormColorComponent],
      providers: [
        { provide: FormGroupDirective, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(FormColorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
