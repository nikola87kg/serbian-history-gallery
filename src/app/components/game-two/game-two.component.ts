import { CdkDragDrop, DragDropModule, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { DefaultLogoDirective } from '@app/directives/default-logo.directive';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { Person, PersonGame } from '@services/persons.service';
import { UtilsService } from '@services/utils.service';

function shuffleAndSlice(list: Person[]) {
  UtilsService.shuffleListOrder(list);
  const nameList = list.slice(0, 5);
  const imageList = [...nameList];
  UtilsService.shuffleListOrder(imageList);
  return [nameList, imageList];
}

@Component({
  selector: 'app-game-two',
  imports: [DragDropModule, MatButtonModule, TranslitPipe, DefaultLogoDirective],
  templateUrl: './game-two.component.html',
  styleUrls: ['./game-two.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameTwoComponent {
  @Input({ transform: shuffleAndSlice }) public list: PersonGame[][] = [[], []];
  @Output() public gameTwoEmitter = new EventEmitter<number>();

  get personList() {
    return this.list[0];
  }

  get imageList() {
    return this.list[1];
  }

  public gamePoints = 0;

  onDropGame2(event: CdkDragDrop<Person[]>) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }

  checkGameTwo() {
    this.personList.forEach((item, index) => {
      const image = this.imageList[index];

      item.correctOrder = image.id === item.id;
      if (item.correctOrder) {
        this.gamePoints += 4;
      }
    });

    this.gamePoints = this.gamePoints || 1;
    this.handleStorage();

  }

  onNext() {
    this.list = [[], []];
    this.gameTwoEmitter.emit(this.gamePoints);
  }

  handleStorage() {
    const today = UtilsService.getTodayFormatted();
    const storageName = `quiz-${today}`;
    const storageQuiz = localStorage.getItem(storageName);
    let points = [];
    if (storageQuiz) {
      points = JSON.parse(storageQuiz);
    }

    points[1] = this.gamePoints;
    localStorage.setItem(storageName, JSON.stringify(points));
  }
}
