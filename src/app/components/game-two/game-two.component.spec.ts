import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTwoComponent } from './game-two.component';

describe('GameTwoComponent', () => {
  let component: GameTwoComponent;
  let fixture: ComponentFixture<GameTwoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GameTwoComponent]
    });
    fixture = TestBed.createComponent(GameTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
