
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, inject, Input
} from '@angular/core';
import { TranslitPipe } from '@app/pipes/translit.pipe';

@Component({
    selector: 'app-game-results',
    imports: [TranslitPipe],
    templateUrl: './game-results.component.html',
    styleUrls: ['./game-results.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameResultsComponent {
  @Input({ required: true }) points: number[] = [];

  private cdr = inject(ChangeDetectorRef);
  private today = new Date();
  private options: any = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

  public totalScore = 0;
  public date = this.today.toLocaleDateString('sr', this.options as any);

  ngOnInit() {
    setTimeout(() => {
      this.calculateTotalScore();
    }, 100);
  }

  calculateTotalScore() {
    this.totalScore = this.points.reduce((a, b) => a + b);
    this.cdr.detectChanges();
  }


}
