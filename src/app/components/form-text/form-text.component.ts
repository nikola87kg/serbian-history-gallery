import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, HostListener, inject, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-form-text',
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatIconModule,
  ],
  templateUrl: './form-text.component.html',
  styleUrls: ['./form-text.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormTextComponent {
  @Input({ required: true }) label!: string;
  @Input({ required: true }) control!: string;
  @Input() wider = false;
  @Input() slug = false;
  @Input() spaceCheck = false;
  @Input() ignoreErrorHint = false;
  @Input() paster = false;

  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public showHint = false;
  public textHint = '';

  public form!: FormGroup;

  get doubleSpaceError() {
    return this.form.controls[this.control].hasError('doublespace');
  }

  ngOnInit(): void {
    UtilsService.scrollTop();
    this.form = this.rootFormGroup.form as FormGroup;
    this.listenChanges();
    this.checkHints();
  }

  fixWikiDate(birthDeath: string): string {
    let fragments = birthDeath.split(' ');
    fragments = fragments.filter(f => !f.includes(','));
    birthDeath = fragments.join(' ');

    birthDeath = birthDeath.split(' — ').join(' - ');
    birthDeath = birthDeath.split(' Јануар ').join('1.');
    birthDeath = birthDeath.split(' јануар ').join('1.');
    birthDeath = birthDeath.split(' Фебруар ').join('2.');
    birthDeath = birthDeath.split(' фебруар ').join('2.');
    birthDeath = birthDeath.split(' Март ').join('3.');
    birthDeath = birthDeath.split(' март ').join('3.');
    birthDeath = birthDeath.split(' Април ').join('4.');
    birthDeath = birthDeath.split(' април ').join('4.');
    birthDeath = birthDeath.split(' Мај ').join('5.');
    birthDeath = birthDeath.split(' мај ').join('5.');
    birthDeath = birthDeath.split(' Јун ').join('6.');
    birthDeath = birthDeath.split(' јун ').join('6.');
    birthDeath = birthDeath.split(' Јул ').join('7.');
    birthDeath = birthDeath.split(' јул ').join('7.');
    birthDeath = birthDeath.split(' Август ').join('8.');
    birthDeath = birthDeath.split(' август ').join('8.');
    birthDeath = birthDeath.split(' Септембар ').join('9.');
    birthDeath = birthDeath.split(' септембар ').join('9.');
    birthDeath = birthDeath.split(' Октобар ').join('10.');
    birthDeath = birthDeath.split(' октобар ').join('10.');
    birthDeath = birthDeath.split(' Новембар ').join('11.');
    birthDeath = birthDeath.split(' новембар ').join('11.');
    birthDeath = birthDeath.split(' Децембар ').join('12.');
    birthDeath = birthDeath.split(' децембар ').join('12.');
    birthDeath = birthDeath.split('(').join('');
    birthDeath = birthDeath.split(')').join('');
    birthDeath.trimStart();
    return birthDeath;
  }

  @HostListener('window:keydown.alt.А', ['$event'])
  fillYears(event: KeyboardEvent) {
    event.preventDefault();
    const birthDeath = this.form.controls['birthDeath']?.value.trim();
    const rawDate = this.fixWikiDate(birthDeath);
    if (rawDate) {
      const aboutSplitter: any[] = rawDate.split('око ') || [];
      const splitter = aboutSplitter.at(-1).split(' - ');
      const bornDate = splitter[0];
      if (bornDate) {
        const bornSplitter = bornDate.split('.');
        if (bornSplitter?.length > 1) {
          this.form.controls['birthDay'].setValue(+bornSplitter[0]);
          this.form.controls['birthMonth'].setValue(+bornSplitter[1]);
          this.form.controls['birthYear'].setValue(+bornSplitter[2]);

          const dieDate = splitter[1];
          if (dieDate) {
            const dieSplitter = dieDate.split('.');
            let ageDifference = Number(dieSplitter.at(-1)) - Number(bornSplitter[2]);
            if (dieDate && !isNaN(ageDifference)) {
              const dieMonth = dieSplitter.at(-2);
              if (dieMonth && bornSplitter[1]) {
                const monthDiff = (Number(dieMonth) - Number(bornSplitter[1]));
                if (monthDiff < 0) {
                  ageDifference--;
                } else if (monthDiff === 0) {
                  const dieDay = dieSplitter.at(-3);
                  const dayDiff = (Number(dieDay) - Number(bornSplitter[0]));
                  if (dayDiff < 0) {
                    ageDifference--;
                  }
                }

              }
              this.form.controls['age'].setValue(+ageDifference);
            }
          }
        } else {
          const bornYear = splitter.at(0);
          this.form.controls['birthYear'].setValue(+bornYear);

          const dieDate = splitter[1];

          if (dieDate) {
            const dieSplitter = dieDate.split('.');
            const ageDifference = Number(dieSplitter.at(-1)) - Number(bornYear);
            if (dieDate && !isNaN(ageDifference)) {
              this.form.controls['age'].setValue(+ageDifference);
            }
          }
        }
      }
    }

    setTimeout(() => {
      this.form.controls['birthDeath']?.setValue(rawDate);
    }, 200);
  }

  onPaste(event: any) {
    const name = this.getField();

    if (name === 'firstName') {
      this.onNameAutocomplete(event);
    } else if (name === 'shortDescription') {
      setTimeout(() => {
        this.onSpaceCheck();
      }, 200);
    }

    if (name === 'text') {
      const textControl = this.form.controls['text'];
      const pastedText = event.clipboardData?.getData('text');
      const cyrillicText = UtilsService.latinToCyrillic(pastedText!);

      setTimeout(() => {
        textControl.reset();
        textControl.setValue(cyrillicText);
      }, 100);
    }

    if (name === 'text') {
      const textControl = this.form.controls['text'];
      const pastedText = event.clipboardData?.getData('text');
      const cyrillicText = UtilsService.latinToCyrillic(pastedText!);

      setTimeout(() => {
        textControl.reset();
        textControl.setValue(cyrillicText);
      }, 100);
    }
  }

  onNameAutocomplete(e: any) {
    const clipboardData = e.clipboardData;
    const fullName = clipboardData.getData('Text');

    const splitted = fullName.split(' ');
    const firstName = splitted[0];
    const lastName = splitted[1];

    if (lastName) {
      this.form.controls['lastName'].setValue(lastName);
      this.form.controls['fullName'].setValue(fullName);
      this.form.controls['commonName'].setValue(fullName);
      this.onConvert();
    }

    if (firstName) {
      setTimeout(() => {
        this.form.controls['firstName'].reset();
        this.form.controls['firstName'].setValue(firstName);
      }, 100);
    }
  }

  onConvert() {
    const fullName = this.form.controls['fullName']?.value?.trim();
    const title = this.form.controls['title']?.value?.trim();
    const firstName = this.form.controls['firstName']?.value?.trim();
    const lastName = this.form.controls['lastName']?.value?.trim();
    const merged = `${firstName} ${lastName}`;
    const name = this.form.controls['name']?.value?.trim();
    const authorName = this.form.controls['authorName']?.value?.trim();
    const slug = UtilsService.slugify(fullName || name || authorName || title || merged);

    const slugControl = this.form.controls['slug'];

    const authorSlugControl = this.form.controls['authorSlug'];
    if (authorSlugControl) {
      authorSlugControl.setValue(slug);
      if (!title) {
        const randomNumber = Math.floor(Math.random() * 900) + 100;
        const slugRandomize = `${slug}-${randomNumber}`;
        slugControl.setValue(slugRandomize);
      } else {
        const titleSlug = UtilsService.slugify(title);
        const slugify = `${slug}-${titleSlug}`;
        slugControl.setValue(slugify);
      }
    } else {
      slugControl.setValue(slug);
    }
  }

  onSpaceCheck() {
    const control = this.form.controls[this.control];
    const controlValue = control?.value || '';
    if (controlValue.includes('  ')) {
      let inputValue = controlValue.split('   ').join(' ');
      inputValue = controlValue.split('  ').join(' ');
      inputValue = inputValue.trim();
      control.setValue(inputValue);
    }
  }

  checkHints() {
    const name = this.getField();

    if (name === 'fullTitle') {
      const storage = localStorage.getItem('last-fulltitle');
      this.showHint = true;

      if (storage) {
        this.textHint = storage;
      }
    }

    if (name === 'place') {
      const storage = localStorage.getItem('last-place');
      this.showHint = true;

      if (storage) {
        this.textHint = storage;
      }
    }

    if (name === 'authorName') {
      const storage = localStorage.getItem('last-author');
      this.showHint = true;

      if (storage) {
        this.textHint = storage;
      }
    }
  }
  onRepeatItem(text: string) {
    const formGroup = this.form?.controls;
    const control = formGroup[this.control];
    control.setValue(text);

    const name = this.getField();
    if (name === 'authorName') {
      this.onConvert();
    }
  }

  listenChanges() {
    this.form?.controls[this.control].valueChanges.subscribe({
      next: (text) => {
        const name = this.getField();

        if (text && name === 'fullTitle') {
          localStorage.setItem('last-fulltitle', text);
        }

        if (text && name === 'place') {
          localStorage.setItem('last-place', text);
        }

        if (text && name === 'authorName') {
          localStorage.setItem('last-author', text);
        }
      }
    });
  }

  getField() {
    const formGroup = this.form.controls;
    const c = formGroup[this.control];
    return Object.keys(formGroup).find(name => c === formGroup[name]) || null;
  }
}
