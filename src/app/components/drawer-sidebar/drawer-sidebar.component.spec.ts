import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { DrawerSidebarComponent } from './drawer-sidebar.component';

describe('DrawerSidebarComponent', () => {
  let component: DrawerSidebarComponent;
  let fixture: ComponentFixture<DrawerSidebarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DrawerSidebarComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(DrawerSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
