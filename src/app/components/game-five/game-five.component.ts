
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { NumbersOnlyDirective } from '@directives/numbers-only.directive';
import { Chronology, ChronologyGame } from '@services/chronology.service';
import { UtilsService } from '@services/utils.service';

function shuffleAndSlice(list: Chronology[]) {
  UtilsService.shuffleListOrder(list);
  const shortlist = list.splice(0, 4);
  return shortlist;
}
@Component({
    selector: 'app-game-five',
    imports: [MatFormFieldModule, MatInputModule, NumbersOnlyDirective, ReactiveFormsModule, FormsModule, MatButtonModule, TranslitPipe],
    templateUrl: './game-five.component.html',
    styleUrls: ['./game-five.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameFiveComponent {
  @Input({ transform: shuffleAndSlice }) public list: ChronologyGame[] = [];
  @Output() public gameFiveEmitter = new EventEmitter<number>();

  public result: number[] | undefined[] = [undefined, undefined, undefined, undefined];
  public gamePoints = 0;
  public correctOrderValues: number[] = [];

  checkGameFive() {
    this.list.forEach((item, index) => {
      const userAnswer = this.result[index] || 0;
      const age = item.age || 0;
      item.correctOrder = item.age === userAnswer;
      item.failDistance = Math.abs(age - userAnswer);
      item.gamePoints = 0;

      this.correctOrderValues[index] = age;

      if (item.failDistance === 0) {
        this.gamePoints += 5;
        item.gamePoints = 5;
      } else if (item.failDistance <= 10) {
        this.gamePoints += 4;
        item.gamePoints = 4;
      } else if (item.failDistance <= 30) {
        this.gamePoints += 3;
        item.gamePoints = 3;
      } else if (item.failDistance <= 50) {
        this.gamePoints += 2;
        item.gamePoints = 2;
      } else if (item.failDistance <= 100) {
        this.gamePoints += 1;
        item.gamePoints = 1;
      }
    });

    this.gamePoints = this.gamePoints || 1;
    this.handleStorage();
  }

  onNext() {
    this.list = [];
    this.gameFiveEmitter.emit(this.gamePoints);
  }

  handleStorage() {
    const today = UtilsService.getTodayFormatted();
    const storageName = `quiz-${today}`;
    const storageQuiz = localStorage.getItem(storageName);
    let points = [];
    if (storageQuiz) {
      points = JSON.parse(storageQuiz);
    }

    points[4] = this.gamePoints;
    localStorage.setItem(storageName, JSON.stringify(points));
  }
}
