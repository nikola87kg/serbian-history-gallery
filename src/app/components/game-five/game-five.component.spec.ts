import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameFiveComponent } from './game-five.component';

describe('GameFiveComponent', () => {
  let component: GameFiveComponent;
  let fixture: ComponentFixture<GameFiveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GameFiveComponent]
    });
    fixture = TestBed.createComponent(GameFiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
