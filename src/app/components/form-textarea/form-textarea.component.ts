
import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@Component({
    selector: 'app-form-textarea',
    imports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule
    ],
    templateUrl: './form-textarea.component.html',
    styleUrls: ['./form-textarea.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormTextareaComponent {
  @Input({ required: true }) label!: string;
  @Input({ required: true }) control!: string;
  @Input() ignoreErrorHint = false;
  @Input() fit = false;
  @Input() rows = 20;
  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public form!: FormGroup;

  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
  }
}
