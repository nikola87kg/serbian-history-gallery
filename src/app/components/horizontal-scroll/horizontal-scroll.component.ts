import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { ActivatedRoute } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';

@Component({
  selector: 'app-horizontal-scroll',
  imports: [CommonModule, MatIconModule, TranslitPipe],
  templateUrl: './horizontal-scroll.component.html',
  styleUrl: './horizontal-scroll.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HorizontalScrollComponent {
  @Input() method: Function | undefined;
  @Input() subtitle: string = '';
  @Input() mainTitle: string = '';
  @Input() textOnly = false;
  @Output() clickEmitter = new EventEmitter<string>();
  private activatedRoute = inject(ActivatedRoute);
  private cdr = inject(ChangeDetectorRef);
  public showScroll = false;
  public name!: string;

  constructor() {
    this.name = 'class-' + String(Math.floor(Math.random() * 1000000));
    this.resetScrolling();
  }

  ngOnInit(): void {
    // TODO
    this.activatedRoute.params.subscribe(_ => {
      this.name = 'class-' + String(Math.floor(Math.random() * 1000000));
      this.resetScrolling();
    });
  }

  resetScrolling() {
    this.showScroll = false;
    setTimeout(() => {
      this.initScrolling();
    }, 100);
  }

  initScrolling() {
    this.showScroll = true;
    this.cdr.detectChanges();
    const selector = `.scroll-container.${this.name}`;
    const slider = document.querySelector(selector) as any;

    if (!slider) {
      return;
    }

    slider.scrollLeft = 0;
    let isDown = false;
    let startX: any;
    let scrollLeft: any;

    slider.addEventListener('mousedown', (e: any) => {
      isDown = true;
      slider.classList.add('active');
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e: any) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 2; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  }

}
