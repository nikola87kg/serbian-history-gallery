import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject, Input } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@app/services/auth.service';
import { DefaultLogoDirective } from '@directives/default-logo.directive';
import { Person } from '@services/persons.service';

@Component({
  selector: 'app-person-mini-card',
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    DefaultLogoDirective,
    TranslitPipe,
  ],
  templateUrl: './person-mini-card.component.html',
  styleUrls: ['./person-mini-card.component.scss'],
  providers: [MatSnackBar, MatBottomSheet],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonMiniCardComponent {
  @Input({ required: true }) card!: Person;
  @Input() smaller = false;

  private router = inject(Router);
  private destroyRef = inject(DestroyRef);
  private isAdmin = false;

  public authService = inject(AuthService);

  get longLastname() {
    if (!this.card.lastName) {
      return false;
    }

    return this.card.lastName?.length > 10;
  }

  ngOnInit(): void {
    this.handleAdmin();
  }

  handleAdmin() {
    this.authService.userProfile$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe(user => {
        this.isAdmin = !!user?.isAdmin;

      });
  }

  onBiography(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onContextMenu($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();
    if (this.isAdmin) {
      this.router.navigateByUrl(`admin/panel/person-form/${this.card.id}`);
    }
  }

}
