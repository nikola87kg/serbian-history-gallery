import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { PersonMiniCardComponent } from './person-mini-card.component';

describe('PersonMiniCardComponent', () => {
  let component: PersonMiniCardComponent;
  let fixture: ComponentFixture<PersonMiniCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [PersonMiniCardComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(PersonMiniCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
