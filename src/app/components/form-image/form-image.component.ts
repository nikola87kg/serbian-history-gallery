import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, inject, Input, Output
} from '@angular/core';
import { AbstractControl, FormGroup, FormGroupDirective } from '@angular/forms';
import { DefaultLogoDirective } from '@directives/default-logo.directive';

@Component({
    selector: 'app-form-image',
    imports: [DefaultLogoDirective],
    templateUrl: './form-image.component.html',
    styleUrls: ['./form-image.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormImageComponent {
  @Input({ required: true }) control!: string;
  @Output() clickEmitter = new EventEmitter<void>();
  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public form!: FormGroup;
  private cdr = inject(ChangeDetectorRef);

  get formControl(): AbstractControl | null {
    return this.form.get(this.control);
  }

  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
    this.cdr.detectChanges();
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.cdr.detectChanges();
    }, 500);
  }

  onImage() {
    this.clickEmitter.emit();
  }
}
