import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyEventCardComponent } from './daily-event-card.component';

describe('DailyEventCardComponent', () => {
  let component: DailyEventCardComponent;
  let fixture: ComponentFixture<DailyEventCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DailyEventCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DailyEventCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
