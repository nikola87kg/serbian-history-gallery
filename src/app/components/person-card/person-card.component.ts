import { Clipboard } from '@angular/cdk/clipboard';
import { NoopScrollStrategy, ScrollStrategyOptions } from '@angular/cdk/overlay';
import { Location, ViewportScroller } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject, input } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialog } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ImageDialogComponent } from '@app/dialogs/image-dialog/image-dialog.component';
import {
  PersonDescriptionSheetComponent
} from '@app/dialogs/person-description-sheet/person-description-sheet.component';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { DefaultLogoDirective } from '@directives/default-logo.directive';
import { IdToSlugPipe } from '@pipes/id-to-slug.pipe';
import { AuthService } from '@services/auth.service';
import { CategoriesService, Category } from '@services/categories.service';
import { GlossaryService } from '@services/glossary.service';
import { Person, PersonsService } from '@services/persons.service';
import { UserProfile, UserProfileService } from '@services/user-profile.service';
import { UtilsService } from '@services/utils.service';

@Component({
  selector: 'app-person-card',
  imports: [
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    DefaultLogoDirective,
    TranslitPipe
  ],
  templateUrl: './person-card.component.html',
  styleUrls: ['./person-card.component.scss'],
  providers: [MatSnackBar, MatBottomSheet, MatIconModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonCardComponent {
  readonly card = input.required<Person>();

  private service = inject(PersonsService);
  private userProfileService = inject(UserProfileService);
  private categoriesService = inject(CategoriesService);
  private router = inject(Router);
  private idToSlugPipe: IdToSlugPipe = inject(IdToSlugPipe);
  private destroyRef = inject(DestroyRef);
  private userProfile!: UserProfile | null;
  private location = inject(Location);
  private clipboard = inject(Clipboard);
  private matBottomSheet = inject(MatBottomSheet);
  private matDialog = inject(MatDialog);
  private scroll = inject(ScrollStrategyOptions);
  private isAdmin = false;
  private scroller = inject(ViewportScroller);

  public Glossary = GlossaryService.Glossary;
  public authService = inject(AuthService);
  public categories: Category[] = [];
  public personCategory!: Category;
  public favouritedByUser: boolean | null = null;
  public hover = false;

  ngOnInit(): void {
    this.handleCategories();
    this.handleFavourites();
  }

  handleCategories(): void {
    this.categoriesService.getItemsSorted('name')
      .pipe(
        takeUntilDestroyed(this.destroyRef),)
      .subscribe({
        next: (response: any) => {
          this.categories = [...response];
          this.personCategory = this.categories.find(c => c.id === this.card().category)!;
        },
      });
  }

  handleFavourites(): void {
    this.authService.userProfile$
      .pipe(
        takeUntilDestroyed(this.destroyRef),
      ).subscribe(user => {
        this.isAdmin = !!user?.isAdmin;
        if (user) {
          this.userProfile = user!;
          const slug = this.card().slug!;
          this.favouritedByUser = !!user?.favourites?.includes(slug);
        } else {
          this.userProfile = null;
          this.favouritedByUser = null;
        }
      });
  }

  onEdit(): void {
    this.router.navigateByUrl(`admin/panel/person-form/${this.card().id}`);
  }

  onEditAux() {
    const url = `admin/panel/person-form/${this.card().id}`;
    UtilsService.openOnMiddleClick(url);
  }

  onDelete(): void {
    const id = this.card().id!;
    this.service.deleteItem(id);
  }

  onBiography() {
    const data = { slug: this.card().slug };
    const disableClose = false;
    const sheet = PersonDescriptionSheetComponent;
    const scrollStrategy = this.scroll.reposition();
    const panelClass = 'narrow-bottom-sheet';

    const href = this.router.url;
    this.location.go(href + '#');
    const scrollPosition = this.scroller.getScrollPosition();

    const sheetRef = this.matBottomSheet.open(sheet, {
      data, scrollStrategy, disableClose, panelClass
    });

    sheetRef.afterDismissed().subscribe((result: { onX: boolean, redirection: boolean; noBack: boolean, url: string }) => {
      if (result?.url) {
        this.router.navigateByUrl(result?.url);
      } else if (result?.noBack) {
        UtilsService.scrollTop();
        return;
      } else if (result?.onX) {
        this.location.back();
      } else {
        if (window.location.href.endsWith('#')) {
          this.location.back();
        };
      }

      if (!result?.redirection) {
        UtilsService.scrollTo(this.scroller, scrollPosition);
      }
    });
  }

  goToPersonPage($event: Event, skipAdmin = false) {
    $event.stopPropagation();

    if (skipAdmin && this.isAdmin) {
      return;
    }

    this.router.navigateByUrl(`biografija/${this.card().slug}`);
  }

  goToCategoryPage() {
    const category = this.card().category!;
    const categories = this.categories;
    const slug = this.idToSlugPipe.transform(category, categories);
    this.router.navigateByUrl(`kategorija/${slug}`);
  }

  onFullStar() {
    if (this.userProfile) {
      const slug = `${this.card().slug}`;
      this.userProfileService.removeFromFavourites(this.userProfile, slug);
      setTimeout(() => {
        UtilsService.openSnackbar(`Личност успешно уклоњена из фаворита.`, false, true);
      }, 120);
    }
  }

  onEmptyStar() {
    if (this.userProfile) {
      const slug = `${this.card().slug}`;
      const [isError, message] = this.userProfileService
        .addToFavourites(this.userProfile, slug);
      UtilsService.openSnackbar(message, isError, true);
    }
  }

  onNullStar() {
    const message = `Морате бити улоговани да бисте додавали фаворите.`;
    UtilsService.openSnackbar(message, true, false);
    this.router.navigateByUrl('nalog');
  }

  onBirth() {
    const { birthDay, birthMonth } = this.card();
    const path = UtilsService.getDaySlug(birthDay!, birthMonth!);
    this.router.navigateByUrl(`kalendar/${path}/rodjeni`);
  }

  onCategory() {
    this.router.navigateByUrl(`kategorija/${this.personCategory.slug}`);
  }

  onContextName($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    if (this.isAdmin) {
      this.router.navigateByUrl(`admin/panel/person-form/${this.card().id}`);
    }
  }
  onContextImage($event: MouseEvent) {
    $event.stopPropagation();
    $event.preventDefault();

    if (this.isAdmin) {
      this.onImage();
    }
  }


  onImage(): void {
    if (!this.card().created) {
      this.card().created = Date.now();
    }
    this.card().updated = Date.now();
    this.openImageDialog(this.card());
  }


  openImageDialog(item: Person): void {
    const service = this.service;
    const defaultFormat = 'portrait';
    this.clipboard.copy(item.commonName!);

    this.matDialog.open(ImageDialogComponent, {
      width: '60rem',
      data: { item, service, id: item.id, defaultFormat },
      scrollStrategy: new NoopScrollStrategy(),
    });
  }

}
