import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';

@Component({
    selector: 'app-form-checkbox',
    imports: [
        MatFormFieldModule,
        ReactiveFormsModule,
        MatCheckboxModule
    ],
    templateUrl: './form-checkbox.component.html',
    styleUrls: ['./form-checkbox.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormCheckboxComponent {
  @Input({ required: true }) label!: string;
  @Input({ required: true }) control!: string;

  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public form!: FormGroup;

  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
  }

}
