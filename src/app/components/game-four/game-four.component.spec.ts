import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameFourComponent } from './game-four.component';

describe('GameFourComponent', () => {
  let component: GameFourComponent;
  let fixture: ComponentFixture<GameFourComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GameFourComponent]
    });
    fixture = TestBed.createComponent(GameFourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
