
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { Holiday, SaintGame } from '@services/saints.service';
import { UtilsService } from '@services/utils.service';

interface SaintMonth {
  id: string | null,
  name: string,
}

function shuffleAndSlice(list: Holiday[]) {
  UtilsService.shuffleListOrder(list);
  const allMonths = list.map(i => i.saintMonth);
  const uniqueMonths = [...new Set(allMonths)];
  uniqueMonths.sort((a, b) => Number(a) - Number(b));

  const months: any = uniqueMonths
    .map(month => {
      const id = month;
      const name = UtilsService.getMonthTitleFromOrder(String(month));
      return { id, name };
    });

  const saints = list.splice(0, 4);
  return [saints, months];
}
@Component({
    selector: 'app-game-four',
    imports: [MatButtonModule, MatSelectModule, TranslitPipe, MatFormFieldModule, FormsModule, ReactiveFormsModule],
    templateUrl: './game-four.component.html',
    styleUrls: ['./game-four.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameFourComponent {
  @Input({ transform: shuffleAndSlice }) public list: any[][] = [[], []];
  @Output() public gameFourEmitter = new EventEmitter<number>();

  get saintList(): SaintGame[] {
    return this.list[0];
  }

  get monthList(): SaintMonth[] {
    return this.list[1];
  }

  public controls: string[] = ['', '', '', ''];
  public correctOrderNames: string[] = [];
  public gamePoints = 0;

  ngAfterViewInit() {
  }


  checkGameFour() {
    this.saintList.forEach((item, index) => {
      item.correctOrder = String(item.saintMonth) === String(this.controls[index]);
      this.correctOrderNames[index] = UtilsService.getMonthTitleFromOrder(String(item.saintMonth));

      if (item.correctOrder) {
        this.gamePoints += 5;
      }
    });

    this.gamePoints = this.gamePoints || 1;
    this.handleStorage();
  }

  onNext() {
    this.list = [[], []];
    this.gameFourEmitter.emit(this.gamePoints);
  }

  handleStorage() {
    const today = UtilsService.getTodayFormatted();
    const storageName = `quiz-${today}`;
    const storageQuiz = localStorage.getItem(storageName);
    let points = [];
    if (storageQuiz) {
      points = JSON.parse(storageQuiz);
    }

    points[3] = this.gamePoints;
    localStorage.setItem(storageName, JSON.stringify(points));
  }
}
