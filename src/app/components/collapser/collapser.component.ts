import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';

@Component({
    selector: 'app-collapser',
    imports: [MatIconModule],
    templateUrl: './collapser.component.html',
    styleUrl: './collapser.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CollapserComponent {
  @Input() minimized = false;

  closeBox() {
    this.minimized = false;
  }

  switchBox() {
    this.minimized = !this.minimized;
  }
}
