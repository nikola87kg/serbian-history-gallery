import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SymbolCardComponent } from './symbol-card.component';

describe('SymbolCardComponent', () => {
  let component: SymbolCardComponent;
  let fixture: ComponentFixture<SymbolCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SymbolCardComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(SymbolCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
