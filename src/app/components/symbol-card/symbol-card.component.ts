import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { AuthService } from '@services/auth.service';
import { Symbol } from '@services/symbols.service';

@Component({
    selector: 'app-symbol-card',
    imports: [CommonModule, MatIconModule, TranslitPipe],
    templateUrl: './symbol-card.component.html',
    styleUrls: ['./symbol-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SymbolCardComponent {
  @Input({ required: true }) item!: Symbol;
  @Input() view = 'full';

  private router = inject(Router);

  public authService = inject(AuthService);

  onLeader(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onEdit() {
    const id = this.item.id;
    this.router.navigateByUrl(`admin/panel/symbol-form/${id}`);
  }
}
