import { map } from 'rxjs';

import { afterNextRender, ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { Chronology, ChronologyService, ChronologyType } from '@services/chronology.service';

interface TimelineItem {
  index: number;
  year: number;
  title?: string;
  description?: string;
  start: string;
}

@Component({
  selector: 'app-timeline',
  imports: [MatTooltipModule, TranslitPipe, MatIconModule],
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimelineComponent {
  private chronologyService = inject(ChronologyService);
  private router = inject(Router);

  public timelineDots: TimelineItem[] = [];
  public timelineSeparators: TimelineItem[] = [];
  public selectedDot!: TimelineItem;

  constructor() {
    afterNextRender(() => {
      this.initScrolling();
      this.setTimeline();
    });
  }

  onDot(dot: TimelineItem) {
    this.selectedDot = dot;
  }

  setTimeline() {
    const chronologies$ = this.chronologyService.getItemsSorted('age');
    const chronologyDots$ = chronologies$.pipe(
      map(arr => arr.filter(e => e.typeId === ChronologyType.state))
    );

    chronologyDots$.subscribe({
      next: (response: Chronology[]) => {
        const dots: any[] = response.map((item, index) => {
          const start = this.getPercentage(item.age as number);
          return {
            index,
            year: item.age,
            title: item.title,
            description: item.innerHtml,
            start,
          };
        });
        this.timelineDots = [...dots];
        const randomNumber = Math.round(Math.random() * 30);


        this.selectedDot = this.timelineDots[randomNumber];
        const firstYear = this.timelineDots[0].year;
        this.scrollTimeline(firstYear, true);
      }
    });

  }

  getPercentage(year: number): string {
    const startYear = 500;
    const endYear = (new Date()).getFullYear() + 20;
    const percentage = ((year - startYear) / (endYear - startYear) * 100);
    return `${percentage}%`;
  }

  initScrolling() {
    const slider = document.querySelector(".timeline-wrapper") as any;

    if (!slider) {
      return;
    }

    let isDown = false;
    let startX: any;
    let scrollLeft: any;

    slider.addEventListener('mousedown', (e: any) => {
      isDown = true;
      slider.classList.add('active');
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
    });
    slider.addEventListener('mouseleave', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mouseup', () => {
      isDown = false;
      slider.classList.remove('active');
    });
    slider.addEventListener('mousemove', (e: any) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 2; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
    });
  }


  onLeftArrow() {
    const selectedIndex = this.selectedDot.index;
    const previousYear = this.selectedDot.year;
    if (selectedIndex !== 0) {
      this.selectedDot = this.timelineDots[selectedIndex - 1];
      this.scrollTimeline(previousYear);
    }
  }

  onRightArrow() {
    const selectedIndex = this.selectedDot.index;
    const previousYear = this.selectedDot.year;
    const lastIndex = this.timelineDots.length - 1;
    if (selectedIndex !== lastIndex) {
      this.selectedDot = this.timelineDots[selectedIndex + 1];
      this.scrollTimeline(previousYear);
    }
  }

  scrollTimeline(previousYear: number, addExtra = false) {
    const container = document.querySelector(".timeline-wrapper");
    if (!container) {
      return;
    }

    const yearDifference = this.selectedDot.year - previousYear;
    const screenWidth = window.screen.width;
    let extraScroll = 0;
    if (screenWidth > 1000) {
      extraScroll = screenWidth / 4;
    }
    let delta = (yearDifference * 2.82 - (addExtra ? extraScroll : 0));
    let isNegative = false;

    if (delta < 0) {
      delta = Math.abs(delta);
      isNegative = true;
    }

    if (addExtra) {
      container.scrollLeft += delta;
      return;
    }

    const loopArray = Array.from(Array(Math.round(delta)).keys(), num => String(num + 1));
    loopArray.forEach((_, i) => {
      setTimeout(() => {

        if (isNegative) {
          container.scrollLeft -= 1;
        } else {
          container.scrollLeft += 1;
        }
      }, 1 * i);
    });
  }

  onBackArrow() {
    const container = document.querySelector(".timeline-wrapper");
    if (!container) {
      return;
    }
    const loopArray = Array.from(Array(100).keys(), num => String(num + 1));
    loopArray.forEach((_, i) => {
      setTimeout(() => {
        container.scrollLeft -= 1;
      }, 1 * i);
    });
  }

  onForwardArrow() {
    const container = document.querySelector(".timeline-wrapper");
    if (!container) {
      return;
    }

    const loopArray = Array.from(Array(100).keys(), num => String(num + 1));
    loopArray.forEach((_, i) => {
      setTimeout(() => {
        container.scrollLeft += 1;
      }, 1 * i);
    });
  }

  onTitle() {
    this.router.navigateByUrl('vremeplov/razvoj-srpske-drzave');
  }

  onHeadline() {
    this.router.navigateByUrl('vremeplov');
  }
}
