import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-form-select',
  imports: [
    MatFormFieldModule,
    ReactiveFormsModule,
    MatSelectModule
  ],
  templateUrl: './form-select.component.html',
  styleUrls: ['./form-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormSelectComponent {
  @Input({ required: true }) label!: string;
  @Input({ required: true }) control!: string;
  @Input({ required: true }) items: any = [];
  @Input() ignoreErrorHint = false;
  @Input() wider = false;
  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public form!: FormGroup;

  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
    this.listenChanges();
  }

  checkHints() {
    const formGroup = this.form?.controls;
    const control = formGroup[this.control];
    const name = Object.keys(formGroup).find(name => control === formGroup[name]) || null;
    if (name === 'category') {
      const lastId = localStorage.getItem('last-category-id');
      if (lastId) {
        control.setValue(lastId);
      }
    }
    if (name === 'epoch') {
      const lastId = localStorage.getItem('last-epoch-id');
      if (lastId) {
        control.setValue(lastId);
      }
    }
  }

  listenChanges() {
    this.form?.controls[this.control].valueChanges.subscribe({
      next: (id) => {
        const formGroup = this.form.controls;
        const c = formGroup[this.control];
        const name = Object.keys(formGroup).find(name => c === formGroup[name]) || null;
        if (id && name === 'category') {
          localStorage.setItem('last-category-id', id);
        }
        if (id && name === 'epoch') {
          localStorage.setItem('last-epoch-id', id);
        }
      }
    });
  }
}
