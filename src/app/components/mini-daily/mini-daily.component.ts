import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { TranslitPipe } from '@app/pipes/translit.pipe';

@Component({
    selector: 'app-mini-daily',
    imports: [CommonModule, TranslitPipe],
    templateUrl: './mini-daily.component.html',
    styleUrl: './mini-daily.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MiniDailyComponent {
  @Input({ required: true }) item!: any;
  @Input() fixed = false;
}
