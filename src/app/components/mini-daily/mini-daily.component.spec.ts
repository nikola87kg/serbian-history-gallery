import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniDailyComponent } from './mini-daily.component';

describe('MiniDailyComponent', () => {
  let component: MiniDailyComponent;
  let fixture: ComponentFixture<MiniDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MiniDailyComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MiniDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
