
import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DefaultLogoDirective } from '@directives/default-logo.directive';
import { AuthService } from '@services/auth.service';
import { CategoriesService, Category } from '@services/categories.service';
import { Epoch } from '@services/epochs.service';
import { GlossaryService } from '@services/glossary.service';

@Component({
    selector: 'app-tile',
    imports: [
        DefaultLogoDirective
    ],
    templateUrl: './tile.component.html',
    styleUrls: ['./tile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TileComponent {
  @Input({ required: true }) tile!: any;
  @Input() alwaysSmall = false;

  private categoriesService = inject(CategoriesService);
  private router = inject(Router);

  public Glossary = GlossaryService.Glossary;
  public authService = inject(AuthService);
  public epochs: Epoch[] = [];
  public categories: Category[] = [];
  public birthday!: boolean;

  ngOnInit(): void {
    this.handleCategories();
  }

  handleCategories(): void {
    this.categoriesService
      .getItemsSorted('name')
      .subscribe({
        next: (response: any) => {
          this.categories = [...response];
        },
      });
  }

  onBiography(slug: string) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

}
