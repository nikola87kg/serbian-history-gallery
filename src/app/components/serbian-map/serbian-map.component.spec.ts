import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SerbianMapComponent } from './serbian-map.component';

describe('SerbianMapComponent', () => {
  let component: SerbianMapComponent;
  let fixture: ComponentFixture<SerbianMapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SerbianMapComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(SerbianMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
