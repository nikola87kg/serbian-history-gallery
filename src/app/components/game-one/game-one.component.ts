import { CdkDragDrop, DragDropModule, moveItemInArray } from '@angular/cdk/drag-drop';

import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, inject, Input, Output
} from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { DefaultLogoDirective } from '@app/directives/default-logo.directive';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { Person, PersonGame } from '@services/persons.service';
import { UtilsService } from '@services/utils.service';

function shuffleAndSlice(list: Person[]) {
  UtilsService.shuffleListOrder(list);
  return list.splice(100, 4);
}

@Component({
    selector: 'app-game-one',
    imports: [DragDropModule, MatButtonModule, DefaultLogoDirective, TranslitPipe],
    templateUrl: './game-one.component.html',
    styleUrls: ['./game-one.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameOneComponent {
  @Input({ transform: shuffleAndSlice }) public list: PersonGame[] = [];
  @Output() public gameOneEmitter = new EventEmitter<number>();

  private cdr = inject(ChangeDetectorRef);
  public gamePoints = 0;

  ngAfterViewInit() {
    for (let key in localStorage) {
      const quizStorage = key.substring(0, 7) === 'quiz-20';
      if (quizStorage) {
        localStorage.removeItem(key);
      }
    }
  }

  onDropGame1(event: CdkDragDrop<Person[]>) {
    moveItemInArray(
      event.container.data,
      event.previousIndex,
      event.currentIndex
    );
  }

  checkGameOne() {
    let correctSolution: PersonGame[] = [...this.list];
    correctSolution.sort((a, b) => this.sortItems(a, b));

    this.assignPoints(correctSolution);
  }

  sortItems(a: Person, b: Person) {
    if (a.birthYear === b.birthYear && a.birthDay && b.birthDay
      && a.birthMonth === b.birthMonth) {
      return (a.birthDay as number) - (b.birthDay as number);
    }
    if (a.birthYear === b.birthYear && a.birthMonth && b.birthMonth) {
      return (a.birthMonth as number) - (b.birthMonth as number);
    }

    return (a.birthYear as number) - (b.birthYear as number);
  }

  assignPoints(correctSolution: PersonGame[]) {
    const correctNames = correctSolution.map(i => i.commonName);

    this.list.forEach((item, index) => {
      item.correctOrder = item.commonName === correctNames[index];
      if (item.correctOrder) {
        this.gamePoints += 5;
      }
    });

    this.gamePoints = this.gamePoints || 1;
    this.handleStorage();
  }

  onNext() {
    this.list = [];
    this.gameOneEmitter.emit(this.gamePoints);
    this.cdr.detectChanges();
  }

  handleStorage() {
    const today = UtilsService.getTodayFormatted();
    const storageName = `quiz-${today}`;

    let points: number[] = [];
    points[0] = this.gamePoints;

    localStorage.setItem(storageName, JSON.stringify(points));
  }

}

