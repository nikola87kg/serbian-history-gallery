import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { MemorialCardComponent } from './memorial-card.component';

describe('MemorialCardComponent', () => {
  let component: MemorialCardComponent;
  let fixture: ComponentFixture<MemorialCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MemorialCardComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(MemorialCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
