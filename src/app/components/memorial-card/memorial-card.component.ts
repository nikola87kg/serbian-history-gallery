import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject, Input } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { TypeToIconPipe } from '@app/pipes/type-to-icon.pipe';
import { TypeToPathPipe } from '@app/pipes/type-to-path.pipe';
import { TypeToTitlePipe } from '@app/pipes/type-to-title.pipe';
import { AuthService } from '@services/auth.service';
import { Memorial, memorialTypes } from '@services/memorial.service';

@Component({
  selector: 'app-memorial-card',
  imports: [CommonModule, MatIconModule, TranslitPipe,
    TypeToIconPipe, TypeToTitlePipe,],
  templateUrl: './memorial-card.component.html',
  styleUrls: ['./memorial-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MemorialCardComponent {
  @Input({ required: true }) item!: Memorial;
  @Input() breadcrumbs = '';
  @Input() isManifestation = false;
  @Input() isNature = false;

  private isAdmin = false;
  private destroyRef = inject(DestroyRef);

  private router = inject(Router);
  private typeToPath = inject(TypeToPathPipe);
  public authService = inject(AuthService);
  public memorialTypes = memorialTypes;

  ngOnInit(): void {
    this.checkAdmin();
  }
  checkAdmin(): void {
    this.authService.userProfile$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(user => this.isAdmin = !!user?.isAdmin);
  }

  onEdit($event: any) {
    $event?.stopPropagation();
    const id = this.item.id;
    const path = this.isNature
      ? 'nature-form'
      : this.isManifestation
        ? 'manifestation-form'
        : 'memorial-form';

    this.router.navigateByUrl(`admin/panel/${path}/${id}`);
  }

  onHeritage($event: any) {
    $event.stopPropagation();
    this.router.navigateByUrl(`srpsko-nasledje`);
  }

  onTypeIcon($event: any) {
    $event.stopPropagation();
    const slug = this.breadcrumbs;
    this.router.navigateByUrl(`srpsko-nasledje/${slug}`);
  }

  onTypeTitle($event: any) {
    $event.stopPropagation();
    const slug = this.breadcrumbs;
    const path = this.typeToPath.transform(this.item.type);
    this.router.navigateByUrl(`srpsko-nasledje/${slug}/${path}`);
  }
  onContextMenu($event: any) {
    if (this.isAdmin) {
      $event.stopPropagation();
      const path = this.isNature ? 'nature-form' :
        this.isManifestation ? 'manifestation-form' : 'memorial-form';
      this.router.navigateByUrl(`admin/panel/${path}/${this.item.id}`);
    }
  }
}
