import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { afterNextRender, ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { UtilsService } from '@services/utils.service';

@Component({
    selector: 'app-daily-quiz',
    imports: [CommonModule, MatButtonModule, DragDropModule,
        MatIconModule, MatDialogModule, TranslitPipe],
    templateUrl: './daily-quiz.component.html',
    styleUrls: ['./daily-quiz.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DailyQuizComponent {
  @Input() shorten = false;

  private router = inject(Router);
  public gamePoints: number[] = [];
  public totalScore!: number;

  constructor() {
    afterNextRender(() => {
      this.checkDailyQuiz();
    });
  }

  checkDailyQuiz() {
    const today = UtilsService.getTodayFormatted();
    const storageName = `quiz-${today}`;
    const storageQuiz = localStorage.getItem(storageName);
    if (storageQuiz) {
      this.gamePoints = JSON.parse(storageQuiz);
      this.totalScore = this.gamePoints.reduce((a, b) => a + b);
    }
  }

  goToQuiz() {
    this.router.navigateByUrl('kviz');
  }

}
