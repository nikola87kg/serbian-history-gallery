import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyQuizComponent } from './daily-quiz.component';

describe('DailyQuizComponent', () => {
  let component: DailyQuizComponent;
  let fixture: ComponentFixture<DailyQuizComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [DailyQuizComponent]
    });
    fixture = TestBed.createComponent(DailyQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
