import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChronologyBoxComponent } from './chronology-box.component';

describe('ChronologyBoxComponent', () => {
  let component: ChronologyBoxComponent;
  let fixture: ComponentFixture<ChronologyBoxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ChronologyBoxComponent]
    });
    fixture = TestBed.createComponent(ChronologyBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
