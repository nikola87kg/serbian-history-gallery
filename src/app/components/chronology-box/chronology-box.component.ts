import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { chronologyTypes } from '@services/chronology.service';

@Component({
    selector: 'app-chronology-box',
    imports: [CommonModule, MatIconModule],
    templateUrl: './chronology-box.component.html',
    styleUrls: ['./chronology-box.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChronologyBoxComponent {
  private router = inject(Router);
  public chronologyTypes = chronologyTypes;
  public icons = [
    'timeline',
    'military_tech',
    'emoji_people',
    'movie',
    'sports_soccer',
    'palette',
  ];

  onItem(id?: string) {
    if (!id) {
      this.router.navigateByUrl(`vremeplov`);
      return;
    }

    this.router.navigateByUrl(`vremeplov/${id}`);
  }

}
