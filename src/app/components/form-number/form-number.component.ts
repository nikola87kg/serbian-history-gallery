import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NumbersOnlyDirective } from '@directives/numbers-only.directive';

@Component({
    selector: 'app-form-number',
    imports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        NumbersOnlyDirective
    ],
    templateUrl: './form-number.component.html',
    styleUrls: ['./form-number.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormNumberComponent {
  @Input({ required: true }) label!: string;
  @Input({ required: true }) control!: string;
  @Input() ignoreErrorHint = false;
  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public form!: FormGroup;

  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
  }
}
