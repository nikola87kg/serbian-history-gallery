import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { SaintCardComponent } from './saint-card.component';

describe('SaintCardComponent', () => {
  let component: SaintCardComponent;
  let fixture: ComponentFixture<SaintCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SaintCardComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(SaintCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
