
import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { TranslitPipe } from '@app/pipes/translit.pipe';

@Component({
    selector: 'app-game-intro',
    imports: [MatButtonModule, TranslitPipe],
    templateUrl: './game-intro.component.html',
    styleUrls: ['./game-intro.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameIntroComponent {
  @Output() private startEmitter = new EventEmitter();
  private today = new Date();
  private options: any = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

  public date = this.today.toLocaleDateString('sr', this.options as any);
  public introStarted = true;

  onIntroButton() {
    this.introStarted = false;
    this.startEmitter.emit();
  }
}
