import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameIntroComponent } from './game-intro.component';

describe('GameIntroComponent', () => {
  let component: GameIntroComponent;
  let fixture: ComponentFixture<GameIntroComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GameIntroComponent]
    });
    fixture = TestBed.createComponent(GameIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
