
import { ChangeDetectionStrategy, Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { Tag, tagGroups } from '@services/tags.service';

function addColor(tag: Tag): Tag | undefined {
  if (!tag) {
    return;
  }
  const tagGroup = tagGroups.find(g => g.id === tag.group);
  tag.color = tagGroup?.color;
  return tag;
}

@Component({
    selector: 'app-tag-item',
    imports: [TranslitPipe],
    templateUrl: './tag-item.component.html',
    styleUrls: ['./tag-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagItemComponent {
  @Input({ required: true, transform: addColor }) tag: Tag | undefined;
  @Input() emitUrl = false;
  @Output() urlEmitter = new EventEmitter<string>();

  private router = inject(Router);

  onTag(tag: Tag | undefined) {
    const url = `tag/${tag?.slug}/licnosti`;

    if (this.emitUrl) {
      this.urlEmitter.emit(url)
    } else {
      this.router.navigateByUrl(url);
    }
  }

}
