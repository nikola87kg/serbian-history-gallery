import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeritageBoxComponent } from './heritage-box.component';

describe('HeritageComponent', () => {
  let component: HeritageBoxComponent;
  let fixture: ComponentFixture<HeritageBoxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HeritageBoxComponent]
    });
    fixture = TestBed.createComponent(HeritageBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
