
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { HeritageUrls } from '@app/services/church.service';
import { GlossaryService } from '@services/glossary.service';

@Component({
  selector: 'app-heritage-box',
  imports: [MatIconModule, TranslitPipe],
  templateUrl: './heritage-box.component.html',
  styleUrls: ['./heritage-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeritageBoxComponent {
  private router = inject(Router);
  public Glossary = GlossaryService.Glossary;

  onHeadline() {
    this.router.navigateByUrl(`srpsko-nasledje`);
  }

  onSpiritual() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.churches}`);
  }

  onMemorial() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.memorials}`);
  }

  onSaint() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.saints}`);
  }

  onSymbol() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.symbols}`);
  }

  onNature() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.nature}`);
  }

  onManifestations() {
    this.router.navigateByUrl(`srpsko-nasledje/${HeritageUrls.manifestations}`);
  }

}
