import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, DestroyRef, inject, Input } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { MatIconModule } from '@angular/material/icon';
import { Router } from '@angular/router';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { TypeToIconPipe } from '@app/pipes/type-to-icon.pipe';
import { TypeToPathPipe } from '@app/pipes/type-to-path.pipe';
import { TypeToTitlePipe } from '@app/pipes/type-to-title.pipe';
import { AuthService } from '@services/auth.service';
import { Church } from '@services/church.service';

@Component({
  selector: 'app-church-card',
  imports: [CommonModule, MatIconModule, TranslitPipe,
    TypeToIconPipe, TypeToTitlePipe],
  templateUrl: './church-card.component.html',
  styleUrls: ['./church-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChurchCardComponent {
  @Input({ required: true }) item!: Church;
  @Input() breadcrumbs = '';
  private isAdmin = false;
  private destroyRef = inject(DestroyRef);

  private router = inject(Router);
  private typeToPath = inject(TypeToPathPipe);

  public authService = inject(AuthService);

  ngOnInit(): void {
    this.checkAdmin();
  }

  checkAdmin(): void {
    this.authService.userProfile$
      .pipe(takeUntilDestroyed(this.destroyRef))
      .subscribe(user => this.isAdmin = !!user?.isAdmin);
  }
  onLeader(slug: string | null) {
    this.router.navigateByUrl(`biografija/${slug}`);
  }

  onEdit($event: any) {
    $event?.stopPropagation();
    const id = this.item.id;
    this.router.navigateByUrl(`admin/panel/church-form/${id}`);
  }

  onHeritage($event: any) {
    $event.stopPropagation();
    this.router.navigateByUrl(`srpsko-nasledje`);
  }

  onTypeIcon($event: any) {
    $event.stopPropagation();
    const slug = this.breadcrumbs;
    this.router.navigateByUrl(`srpsko-nasledje/${slug}`);
  }

  onTypeTitle($event: any) {
    $event.stopPropagation();
    const slug = this.breadcrumbs;
    const path = this.typeToPath.transform(this.item.type);
    this.router.navigateByUrl(`srpsko-nasledje/${slug}/${path}`);
  }

  onContextMenu() {
    if (this.isAdmin) {
      this.router.navigateByUrl(`admin/panel/church-form/${this.item.id}`);
    }
  }
}
