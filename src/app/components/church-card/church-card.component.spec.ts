import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Auth } from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { Storage } from '@angular/fire/storage';

import { ChurchCardComponent } from './church-card.component';

describe('ChurchCardComponent', () => {
  let component: ChurchCardComponent;
  let fixture: ComponentFixture<ChurchCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ChurchCardComponent],
      providers: [
        { provide: Firestore, useValue: {} },
        { provide: Storage, useValue: {} },
        { provide: Auth, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(ChurchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
