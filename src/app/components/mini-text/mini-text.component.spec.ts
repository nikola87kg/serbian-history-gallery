import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MiniTextComponent } from './mini-text.component';

describe('MiniTextComponent', () => {
  let component: MiniTextComponent;
  let fixture: ComponentFixture<MiniTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MiniTextComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(MiniTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
