
import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy, Component, effect, EventEmitter, inject,
  input,
  Output
} from '@angular/core';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { ThemeService } from '@app/services/theme.service';

@Component({
  selector: 'app-mini-text',
  imports: [CommonModule, TranslitPipe],
  templateUrl: './mini-text.component.html',
  styleUrl: './mini-text.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MiniTextComponent {
  readonly item = input.required<any>();
  readonly index = input.required<number>();
  readonly dynamicSize = input<boolean>();
  readonly fade = input<boolean>();

  public fontSize = 16;
  @Output() clickEmitter = new EventEmitter<string>();


  public themeService = inject(ThemeService);

  constructor() {
    effect(() => {
      if (this.dynamicSize()) {
        const obj = this.item();
        const text = obj.text || obj.innerHtml || '';
        const textSize = text.length;
        if (textSize < 80) {
          this.fontSize = 22;
        } else if (textSize < 100) {
          this.fontSize = 20;
        } else if (textSize < 125) {
          this.fontSize = 19;
        } else if (textSize < 150) {
          this.fontSize = 18;
        } else if (textSize < 200) {
          this.fontSize = 16;
        } else if (textSize < 220) {
          this.fontSize = 14;
        } else if (textSize < 240) {
          this.fontSize = 13;
        } else {
          this.fontSize = 12;
        }
      }
    });
  }



}
