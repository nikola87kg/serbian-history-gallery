import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, HostListener, inject, Input
} from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { GlossaryService } from '@services/glossary.service';

'@angular/core';
@Component({
  selector: 'app-form-multiselect',
  imports: [
    MatFormFieldModule,
    ReactiveFormsModule,
    MatSelectModule
  ],
  templateUrl: './form-multiselect.component.html',
  styleUrls: ['./form-multiselect.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormMultiselectComponent {
  @Input({ required: true }) label!: string;
  @Input({ required: true }) control!: string;
  @Input({ required: true }) items: any = [];

  private rootFormGroup = inject(FormGroupDirective);
  private cdr = inject(ChangeDetectorRef);

  public form!: FormGroup;
  public repeatItems: any[] = [];
  public skipStorage = false;
  public showHint = false;
  public skipFirst = true;

  @HostListener('window:keydown.alt.А', ['$event']) addTagShortcut(event: any) {
    event.preventDefault();
    const isTagField = this.label === GlossaryService.Glossary.tags;
    if (isTagField) {
      this.onAddTag();
    }
  }

  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
    this.listenChanges();

    setTimeout(() => {
      this.checkHints();
    }, 1000);
  }

  onAddTag() {
    let city = this.form.controls['city']?.value.trim();
    city = city.split(' )').join('');
    city = city.split(')').join('');
    city = city.split(' (').join('');
    city = city.split('(').join('');
    const lastChar = city.substr(city.length - 1);
    if (lastChar === ',') {
      city = city.slice(0, -1);
    }
    const item = this.items.slice().reverse()
      .find((i: any) => city && city.includes(i.name));

    if (item) {
      if (this.label === GlossaryService.Glossary.tags) {
        const currentValue = this.form.controls['tags'].value || [];

        if (!currentValue.includes(item.id)) {
          const newValue = [item.id, ...currentValue];
          this.form.controls['tags'].setValue(newValue);
        }
      }
    }

    const title = this.form.controls['fullTitle']?.value.trim();
    const itemTitle = this.items
      .find((i: any) => title && title.includes(i.name));

    if (itemTitle) {
      if (this.label === GlossaryService.Glossary.tags) {
        const currentValue = this.form.controls['tags'].value || [];
        if (!currentValue.includes(itemTitle.id)) {
          const newValue = [itemTitle.id, ...currentValue];
          this.form.controls['tags'].setValue(newValue);
        }
      }
    }

    setTimeout(() => {
      this.form.controls['city']?.setValue(city);
    }, 200);

  }

  checkHints() {
    const formGroup = this.form?.controls;
    const control = formGroup[this.control];
    const name = Object.keys(formGroup).find(name => control === formGroup[name]) || null;

    if (name === 'tags') {
      const storage = localStorage.getItem('last-tag-id');
      this.showHint = true;

      if (storage) {
        const lastIds: any = storage.split(',');
        this.repeatItems = this.items.filter((i: any) => lastIds?.includes(i.id));
      }

      this.cdr.detectChanges();
    }
  }

  onRepeatItem(itemId: number) {
    const formGroup = this.form?.controls;
    const control = formGroup[this.control];
    const currentItems = control.value || [];
    const newItems = [...currentItems, itemId];

    this.skipStorage = true;
    control.setValue(newItems);
    setTimeout(() => {
      this.skipStorage = false;
    }, 500);
  }

  listenChanges() {
    this.form?.controls[this.control].valueChanges.subscribe({
      next: (id) => {
        if (this.skipFirst) {
          this.skipFirst = false;
          return;
        }

        const formGroup = this.form.controls;
        const c = formGroup[this.control];
        const name = Object.keys(formGroup).find(name => c === formGroup[name]) || null;

        if (id && name === 'tags' && !this.skipStorage) {
          localStorage.setItem('last-tag-id', id);
        }
      }
    });
  }
}
