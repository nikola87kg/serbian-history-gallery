import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormGroupDirective } from '@angular/forms';

import { FormUrlComponent } from './form-url.component';

describe('FormUrlComponent', () => {
  let component: FormUrlComponent;
  let fixture: ComponentFixture<FormUrlComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormUrlComponent],
      providers: [
        { provide: FormGroupDirective, useValue: {} },
      ],
    });
    fixture = TestBed.createComponent(FormUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
