
import { ChangeDetectionStrategy, Component, inject, Input } from '@angular/core';
import { FormGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

@Component({
    selector: 'app-form-url',
    imports: [
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
        MatIconModule
    ],
    templateUrl: './form-url.component.html',
    styleUrls: ['./form-url.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormUrlComponent {
  @Input({ required: true }) label!: string;
  @Input({ required: true }) control!: string;
  @Input() ignoreErrorHint = false;
  @Input() wider = false;
  private rootFormGroup: FormGroupDirective = inject(FormGroupDirective);
  public form!: FormGroup;

  ngOnInit(): void {
    this.form = this.rootFormGroup.form as FormGroup;
  }
}

