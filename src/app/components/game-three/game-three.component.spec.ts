import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameThreeComponent } from './game-three.component';

describe('GameThreeComponent', () => {
  let component: GameThreeComponent;
  let fixture: ComponentFixture<GameThreeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [GameThreeComponent]
    });
    fixture = TestBed.createComponent(GameThreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
