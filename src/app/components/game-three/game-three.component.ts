
import {
  ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, inject, Input, Output
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { TranslitPipe } from '@app/pipes/translit.pipe';
import { Quote, QuoteGame } from '@services/quotes.service';
import { UtilsService } from '@services/utils.service';

interface QuoteAuthor {
  slug: string,
  name: string,
}

function shuffleAndSlice(list: Quote[]) {
  UtilsService.shuffleListOrder(list);
  const allSlugs = list.map(i => i.authorSlug);
  const uniqueSlugs = [...new Set(allSlugs)].slice(0, 6);

  const authors = uniqueSlugs.map(slug => {
    const quote = list.find(i => i.authorSlug === slug);
    const name = quote!.authorName;
    return { slug, name };
  });

  const quotes = list.splice(0, 4);
  return [quotes, authors];
}

@Component({
    selector: 'app-game-three',
    imports: [MatButtonModule, MatSelectModule, TranslitPipe, MatFormFieldModule, FormsModule, ReactiveFormsModule],
    templateUrl: './game-three.component.html',
    styleUrls: ['./game-three.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GameThreeComponent {
  @Input({ transform: shuffleAndSlice }) public list: any[][] = [[], []];
  @Output() public gameThreeEmitter = new EventEmitter<number>();
  private cdr = inject(ChangeDetectorRef);

  get quoteList(): QuoteGame[] {
    return this.list[0];
  }

  get authorList(): QuoteAuthor[] {
    return this.list[1];
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.cdr.detectChanges();
    }, 1);
  }

  public controls: string[] = ['', '', '', ''];
  public correctOrderNames: string[] = [];
  public gamePoints = 0;

  checkGameThree() {
    this.quoteList.forEach((item, index) => {
      item.correctOrder = item.authorSlug === this.controls[index];
      this.correctOrderNames[index] = item.authorName || '';

      if (item.correctOrder) {
        this.gamePoints += 5;
      }
    });

    this.gamePoints = this.gamePoints || 1;
    this.handleStorage();
  }

  onNext() {
    this.list = [[], []];
    this.gameThreeEmitter.emit(this.gamePoints);
  }

  handleStorage() {
    const today = UtilsService.getTodayFormatted();
    const storageName = `quiz-${today}`;
    const storageQuiz = localStorage.getItem(storageName);
    let points = [];
    if (storageQuiz) {
      points = JSON.parse(storageQuiz);
    }

    points[2] = this.gamePoints;
    localStorage.setItem(storageName, JSON.stringify(points));
    this.cdr.detectChanges();
  }
}
