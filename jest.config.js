module.exports = {
  preset: 'jest-preset-angular',
  setupFilesAfterEnv: ['<rootDir>/src/setupJest.ts'],
  moduleNameMapper: {
    "^src/(.*)$": "<rootDir>/src/$1",
    "^@app/(.*)$": "<rootDir>/src/app/$1",
    "^@assets/(.*)$": "<rootDir>/src/assets/$1",
    "^@env/(.*)$": "<rootDir>/src/environments/$1",
    "^@components/(.*)$": "<rootDir>/src/app/components/$1",
    "^@dialogs/(.*)$": "<rootDir>/src/app/dialogs/$1",
    "^@directives/(.*)$": "<rootDir>/src/app/directives/$1",
    "^@pages/(.*)$": "<rootDir>/src/app/pages/$1",
    "^@pipes/(.*)$": "<rootDir>/src/app/pipes/$1",
    "^@services/(.*)$": "<rootDir>/src/app/services/$1",
    "^@admin-panel/(.*)$": "<rootDir>/src/app/pages/admin-panel/$1"
  }
};